BeAnArtist

Auteur : Guillaume Buchle
Mail : guillaume.buchle@reseau.eseo.fr
Date de derni�re Mise � Jour : 09/05/2017

Veuillez trouver dans cette archive la javadoc de mon projet, ainsi que le cahier de test des extensions cit�es ci-dessous.

##############################################
# Liste des extensions (r�alis�es en mon�me) #
##############################################

OUTILS
-------

- Outil Main : Outil disponible uniquement si l'outil s�lection a �t� choisie.
	       Il permet d'agir sur les formes s�lectionn�es
	       Le passage de l'outil Main, � l'outil S�lection ne change pas la s�lection faites

- Pour tous les outils, seul le boutton gauche de la sourie permet d'effectuer l'action voulue
_____________________________________________________________________________________________________

EXTENSIONS UI
---------------

I - Barre de Menu
	
	- Barre de Menu avec 3 menus : "Fichier", "Edition", "?"

	- Menu "Fichier" : Ouvrir (ctrl+O), Quitter (ctrl+Q), Enregistrer (ctrl+S), Enregistrer sous (ctrl+maj+S), Nouveau fichier (ctrl+N), Exporter ...

	- Nouveau Fichier : r�initialise le panneau de dessin, et ouvre un nouveau fichier
	- Ouvrir : ouvrir un fichier de sauvegarde BeAnArtist (.baasav)
	- Enregistrer : enregistre un fichier d�j� sauvagarder sans redemander o� l'enregistrer
	- Enregistrer sous ... : enregistre un fichier en demandant o� l'enregistrer
	- Exporter : demande o� exporter le dessin au format voulu (jpeg, png ou gif)
	- Quitter : ferme la fenetre BeAnArtist et arr�te le programme (si le fichier est nouveau, le syst�me demande CONFIRMATION avant d'effectuer ces actions).
			Lorsque la fenetre est r�duite, la demande de confirmation s'affiche au millieu de l'�cran.

	- Menu "edition" : Tout s�lectionner (ctrl+A), Annuler s�lection (Echap), Effacer S�lection (Suppr), Tout Effacer (ctrl+maj+Suppr), Couleur de Fond, Couleur du trait dessin,
                           Couleur du trait s�lection, Couleur remplissage s�lection, Epaisseur Bordure s�lection

	- Tout S�lectionner : actif que quand l'outil de s�lection ou la main est choisi. 
			      S�lectionne toutes les formes
	- Annuler s�lection : actif que quand l'outil de s�lection ou la main est choisi.
			      D�s�lectionne toutes les formes
	- Effacer s�lection : actif que quand l'outil de s�lection ou la main est choisi.
			      Efface uniquement les formes s�lectionn�es
	- Tout Effacer : tout le temps actif
		         Efface toutes les formes dessin�es
	- Couleur de Fond : tout le temps actif
		            permet de choisir la couleur de fond du panneau de dessin, ou de mettre celle par d�faut
	- Couleur du trait dessin : actif que quand un outil de dessin est choisi
			             permet de choisir la couleur de dessin du "crayon" dessinant les forme sur le panneau, ou de prendre celle par d�faut
	- Couleur du trait s�lection : actif que quand l'outil de s�lection ou la main est choisi.
			               permet de choisir la couleur de la bordure des forme s�lectionn�es, ou de prendre celle par d�faut
	- Couleur remplissage s�lection : actif que quand l'outil de s�lection ou la main est choisi.
			                  permet de choisir la couleur de remplissage des forme s�lectionn�es, ou de le mettre transparent
	- Epaisseur Bordure s�lection : actif que quand l'outil de s�lection ou la main est choisi.
			                permet de choisir l'�paisseur des bordures des forme s�lectionn�es, ou de prendre celle par d�faut.
                                        Le choix se fait via une fenetre PopUp qui v�rifie la bonne saisie d'un entier strictement positif, et signal l'utilisateur en cas d'erreur 

	- Menu "?" : A Propos

	- A Propos : affiche une fenetre PopUp informant l'utilisateur sur la nature du produit qu'il utilise

II - Cadran Curseur
	
	- Cadran en bas � droite de la fenetre BeAnArtist indiquant o� est la sourie sur le panneau dessin en pixels, et uniquement quand elle est au dessus.

III - Icones

	- Il y a des icones de mises � certains boutton du Panneau Barre d'Outils
	- Il y a des icones de mises certains item de la barre de Menu
	- Il y a une icone � la fen�tre BeAnArtist
	- Il y a des icone dans le menu contextuel
_______________________________________________________________________________________________________________________________________

EXTENSION DESSIN
----------------

I - Temps r�elle

	- Lors du dessin d'une forme, celle-ci se trace au fur et � mesure que notre sourie bouge (en cliquer-glisser)

	- Avec l'outil selection, lors d'un cliquer-glisser, un cadre de s�lection apparait, et les formes deviennent s�lectionn�es (en gras) � son contact, et se d�s�lectionnent 	       		lorsqu'elles sortent de ce cadre.

II - Menu Contextuel

	- Un clic droit sur le panneau dessin fait apparaitre un menu contextuel avec les options propres � l'outil courant (m�me quand aucun n'est s�lectionn�)

III - Carr� et Cercle

	- Les VueCercle et VueCarre sont untilis� dans l'affichage en temps r�elle du dessin, mais au moment de relacher le clique, ils sont respectivement remplacer par des vueEllipse et 		vue Rectangle de m�me dimension afin de permettre leur d�formation lors du redimensionnement
_______________________________________________________________________________________________________________________________________

EXTENSION FONCTIONNALITES
--------------------------

I - Raccourcie clavier
	
	- Des combinaisons de certaines touches du clavier permettent d'appeler directement certaines fonctionnalit�s (telle que celles d�crites pour la barre de menu)
	- Si le fichier en cours est un nouveau fichier, l'enregistrement est bloqu�, on ne peut qu'enregistrer sous. En revanche, dans ce cas uniquement, la combinaison "ctrl+s" appelle 		l'action enregistrer sous, et non l'aciont enregistrer.
	(les raccourcies clavier li� � la barre de menu, et celui ci-dessus repr�sente l'ensemble des raccourcie clavier impl�ment�)

II - Modification des formes dessin�es
	
	- OutilMain : - Le curseur de la sourie change selon l'endroit o� se trouve la sourie par rapport aux formes s�lectionn�es.
		      - Le deplacer les formes s�lectionn�es peut se faire lorsque le curseur est une main.
		      - Modifier la taille des formes s�lectionn�es peut de faie quand le curseur est une de double fl�che de redimensionnement (horizontale, verticale ou diagonale).
		      - Le redimensionnement modifie la largeur, la hauteur et la position des formes s�lectionn�es relativement � leut position par rapport au coin Haut-Gauche du 						rectangle les englobant.
		      - Le redimensionnement ne permet PAS de changer l'orientation des formes (pas assez de temps pour l'implementer, j'ai donc d�cid� de bloquer l'orientation). Par 						exemple, si l'on utilise bord droit pour modifier la largeur et que notre sourie se retrouve plus � gauche que le point le plus � gauche des formes 						s�lectionn�es, ces derni�res restent aplaties.
		      - Si la double fl�che est horizontale, respectivement verticale, on ne peut modifier que la largeur, respectivement la hauteur
			
	- Zoom : A tout moment l'utilisation de la molette de la sourie permet d'effectuer un zoom avant ou arri�re

III - S�lection

	- Outil S�lection : Un ctrl+clique gauche permet de s�lectionner/d�selectionner plusieurs forme
	                    Un clique gauche, seul, permet de s�lectionner/d�selectionner une seule forme � la fois
			    Un cliquer-glisser effecer l'ancienne s�lection et permet de s�lectionner plusieur formes. Les formes sont directement s�lectionn�es/d�s�lectionn�es d�s 						qu'elles entrent/sortent du rectangle de s�lection.
			    A la fin de la s�lection (boutton sourie relach�), un cadre apparait tout autour des formes s�lectionn�es, avec ses coin recouvert par des petits carr�s.

IV - Enregistrement et ouverture fichier

	- Lors de l'enregistrement/ouverture du fichier, le titre de la fenetre est remplac� par le nom de sauvegarde (sans extension) du fichier sur lequelle on travail.
	- Lors de la cr�ation d'un nouveau fichier, le nom de la fenetre redevient "Nouveau Fichier"



