package fr.eseo.gpi.beanartist;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

/**
 * Application permettant le dessin de forme et la sauvegarde au format image
 * @author guiguibubu
 *
 */
public class BeAnArtist {

	public static void main(String[] args) {
		FenetreBeAnArtist beAnArtist = FenetreBeAnArtist.getInstance();
		beAnArtist.getPanneauDessin().requestFocusInWindow();
		beAnArtist.setVisible(true);
	}

}
