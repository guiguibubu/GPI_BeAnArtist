package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionAPropos extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NOM_ACTION = "A_PROPOS";
	
	private FenetreBeAnArtist fenetre;
	
	public ActionAPropos(FenetreBeAnArtist fenetre) {
		super(ActionAPropos.NOM_ACTION);
		this.fenetre = fenetre;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(this.fenetre,
			    "                                                        BeAnArtist\nAuteur : Guillaume Buchle\nE-mail : guillaume.buchle@reseau.eseo.fr\nCet éditeur graphique à été dévellopé dans le cadre de ma formation à l'ESEO\nDate dernière Mise à Jour : 09/05/2017", "A Propos" , JOptionPane.PLAIN_MESSAGE);	
	}

}
