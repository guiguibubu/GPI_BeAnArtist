package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionAnnulerSelection extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "ANNULER_SELECTION";
	
	private PanneauDessin panneauDessin;
	
	public ActionAnnulerSelection(PanneauDessin panneauDessin) {
		super(ActionAnnulerSelection.NOM_ACTION);
		this.panneauDessin = panneauDessin;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.panneauDessin.getOutilCourant() instanceof OutilSelection){
			((OutilSelection)this.panneauDessin.getOutilCourant()).annulerSelection();
		}
	}

}
