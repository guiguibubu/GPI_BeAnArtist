package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionChoisirCouleurLigne extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "COULEUR";
	
	private boolean couleurParDefaut;
	private PanneauDessin panneau;
	
	public ActionChoisirCouleurLigne() {
		super(ActionChoisirCouleurLigne.NOM_ACTION);
	}
	
	public ActionChoisirCouleurLigne(PanneauDessin panneau, boolean couleurParDefaut){
		this();
		this.couleurParDefaut = couleurParDefaut;
		this.panneau = panneau;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		
		if(this.couleurParDefaut){
			this.panneau.setCouleurLigneCourante(PanneauDessin.COULEUR_LIGNE_PAR_DEFAUT);
		}
		else{
			Color nouvelleCouleur = JColorChooser.showDialog(FenetreBeAnArtist.getInstance(), "Choix Couleur Ligne", Color.BLACK);
			if(nouvelleCouleur != null){
				this.panneau.setCouleurLigneCourante(nouvelleCouleur);
			}
		}
		if(this.panneau != null){
			this.panneau.requestFocusInWindow();
		}
	}
}
