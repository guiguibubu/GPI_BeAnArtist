package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionChoixCouleurFond extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "COULEUR FOND";
	
	private boolean couleurParDefaut;
	
	private PanneauDessin panneauDessin;
	
	public ActionChoixCouleurFond(PanneauDessin panneauDessin, boolean couleurParDefaut) {
		super(ActionChoixCouleurFond.NOM_ACTION);
		this.panneauDessin = panneauDessin;
		this.couleurParDefaut = couleurParDefaut;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.couleurParDefaut){
			this.panneauDessin.setBackground(PanneauDessin.COULEUR_FOND_PAR_DEFAUT);
			this.panneauDessin.repaint();
		}
		else{
			Color nouvelleCouleur = JColorChooser.showDialog(FenetreBeAnArtist.getInstance(), "Choix Couleur de fond", Color.white);
			if(nouvelleCouleur != null){
				this.panneauDessin.setBackground(nouvelleCouleur);
				this.panneauDessin.repaint();
			}
		}
		this.panneauDessin.requestFocusInWindow();
	}

}
