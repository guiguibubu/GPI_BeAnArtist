package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionChoixCouleurRemplissage extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "COULEUR REMPLISSAGE";
	
	private boolean transparent;
	
	private PanneauDessin panneauDessin;
	
	public ActionChoixCouleurRemplissage(PanneauDessin panneauDessin, boolean transparent) {
		super(ActionChoixCouleurRemplissage.NOM_ACTION);
		this.panneauDessin = panneauDessin;
		this.transparent = transparent;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.transparent){
			for(VueForme vue : this.panneauDessin.getVueFormes()){
				vue.getForme().setCouleurRemplissage(null);
			}
			this.panneauDessin.repaint();
		}
		else{
			Color nouvelleCouleur = JColorChooser.showDialog(FenetreBeAnArtist.getInstance(), "Choix Couleur du trait", Color.white);
			if(nouvelleCouleur != null){
				for(VueForme vue : this.panneauDessin.getVueFormes()){
					if(vue.estSelectionne()){
						vue.getForme().setCouleurRemplissage(nouvelleCouleur);
					}
				}
				this.panneauDessin.repaint();
			}
		}
		this.panneauDessin.requestFocusInWindow();
	}

}
