package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionChoixCouleurTrait extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "COULEUR_TRAIT";
	
	private boolean couleurParDefaut;
	
	private PanneauDessin panneauDessin;
	
	public ActionChoixCouleurTrait(PanneauDessin panneauDessin, boolean couleurParDefaut) {
		super(ActionChoixCouleurTrait.NOM_ACTION);
		this.panneauDessin = panneauDessin;
		this.couleurParDefaut = couleurParDefaut;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(this.couleurParDefaut){
			for(VueForme vue : this.panneauDessin.getVueFormes()){
				if(vue.estSelectionne()){
					vue.getForme().setCouleurLigne(PanneauDessin.COULEUR_LIGNE_PAR_DEFAUT);
				}
			}
			this.panneauDessin.repaint();
		}
		else{
			Color nouvelleCouleur = JColorChooser.showDialog(FenetreBeAnArtist.getInstance(), "Choix Couleur du trait", Color.white);
			if(nouvelleCouleur != null){
				for(VueForme vue : this.panneauDessin.getVueFormes()){
					if(vue.estSelectionne()){
						vue.getForme().setCouleurLigne(nouvelleCouleur);
					}
				}
				this.panneauDessin.repaint();
			}
		}
		this.panneauDessin.requestFocusInWindow();
	}
}
