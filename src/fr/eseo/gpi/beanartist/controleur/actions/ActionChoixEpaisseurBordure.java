package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.popup.PopupEpaisseurTrait;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionChoixEpaisseurBordure extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "EPAISSEUR_BORDURE";
	
	private boolean epaisseurParDefaut;
	
	private PanneauDessin panneauDessin;
	private PopupEpaisseurTrait popupEpaisseur;
	
	public ActionChoixEpaisseurBordure(PanneauDessin panneauDessin, boolean epaisseurParDefaut) {
		super(ActionChoixEpaisseurBordure.NOM_ACTION);
		this.panneauDessin = panneauDessin;
		this.epaisseurParDefaut = epaisseurParDefaut;
		if (!this.epaisseurParDefaut){
			this.popupEpaisseur = new PopupEpaisseurTrait(FenetreBeAnArtist.getInstance());
			this.popupEpaisseur.pack();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(this.epaisseurParDefaut){
			for(VueForme vue : this.panneauDessin.getVueFormes()){
				if(vue.estSelectionne()){
					vue.getForme().setEpaisseurBordure(Forme.EPAISSEUR_BORDURE_PAR_DEFAUT);
				}
			}
			this.panneauDessin.repaint();
		}
		else{
			this.popupEpaisseur.setLocationRelativeTo(FenetreBeAnArtist.getInstance());
			this.popupEpaisseur.setVisible(true);
			Integer nouvelleEpaisseur = this.popupEpaisseur.getNouvelleEpaisseur();
			if(nouvelleEpaisseur != null){
				for(VueForme vue : this.panneauDessin.getVueFormes()){
					if(vue.estSelectionne()){
						vue.getForme().setEpaisseurBordure(nouvelleEpaisseur);
					}
				}
				this.panneauDessin.repaint();
			}
		}
		this.panneauDessin.requestFocusInWindow();
	}
}
