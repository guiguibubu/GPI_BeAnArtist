package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionEffacer extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "Effacer Tout";	
	
	public ActionEffacer() {
		super(ActionEffacer.NOM_ACTION);
	}
	
	public void actionPerformed(ActionEvent e){
		FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().clear();
		FenetreBeAnArtist.getInstance().getPanneauDessin().repaint();
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
	}
}
