package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionEnregistrer extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "ENREGISTRER SOUS";

	private JFileChooser fileChooser;
	
	private static final String DESCRIPTION_EXTENSION = "BeAnArtist Save File"; 
	private static final String EXTENSION = "baasav"; 
	
	public ActionEnregistrer() {
		super(ActionEnregistrer.NOM_ACTION);
		this.fileChooser = new JFileChooser("./save");
		FileNameExtensionFilter extensionFichier = new FileNameExtensionFilter(ActionEnregistrer.DESCRIPTION_EXTENSION, ActionEnregistrer.EXTENSION);
		this.fileChooser.setFileFilter(extensionFichier);
		this.fileChooser.setDialogTitle("Enregistrer sous");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		if(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() instanceof OutilSelection){
			((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).annulerSelection();
		}
		
		int reponse = this.fileChooser.showSaveDialog(FenetreBeAnArtist.getInstance());
		if(reponse == JFileChooser.APPROVE_OPTION){
			this.enregistrerDessin();
		}
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
	}
	
	private void enregistrerDessin(){
		String nomFichier = this.fileChooser.getName(this.fileChooser.getSelectedFile());
		if((nomFichier.length() <= ActionEnregistrer.EXTENSION.length()+1) || ((nomFichier.length() > ActionEnregistrer.EXTENSION.length()+1) && !(nomFichier.substring(nomFichier.length()-ActionEnregistrer.EXTENSION.length()-1, nomFichier.length()).equals("."+ActionEnregistrer.EXTENSION)))){
			nomFichier += "."+ActionEnregistrer.EXTENSION;
		}
		String adresseFichier = this.fileChooser.getCurrentDirectory().getPath()+"\\"+nomFichier;
		
		File fichierSauvegarde =  new File(adresseFichier) ;
		
		FileOutputStream fileOutputStream;
		ObjectOutputStream objectOutputStream;
		try {
			fileOutputStream = new FileOutputStream(fichierSauvegarde);
			objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeInt(FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().size());
			objectOutputStream.writeInt(FenetreBeAnArtist.getInstance().getPanneauDessin().getBackground().getRGB());
			if(FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().size() == 0){
				objectOutputStream.writeObject(null);
			}
			for(int i = 0; i <  FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().size(); i++){
				objectOutputStream.writeObject(FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().get(i).getForme()) ;
			}
			fileOutputStream.close();
			objectOutputStream.close();
			
			FenetreBeAnArtist.getInstance().setTitle(nomFichier.substring(0, nomFichier.length()-ActionEnregistrer.EXTENSION.length()-1));
			FenetreBeAnArtist.getInstance().setCheminSauvegarde(adresseFichier);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
