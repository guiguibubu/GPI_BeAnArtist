package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionEnregistrerFichierDejaCree extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "ENREGISTRER"; 
	
	private String cheminFichierSauvegarde;
	
	public ActionEnregistrerFichierDejaCree() {
		super(ActionEnregistrerFichierDejaCree.NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		if(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() instanceof OutilSelection){
			((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).annulerSelection();
		}
		this.setCheminSauvegarde(FenetreBeAnArtist.getInstance().getCheminSauvegarde());
		this.enregistrerDessin();
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
	}
	
	private void enregistrerDessin(){
		
		File fichierSauvegarde =  new File(this.cheminFichierSauvegarde) ;
		
		FileOutputStream fileOutputStream;
		ObjectOutputStream objectOutputStream;
		try {
			fileOutputStream = new FileOutputStream(fichierSauvegarde);
			objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeInt(FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().size());
			objectOutputStream.writeInt(FenetreBeAnArtist.getInstance().getPanneauDessin().getBackground().getRGB());
			if(FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().size() == 0){
				objectOutputStream.writeObject(null);
			}
			for(int i = 0; i <  FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().size(); i++){
				objectOutputStream.writeObject(FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().get(i).getForme()) ;
			}
			fileOutputStream.close();
			objectOutputStream.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setCheminSauvegarde(String chemin){
		this.cheminFichierSauvegarde = chemin;
	}
}
