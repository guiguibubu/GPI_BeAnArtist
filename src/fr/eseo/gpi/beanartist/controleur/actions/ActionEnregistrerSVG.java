package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.xml.EnregistreurSVG;

public class ActionEnregistrerSVG extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "EXPORTER SVG";

	private JFileChooser fileChooser;
	private EnregistreurSVG enregistreurSVG;
	
	private static final String DESCRIPTION_EXTENSION = "Scalable Vector Graphics (SVG)"; 
	private static final String EXTENSION = "svg"; 
	
	public ActionEnregistrerSVG() {
		super(ActionEnregistrerSVG.NOM_ACTION);
		this.fileChooser = new JFileChooser("./save");
		FileNameExtensionFilter extensionFichier = new FileNameExtensionFilter(ActionEnregistrerSVG.DESCRIPTION_EXTENSION, ActionEnregistrerSVG.EXTENSION);
		this.fileChooser.setFileFilter(extensionFichier);
		this.fileChooser.setDialogTitle("Exporter en SVG");
		this.enregistreurSVG = new EnregistreurSVG();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		if(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() instanceof OutilSelection){
			((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).annulerSelection();
		}
		
		int reponse = this.fileChooser.showSaveDialog(FenetreBeAnArtist.getInstance());
		if(reponse == JFileChooser.APPROVE_OPTION){
			this.enregistrerDessin();
		}
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
	}
	
	private void enregistrerDessin(){
		String nomFichier = this.fileChooser.getName(this.fileChooser.getSelectedFile());
		if((nomFichier.length() <= ActionEnregistrerSVG.EXTENSION.length()+1) || ((nomFichier.length() > ActionEnregistrerSVG.EXTENSION.length()+1) && !(nomFichier.substring(nomFichier.length()-ActionEnregistrerSVG.EXTENSION.length()-1, nomFichier.length()).equals("."+ActionEnregistrerSVG.EXTENSION)))){
			nomFichier += "."+ActionEnregistrerSVG.EXTENSION;
		}
		String adresseFichier = this.fileChooser.getCurrentDirectory().getPath()+"\\"+nomFichier;
		
		try {
			this.enregistreurSVG.enregistreDessin(adresseFichier, FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}
