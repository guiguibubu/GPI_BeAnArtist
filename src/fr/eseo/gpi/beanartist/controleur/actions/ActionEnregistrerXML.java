package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.xml.EnregistreurXML;

public class ActionEnregistrerXML extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "EXPORTER XML";

	private JFileChooser fileChooser;
	private EnregistreurXML enregistreurXML;
	
	private static final String DESCRIPTION_EXTENSION = "Extensible Markup Language (XML)"; 
	private static final String EXTENSION = "xml"; 
	
	public ActionEnregistrerXML() {
		super(ActionEnregistrerXML.NOM_ACTION);
		this.fileChooser = new JFileChooser("./save");
		FileNameExtensionFilter extensionFichier = new FileNameExtensionFilter(ActionEnregistrerXML.DESCRIPTION_EXTENSION, ActionEnregistrerXML.EXTENSION);
		this.fileChooser.setFileFilter(extensionFichier);
		this.fileChooser.setDialogTitle("Exporter en XML");
		this.enregistreurXML = new EnregistreurXML();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		if(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() instanceof OutilSelection){
			((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).annulerSelection();
		}
		
		int reponse = this.fileChooser.showSaveDialog(FenetreBeAnArtist.getInstance());
		if(reponse == JFileChooser.APPROVE_OPTION){
			this.enregistrerDessin();
		}
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
	}
	
	private void enregistrerDessin(){
		String nomFichier = this.fileChooser.getName(this.fileChooser.getSelectedFile());
		if((nomFichier.length() <= ActionEnregistrerXML.EXTENSION.length()+1) || ((nomFichier.length() > ActionEnregistrerXML.EXTENSION.length()+1) && !(nomFichier.substring(nomFichier.length()-ActionEnregistrerXML.EXTENSION.length()-1, nomFichier.length()).equals("."+ActionEnregistrerXML.EXTENSION)))){
			nomFichier += "."+ActionEnregistrerXML.EXTENSION;
		}
		String adresseFichier = this.fileChooser.getCurrentDirectory().getPath()+"\\"+nomFichier;
		
		try {
			this.enregistreurXML.enregistreDessin(adresseFichier, FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}
