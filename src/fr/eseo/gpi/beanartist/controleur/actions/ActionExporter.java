package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.eseo.gpi.beanartist.controleur.outils.Outil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;
import fr.eseo.gpi.beanartist.xml.EnregistreurSVG;
import fr.eseo.gpi.beanartist.xml.EnregistreurXML;

public class ActionExporter extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NOM_ACTION = "EXPORTER";
	
	private static final String DESCRIPTION_EXTENSION_JPEG = "Joint Photographic Experts Group (JPEG)";
	private static final String EXTENSION_JPEG = "jpeg";
	private static final String DESCRIPTION_EXTENSION_PNG = "Portable Network Graphics (PNG)";
	private static final String EXTENSION_PNG = "png";
	private static final String DESCRIPTION_EXTENSION_GIF = "Graphics Interchange Format (GIF)";
	private static final String EXTENSION_GIF = "gif";
	private static final String DESCRIPTION_EXTENSION_SVG = "Scalable Vector Graphics (SVG)"; 
	private static final String EXTENSION_SVG = "svg";
	private static final String DESCRIPTION_EXTENSION_XML = "Extensible Markup Language (XML)"; 
	private static final String EXTENSION_XML = "xml"; 
	
	
	private PanneauDessin panneauDessin;
	private JFileChooser fileChooser;
	private ImageWriter imageWriterJPEG;
	private ImageWriter imageWriterPNG;
	private ImageWriter imageWriterGIF;
	private EnregistreurSVG enregistreurSVG;
	private EnregistreurXML enregistreurXML;
	
	private ImageWriter imageWriterChoisie;
	private String formatImage;
	
	public ActionExporter(PanneauDessin panneauDessin) {
		super(ActionExporter.NOM_ACTION);
		this.panneauDessin = panneauDessin;
		this.fileChooser = new JFileChooser("./save");
		FileNameExtensionFilter extensionFichierJPEG = new FileNameExtensionFilter(ActionExporter.DESCRIPTION_EXTENSION_JPEG, ActionExporter.EXTENSION_JPEG);
		FileNameExtensionFilter extensionFichierPNG = new FileNameExtensionFilter(ActionExporter.DESCRIPTION_EXTENSION_PNG, ActionExporter.EXTENSION_PNG);
		FileNameExtensionFilter extensionFichierGIF = new FileNameExtensionFilter(ActionExporter.DESCRIPTION_EXTENSION_GIF, ActionExporter.EXTENSION_GIF);
		FileNameExtensionFilter extensionFichierXML = new FileNameExtensionFilter(ActionExporter.DESCRIPTION_EXTENSION_XML, ActionExporter.EXTENSION_XML);
		FileNameExtensionFilter extensionFichierSVG = new FileNameExtensionFilter(ActionExporter.DESCRIPTION_EXTENSION_SVG, ActionExporter.EXTENSION_SVG);
		this.fileChooser.addChoosableFileFilter(extensionFichierJPEG);
		this.fileChooser.addChoosableFileFilter(extensionFichierPNG);
		this.fileChooser.addChoosableFileFilter(extensionFichierGIF);
		this.fileChooser.addChoosableFileFilter(extensionFichierXML);
		this.fileChooser.addChoosableFileFilter(extensionFichierSVG);
		
		this.fileChooser.setDialogTitle("Exporter Dessin");
		
		Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpeg");
		this.imageWriterJPEG = writers.next();
		writers = ImageIO.getImageWritersByFormatName("png");
		this.imageWriterPNG = writers.next();
		writers = ImageIO.getImageWritersByFormatName("gif");
		this.imageWriterGIF = writers.next();
		this.enregistreurXML = new EnregistreurXML();
		this.enregistreurSVG = new EnregistreurSVG();

		this.formatImage = ActionExporter.EXTENSION_JPEG;
		this.imageWriterChoisie = this.imageWriterJPEG;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		
		Outil outilCourant = FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant();
		if (outilCourant instanceof OutilSelection){
			((OutilSelection)outilCourant).annulerSelection();
		}
		
		int reponse = this.fileChooser.showSaveDialog(FenetreBeAnArtist.getInstance());
		if(reponse == JFileChooser.APPROVE_OPTION){
			this.exporterDessin();
		}
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
	}
	
	private void exporterDessin(){
		String nomFichier = this.fileChooser.getName(this.fileChooser.getSelectedFile());
		if(nomFichier.endsWith("."+ActionExporter.EXTENSION_JPEG)){
			this.formatImage = ActionExporter.EXTENSION_JPEG;
		}
		else if (nomFichier.endsWith("."+ActionExporter.EXTENSION_PNG)){
			this.formatImage = ActionExporter.EXTENSION_PNG;
		}
		
		else if (nomFichier.endsWith("."+ActionExporter.EXTENSION_GIF)){
			this.formatImage = ActionExporter.EXTENSION_GIF;
		}
		
		else if (nomFichier.endsWith("."+ActionExporter.EXTENSION_XML)){
			this.formatImage = ActionExporter.EXTENSION_XML;
		}
		
		else if (nomFichier.endsWith("."+ActionExporter.EXTENSION_SVG)){
			this.formatImage = ActionExporter.EXTENSION_SVG;
		}
		
		else if (!this.fileChooser.getFileFilter().getDescription().equals("Tous les fichiers")){
			this.formatImage = ((FileNameExtensionFilter)this.fileChooser.getFileFilter()).getExtensions()[0];
		}
		else{
			return;
		}

		switch(this.formatImage){
			case ActionExporter.EXTENSION_JPEG : this.imageWriterChoisie = this.imageWriterJPEG; 
				break;
			case ActionExporter.EXTENSION_PNG : this.imageWriterChoisie = this.imageWriterPNG; 
				break;
			case ActionExporter.EXTENSION_GIF : this.imageWriterChoisie = this.imageWriterGIF; 
				break;
			default : this.imageWriterChoisie = null;
				break;
		}
		
		if((nomFichier.length() <= this.formatImage.length()+1) || ((nomFichier.length() > this.formatImage.length()+1) && !(nomFichier.substring(nomFichier.length()-this.formatImage.length()-1, nomFichier.length()).equals("."+this.formatImage)))){
			nomFichier += "."+this.formatImage;
		}
		String adresseFichier = this.fileChooser.getCurrentDirectory().getPath()+"\\"+nomFichier;
		
		File fichierSauvegarde =  new File(adresseFichier) ;
	
		try {
			if(this.imageWriterChoisie != null){
				ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(fichierSauvegarde);
				this.imageWriterChoisie.setOutput(imageOutputStream);
				this.imageWriterChoisie.write(this.creerBufferedImage());
				imageOutputStream.close();
			}
			else if (this.formatImage.equals(DESCRIPTION_EXTENSION_XML)){
				this.enregistreurXML.enregistreDessin(adresseFichier, FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes());
			}
			else if (this.formatImage.equals(DESCRIPTION_EXTENSION_SVG)){
				this.enregistreurSVG.enregistreDessin(adresseFichier, FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private BufferedImage creerBufferedImage() {
	    int largeurImage = this.panneauDessin.getWidth();
	    int hauteurImage = this.panneauDessin.getHeight();
	    BufferedImage bi = new BufferedImage(largeurImage, hauteurImage, BufferedImage.TYPE_INT_RGB);
	    this.panneauDessin.paint(bi.getGraphics());
	    return bi;
	}
}
