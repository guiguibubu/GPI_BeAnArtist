package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JToggleButton;

import fr.eseo.gpi.beanartist.controleur.outils.Outil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilCarre;
import fr.eseo.gpi.beanartist.controleur.outils.OutilCercle;
import fr.eseo.gpi.beanartist.controleur.outils.OutilEllipse;
import fr.eseo.gpi.beanartist.controleur.outils.OutilLigne;
import fr.eseo.gpi.beanartist.controleur.outils.OutilRectangle;
import fr.eseo.gpi.beanartist.controleur.outils.OutilTrace;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import java.util.ArrayList;

public class ActionForme extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION_RECTANGLE = "RECTANGLE";
	public static final String NOM_ACTION_ELLIPSE = "ELLIPSE";	
	public static final String NOM_ACTION_CARRE = "CARRE";	
	public static final String NOM_ACTION_CERCLE = "CERCLE";	
	public static final String NOM_ACTION_LIGNE = "LIGNE";	
	public static final String NOM_ACTION_TRACE = "TRACE";	
	
	private static ArrayList<String> nomAction = new ArrayList<String>();
	private static ArrayList<Outil> outilsForme = new ArrayList<Outil>();
	
	
	public ActionForme(String nom) {
		super(nom);
		if(ActionForme.nomAction.isEmpty()){
			ActionForme.nomAction.add(ActionForme.NOM_ACTION_RECTANGLE);
			ActionForme.nomAction.add(ActionForme.NOM_ACTION_ELLIPSE);
			ActionForme.nomAction.add(ActionForme.NOM_ACTION_CARRE);
			ActionForme.nomAction.add(ActionForme.NOM_ACTION_CERCLE);
			ActionForme.nomAction.add(ActionForme.NOM_ACTION_LIGNE);
			ActionForme.nomAction.add(ActionForme.NOM_ACTION_TRACE);
		}
		if(ActionForme.outilsForme.isEmpty()){
			ActionForme.outilsForme.add(new OutilRectangle());
			ActionForme.outilsForme.add(new OutilEllipse());
			ActionForme.outilsForme.add(new OutilCarre());
			ActionForme.outilsForme.add(new OutilCercle());
			ActionForme.outilsForme.add(new OutilLigne());
			ActionForme.outilsForme.add(new OutilTrace());
		}
	}
	
	public ActionForme(){
		this("");
	}
	
	public void actionPerformed(ActionEvent e){
		//System.out.println(((JToggleButton) e.getSource()).isSelected());

		if (((JToggleButton) e.getSource()).isSelected()){
			//On s'assure que s'il y a déjà un boutton selectionné, il soit déselectionné 
			FenetreBeAnArtist.getInstance().getPanneauBarreOutils().getGroupeBoutton().clearSelection();
			
			int indexAction = ActionForme.nomAction.indexOf(e.getActionCommand());
			
			if((indexAction >= 0)&&(indexAction < ActionForme.nomAction.size())){
				Outil outilVoulu = ActionForme.outilsForme.get(indexAction);
				
				if(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() != null){
					if(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant().getClass() != outilVoulu.getClass()){	
						
						((JToggleButton) e.getSource()).setSelected(true);
						outilVoulu.associer(FenetreBeAnArtist.getInstance().getPanneauDessin());
						//On quitte le mose de sélection
						FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
					}
					else{
						FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant().associer(null);
					}
				}
				else{
					((JToggleButton) e.getSource()).setSelected(true);
					
					switch(e.getActionCommand()){
					case ActionForme.NOM_ACTION_RECTANGLE : new OutilRectangle(FenetreBeAnArtist.getInstance().getPanneauDessin()); 
						break;
					case ActionForme.NOM_ACTION_ELLIPSE : new OutilEllipse(FenetreBeAnArtist.getInstance().getPanneauDessin()); 
						break;
					case ActionForme.NOM_ACTION_CARRE : new OutilCarre(FenetreBeAnArtist.getInstance().getPanneauDessin());
						break;
					case ActionForme.NOM_ACTION_CERCLE : new OutilCercle(FenetreBeAnArtist.getInstance().getPanneauDessin());
						break;
					case ActionForme.NOM_ACTION_LIGNE : new OutilLigne(FenetreBeAnArtist.getInstance().getPanneauDessin());
						break;
					case ActionForme.NOM_ACTION_TRACE : new OutilTrace(FenetreBeAnArtist.getInstance().getPanneauDessin());
						break;
					default : break;
					}
				}
			}
			
			FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
		}
	}
}
