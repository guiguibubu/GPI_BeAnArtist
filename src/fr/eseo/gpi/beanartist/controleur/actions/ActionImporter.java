package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.xml.LecteurXML;

public class ActionImporter extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "IMPORTER";	
	
	private JFileChooser fileChooser;
	private LecteurXML lecteurXML;
	
	private static final String DESCRIPTION_EXTENSION_XML = "Extensible Markup Language (XML)"; 
	private static final String EXTENSION_XML = "xml"; 
	
	public ActionImporter() {
		super(ActionImporter.NOM_ACTION);
		this.fileChooser = new JFileChooser("./save");
		FileNameExtensionFilter extensionFichierXML = new FileNameExtensionFilter(ActionImporter.DESCRIPTION_EXTENSION_XML, ActionImporter.EXTENSION_XML);
		this.fileChooser.addChoosableFileFilter(extensionFichierXML);
		this.fileChooser.setDialogTitle("Importer");
		this.lecteurXML = new LecteurXML();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		if(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() instanceof OutilSelection){
			((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).annulerSelection();
		}
		
		int reponse = this.fileChooser.showOpenDialog(FenetreBeAnArtist.getInstance());
		if(reponse == JFileChooser.APPROVE_OPTION){
			this.ouvrirDessin();
			FenetreBeAnArtist.getInstance().getPanneauDessin().repaint();
		}
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();

	}
	
	private void ouvrirDessin(){
		String nomFichier = this.fileChooser.getName(this.fileChooser.getSelectedFile());
		String adresseFichier = this.fileChooser.getCurrentDirectory().getPath()+"\\"+nomFichier;
		if(nomFichier.endsWith("."+ActionImporter.EXTENSION_XML)){
			try {
				FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().clear();
				this.lecteurXML.importe(FenetreBeAnArtist.getInstance(), adresseFichier);
				
				FenetreBeAnArtist.getInstance().setTitle(nomFichier.substring(0, nomFichier.length()-ActionImporter.EXTENSION_XML.length()-1));
				FenetreBeAnArtist.getInstance().getMenuBarre().autoriserEnregistrer();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else{
			JOptionPane.showMessageDialog(FenetreBeAnArtist.getInstance(), "BeAnArtist ne peux pas importer ce fichier");
		}
	}
}
