package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionNouveauFichier extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NOM_ACTION = "NOUVEAU FICHIER";
	
	public ActionNouveauFichier() {
		super(ActionNouveauFichier.NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		new ActionEffacer().actionPerformed(e);
		FenetreBeAnArtist.getInstance().getPanneauDessin().setBackground(PanneauDessin.COULEUR_FOND_PAR_DEFAUT);
		FenetreBeAnArtist.getInstance().setTitle(FenetreBeAnArtist.TITRE_PAR_DEFAUT);
		FenetreBeAnArtist.getInstance().getMenuBarre().nouveauFichier();
	}

}
