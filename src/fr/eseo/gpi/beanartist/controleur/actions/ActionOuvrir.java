package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.vue.formes.VueCercle;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueTrace;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionOuvrir extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "OUVRIR";	
	
	private JFileChooser fileChooser;
	
	private static final String DESCRIPTION_EXTENSION = "BeAnArtist Save File"; 
	private static final String EXTENSION = "baasav"; 
	
	public ActionOuvrir() {
		super(ActionOuvrir.NOM_ACTION);
		this.fileChooser = new JFileChooser("./save");
		FileNameExtensionFilter extensionFichier = new FileNameExtensionFilter(ActionOuvrir.DESCRIPTION_EXTENSION, ActionOuvrir.EXTENSION);
		this.fileChooser.setFileFilter(extensionFichier);
		this.fileChooser.setDialogTitle("Ouvrir");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//On quitte le mode de sélection
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
		int reponse = this.fileChooser.showOpenDialog(FenetreBeAnArtist.getInstance());
		if(reponse == JFileChooser.APPROVE_OPTION){
			this.ouvrirDessin();
			FenetreBeAnArtist.getInstance().getPanneauDessin().repaint();
		}
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();

	}
	
	private void ouvrirDessin(){
		String nomFichier = this.fileChooser.getName(this.fileChooser.getSelectedFile());
		String adresseFichier = this.fileChooser.getCurrentDirectory().getPath()+"\\"+nomFichier;
		
		File fichierSauvegarde =  new File(adresseFichier);
		
		FileInputStream fileInputStream;
		ObjectInputStream objectInputStream;
		if(nomFichier.endsWith(ActionOuvrir.EXTENSION)){
			try {
				fileInputStream = new FileInputStream(fichierSauvegarde);
				objectInputStream = new ObjectInputStream(fileInputStream);
				int NbFormes = objectInputStream.readInt();
				
				FenetreBeAnArtist.getInstance().getPanneauDessin().setBackground(new Color(objectInputStream.readInt()));
				FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().clear();
				for(int i = 0; i <  NbFormes; i++){
					Forme forme = (Forme)objectInputStream.readObject();
					VueForme vueForme;
					switch(forme.getClass().getSimpleName()){
					case "Carre" : vueForme = new VueCarre((Carre)forme);FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().add(vueForme);
						break;
					case "Cercle" : vueForme = new VueCercle((Cercle)forme);FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().add(vueForme);
						break;
					case "Ellipse" : vueForme = new VueEllipse((Ellipse)forme);FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().add(vueForme);
						break;
					case "Ligne" : vueForme = new VueLigne((Ligne)forme);FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().add(vueForme);
						break;
					case "Rectangle" : vueForme = new VueRectangle((Rectangle)forme);FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().add(vueForme);
						break;
					case "Trace" : vueForme = new VueTrace((Trace)forme);FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().add(vueForme);
						break;
					default : break;
					}
				}
				fileInputStream.close();
				objectInputStream.close();
				
				FenetreBeAnArtist.getInstance().setTitle(nomFichier.substring(0, nomFichier.length()-ActionOuvrir.EXTENSION.length()-1));
				FenetreBeAnArtist.getInstance().getMenuBarre().autoriserEnregistrer();
				
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		else{
			JOptionPane.showMessageDialog(FenetreBeAnArtist.getInstance(), "BeAnArtist ne peux pas ouvrir ce fichier");
		}
	}
}
