package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionQuitter extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NOM_ACTION = "QUITTER";
	
	public ActionQuitter() {
		super(ActionQuitter.NOM_ACTION);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(FenetreBeAnArtist.getInstance().getTitle().equals(FenetreBeAnArtist.TITRE_PAR_DEFAUT)){
			int retour = JOptionPane.showConfirmDialog(FenetreBeAnArtist.getInstance(), "Etes-vous sûr de vouloir quitter sans sauvegarder ?", "Quitter sans sauvegarder", JOptionPane.YES_NO_OPTION);
			if(retour == JOptionPane.OK_OPTION){
				FenetreBeAnArtist.getInstance().dispose();
				System.exit(0);
			}
		}
		else{
			FenetreBeAnArtist.getInstance().dispose();
			System.exit(0);
		}
	}

}
