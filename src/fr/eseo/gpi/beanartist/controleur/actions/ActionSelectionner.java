package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JToggleButton;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionSelectionner extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NOM_ACTION = "SELECTION";
	public static final String NOM_ACTION_DEPLACEMENT = "MAIN";
	
	private OutilSelection outilSelection;
	
	public ActionSelectionner() {
		super(ActionSelectionner.NOM_ACTION);
		if(this.outilSelection == null){
			this.outilSelection = new OutilSelection();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (((JToggleButton) e.getSource()).isSelected()){
			//On s'assure que s'il y a déjà un boutton selectionné, il soit déselectionné 
			FenetreBeAnArtist.getInstance().getPanneauBarreOutils().getGroupeBoutton().clearSelection();
			
			if(!(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() instanceof OutilSelection) || (e.getActionCommand().equals(ActionSelectionner.NOM_ACTION) &&((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).isAutoriseDeplacer())){
				((JToggleButton) e.getSource()).setSelected(true);	
				FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(true);
				if(!(FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant() instanceof OutilSelection)){
					this.outilSelection.associer(FenetreBeAnArtist.getInstance().getPanneauDessin());
				}
				this.outilSelection.autoriserDeplacement(false);
			}
			else if(e.getActionCommand().equals(ActionSelectionner.NOM_ACTION_DEPLACEMENT) && !((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).isAutoriseDeplacer()){
				this.outilSelection.autoriserDeplacement(true);
				((JToggleButton) e.getSource()).setSelected(true);
			}
			else if(e.getActionCommand().equals(ActionSelectionner.NOM_ACTION_DEPLACEMENT) && ((OutilSelection)FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant()).isAutoriseDeplacer()){
				this.outilSelection.autoriserDeplacement(false);
				FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant().associer(null);
				FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
			}
			else{
				FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant().associer(null);
				FenetreBeAnArtist.getInstance().getPanneauBarreOutils().modeSelection(false);
			}
		}
		
		FenetreBeAnArtist.getInstance().getPanneauDessin().requestFocusInWindow();
	}

}
