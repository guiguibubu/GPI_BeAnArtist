package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

/**
 * Classe abstraite modélisant un outil
 *
 */
public abstract class Outil implements MouseListener, MouseMotionListener {
	
	private PanneauDessin panneauDessin;
	private Point debut;
	private Point fin;
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Le panneau Dessin de l'outil
	 */
	public Outil(PanneauDessin panneauDessin) {
		if(panneauDessin != null){
			this.associer(panneauDessin);
		}
		else{
			this.panneauDessin = null;
		}
		
		this.debut = new Point();
		this.fin = new Point();
		
	}
	
	/**
	 * Construit un outil lié à aucun panneau Dessin
	 */
	public Outil() {
		this(null);
	}
	
	/**
	 * Renvoie le panneau Dessin auquel est associé l'outil
	 * @return Lepe=anneau Dessin auquel est associé l'outil
	 */
	public PanneauDessin getPanneauDessin(){
		return this.panneauDessin;
	}
	
	/**
	 * Modifie le panneau dessin de l'outil 
	 * @param panneauDessin Le nouveau panneau Dessin de l'outil
	 */
	public void setPanneauDessin(PanneauDessin panneauDessin){
		this.panneauDessin = panneauDessin;
	}
	
	/**
	 * Associe l'outil à un panneau Dessin, après avoir libéré celui-ci de tout autre outil, et davoir libéré son propre panneau Dessin aussi
	 * @param panneauDessin Le nouveau panneau Dessin de l'outil (juste libération si null)
	 */
	public void associer(PanneauDessin panneauDessin){
		if(panneauDessin == null){
			this.liberer();
		}
		else{
			if(this.panneauDessin != null){
				this.liberer();
			}
			this.panneauDessin = panneauDessin;
			if (this.panneauDessin.getOutilCourant() != null){
				this.liberer();
			}
			this.panneauDessin.addMouseListener(this);
			this.panneauDessin.addMouseMotionListener(this);
			//this.panneauDessin.addKeyListener((OutilForme)this);
			this.panneauDessin.setOutilCourant(this);
			
			if(this.getClass() == OutilSelection.class){
				this.panneauDessin.addKeyListener((OutilSelection)this);
				this.panneauDessin.requestFocusInWindow();
				this.panneauDessin.modeSelection(true);
			}
		}
	}
	
	private void liberer(){
		if(this.panneauDessin.getOutilCourant() == this){
			this.panneauDessin.setOutilCourant(null);
			this.panneauDessin.removeMouseListener(this);
			this.panneauDessin.removeMouseMotionListener(this);
			//this.panneauDessin.removeKeyListener(this);
			if(this.getClass() == OutilSelection.class){
				this.panneauDessin.removeKeyListener((KeyListener) this);
				this.panneauDessin.modeSelection(false);
			}
			this.panneauDessin = null;
		}
		else{
			this.panneauDessin.getOutilCourant().liberer();
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		
		if(e.getButton() == MouseEvent.BUTTON1){
			if(this.debut == null){
				this.debut = new Point(e.getX(), e.getY());
			}
			else{
				this.debut.deplacerVers(e.getX(), e.getY());
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		if(e.getButton() == MouseEvent.BUTTON1){
			if(this.fin == null){
				this.fin = new Point(e.getX(), e.getY());
			}
			else{
				this.fin.deplacerVers(e.getX(), e.getY());
			}
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		this.fin.deplacerVers(e.getX(), e.getY());
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	
	public Point getDebut(){
		return this.debut;
	}
	
	public Point getFin(){
		return this.fin;
	}
	
	public void setDebut(Point debut) {
		this.debut = debut;
	}
	
	public void setFin(Point fin) {
		this.fin = fin;
	}

}
