package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;

/**
 * Classe modélisant un outil pour faire des carré
 *
 */
public class OutilCarre extends OutilForme {

	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Le panneau Dessin de l'outil
	 */
	public OutilCarre(PanneauDessin panneauDessin) {
		super(panneauDessin);
	}
	
	/**
	 * Construit un outil lié à aucun panneau Dessin
	 */
	public OutilCarre() {
		super();
	}

	@Override
	protected VueForme creerVueForme() {
		double xDebut = this.getDebut().getX();
		double xFin = this.getFin().getX();
		double yDebut = this.getDebut().getY();
		double yFin = this.getFin().getY();
		
		double largeur = Math.abs(xDebut - xFin);
		double hauteur = Math.abs(yDebut - yFin);
		double cote = Math.max(largeur, hauteur);
		double xOrigine = (xDebut < xFin) ? xDebut : xDebut - cote;
		double yOrigine = (yDebut < yFin) ? yDebut : yDebut - cote;
		
		Carre carre = new Carre(xOrigine, yOrigine, cote);
		carre.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueCarre(carre);
	}
	
	@Override
	protected VueForme creerVueFormeDefaut() {
		Carre carre = new Carre(this.getDebut());
		carre.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueCarre(carre);
	}
	
	protected VueForme creerVueFormeFinale() {
		double xDebut = this.getDebut().getX();
		double xFin = this.getFin().getX();
		double yDebut = this.getDebut().getY();
		double yFin = this.getFin().getY();
		
		double largeur = Math.abs(xDebut - xFin);
		double hauteur = Math.abs(yDebut - yFin);
		double cote = Math.max(largeur, hauteur);
		double xOrigine = (xDebut < xFin) ? xDebut : xDebut - cote;
		double yOrigine = (yDebut < yFin) ? yDebut : yDebut - cote;
		
		Rectangle carre = new Rectangle(xOrigine, yOrigine, cote, cote);
		carre.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueRectangle(carre);
	}

}
