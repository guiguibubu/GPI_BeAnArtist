package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;
import fr.eseo.gpi.beanartist.vue.formes.VueCercle;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;


/**
 * Classe modélisant un outil pour faire des cercles
 *
 */
public class OutilCercle extends OutilForme {

	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Le panneau Dessin de l'outil
	 */
	public OutilCercle(PanneauDessin panneauDessin) {
		super(panneauDessin);
	}
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 */
	public OutilCercle() {
		super();
	}

	@Override
	protected VueForme creerVueForme() {
		double xDebut = this.getDebut().getX();
		double xFin = this.getFin().getX();
		double yDebut = this.getDebut().getY();
		double yFin = this.getFin().getY();
		
		double largeur = Math.abs(xDebut - xFin);
		double hauteur = Math.abs(yDebut - yFin);
		double cote = Math.max(largeur, hauteur);
		double xOrigine = (xDebut < xFin) ? xDebut : xDebut - cote;
		double yOrigine = (yDebut < yFin) ? yDebut : yDebut - cote;
		
		Cercle cercle = new Cercle(xOrigine, yOrigine, cote);
		cercle.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueCercle(cercle);
	}
	
	@Override
	protected VueForme creerVueFormeDefaut() {
		Cercle cercle = new Cercle(this.getDebut());
		cercle.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueCercle(cercle);
	}
	
	protected VueForme creerVueFormeFinale() {
		double xDebut = this.getDebut().getX();
		double xFin = this.getFin().getX();
		double yDebut = this.getDebut().getY();
		double yFin = this.getFin().getY();
		
		double largeur = Math.abs(xDebut - xFin);
		double hauteur = Math.abs(yDebut - yFin);
		double cote = Math.max(largeur, hauteur);
		double xOrigine = (xDebut < xFin) ? xDebut : xDebut - cote;
		double yOrigine = (yDebut < yFin) ? yDebut : yDebut - cote;
		
		Ellipse cercle = new Ellipse(xOrigine, yOrigine, cote, cote);
		cercle.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueEllipse(cercle);
	}
}
