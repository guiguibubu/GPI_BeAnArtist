package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;

/**
 * Classe modélisant un outil pour faire des ellipse
 *
 */
public class OutilEllipse extends OutilForme {

	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Le panneau Dessin de l'outil
	 */
	public OutilEllipse(PanneauDessin panneauDessin) {
		super(panneauDessin);
	}
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 */
	public OutilEllipse() {
		super();
	}

	@Override
	protected VueForme creerVueForme() {
		double xOrigine = Math.min(this.getDebut().getX(), this.getFin().getX());
		double yOrigine = Math.min(this.getDebut().getY(), this.getFin().getY());
		double largeur = Math.abs(this.getDebut().getX() - this.getFin().getX());
		double hauteur = Math.abs(this.getDebut().getY() - this.getFin().getY());
		
		Ellipse ellipse = new Ellipse(xOrigine, yOrigine, largeur, hauteur);
		ellipse.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueEllipse(ellipse);
	}
	
	@Override
	protected VueForme creerVueFormeDefaut() {
		Ellipse ellipse = new Ellipse(this.getDebut());
		ellipse.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueEllipse(ellipse);
	}

}
