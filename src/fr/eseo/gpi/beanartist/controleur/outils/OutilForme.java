package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.event.MouseEvent;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

/**
 * Classe abstraite modélisant un outil utilisé pour tracer des formes
 *
 */
public abstract class OutilForme extends Outil{
	
	protected VueForme vueFormeActuel;
	
	private boolean annulationDessin;
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Le panneau Dessin de l'outil
	 */
	public OutilForme(PanneauDessin panneauDessin) {
		super(panneauDessin);
	}
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 */
	public OutilForme() {
		super();
	}

	@Override
	public void mouseClicked(MouseEvent e){
		
		super.mousePressed(e);
		super.mouseReleased(e);
		
		if(e.getButton() == MouseEvent.BUTTON1){
			
			this.getPanneauDessin().getVueFormes().remove(this.vueFormeActuel);
			
			if(this.getPanneauDessin() != null){
				if(e.getClickCount() == 2 && !(this instanceof OutilTrace)){
					this.getPanneauDessin().ajouterVueForme(this.creerVueFormeDefaut());
					this.getPanneauDessin().repaint();
				}
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(this.getPanneauDessin() != (PanneauDessin)e.getSource()){
			this.setPanneauDessin((PanneauDessin)e.getSource());
		}
		
		this.annulationDessin = false;
		if(e.getButton() == MouseEvent.BUTTON1){
			super.mousePressed(e);
			super.mouseReleased(e);
			this.vueFormeActuel = this.creerVueForme();
			this.getPanneauDessin().ajouterVueForme(this.vueFormeActuel);
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e){ 
		
		if(e.getButton() == MouseEvent.BUTTON1 && this.getPanneauDessin() != null && !this.annulationDessin){
			super.mouseReleased(e);
			if ((Math.abs(e.getX() - super.getDebut().getX()) > 1) && ((Math.abs(e.getY() - super.getDebut().getY()) > 1)) || this instanceof OutilTrace){
				
				if(!(this instanceof OutilTrace)){
					this.vueFormeActuel.rafraichirAffichage(this.getDebut(), this.getFin());
				}
				
				if(this instanceof OutilCarre){
					this.getPanneauDessin().getVueFormes().remove(this.vueFormeActuel);
					this.getPanneauDessin().ajouterVueForme(((OutilCarre)this).creerVueFormeFinale());
				}
				
				if(this instanceof OutilCercle){
					this.getPanneauDessin().getVueFormes().remove(this.vueFormeActuel);
					this.getPanneauDessin().ajouterVueForme(((OutilCercle)this).creerVueFormeFinale());
				}
				
				this.getPanneauDessin().repaint();
			}
			else{
				this.getPanneauDessin().getVueFormes().remove(this.vueFormeActuel);
			}
			this.setDebut(null);
			this.setFin(null);
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if(e.getModifiers() == MouseEvent.BUTTON1_MASK){
			if(this.getPanneauDessin() != null){
				if(!this.annulationDessin){
					this.getFin().deplacerVers(e.getX(), e.getY());
					this.vueFormeActuel.rafraichirAffichage(this.getDebut(), this.getFin());
					this.getPanneauDessin().repaint();
				}
			}
		}
	}
	
	/**
	 * Créé la vue de la forme lié à l'outil
	 * @return La vue de la forme
	 */
	protected abstract VueForme creerVueForme();
	/**
	 * Créé la vue de la forme lié à l'outil à l'endroit du clique, avec les dimensions par défaut de la forme
	 * @return La vue de la forme
	 */
	protected abstract VueForme creerVueFormeDefaut();

}
