package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;

/**
 * Classe modélisant un outil pour faire des lignes
 *
 */
public class OutilLigne extends OutilForme {

	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Le panneau Dessin de l'outil
	 */
	public OutilLigne(PanneauDessin panneauDessin) {
		super(panneauDessin);
	}
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 */
	public OutilLigne() {
		super();
	}
	
	@Override
	protected VueForme creerVueForme() {
		double xOrigine = this.getDebut().getX();
		double yOrigine = this.getDebut().getY();
		double largeur = this.getFin().getX() - this.getDebut().getX();
		double hauteur = this.getFin().getY() - this.getDebut().getY();
		Ligne ligne = new Ligne(xOrigine, yOrigine, largeur, hauteur);
		ligne.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueLigne(ligne);
	}
	
	@Override
	protected VueForme creerVueFormeDefaut(){
		double xOrigine = this.getDebut().getX();
		double yOrigine = this.getDebut().getY();
		Ligne ligne = new Ligne(new Point(xOrigine, yOrigine));
		ligne.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueLigne(ligne);
	}

}
