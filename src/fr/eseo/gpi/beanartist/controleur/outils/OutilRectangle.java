package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;

/**
 * Classe modélisant un outil pour faire des rectangle
 *
 */
public class OutilRectangle extends OutilForme {

	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Le panneau Dessin de l'outil
	 */
	public OutilRectangle(PanneauDessin panneauDessin) {
		super(panneauDessin);
	}
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 */
	public OutilRectangle() {
		super();
	}

	@Override
	protected VueForme creerVueForme() {
		double xOrigine = Math.min(this.getDebut().getX(), this.getFin().getX());
		double yOrigine = Math.min(this.getDebut().getY(), this.getFin().getY());
		double largeur = Math.abs(this.getDebut().getX() - this.getFin().getX());
		double hauteur = Math.abs(this.getDebut().getY() - this.getFin().getY());
		
		Rectangle rectangle = new Rectangle(xOrigine, yOrigine, largeur, hauteur);
		rectangle.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueRectangle(rectangle);
	}
	
	@Override
	protected VueForme creerVueFormeDefaut() {
		Rectangle rectangle = new Rectangle(this.getDebut());
		rectangle.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueRectangle(rectangle);
	}

}
