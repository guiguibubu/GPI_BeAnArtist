package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.mouseManager.MouseManagerMain;
import fr.eseo.gpi.beanartist.vue.formes.VueCoin;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.formes.VueSelection;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

/**
 * Classe modélisant un outil pour sélectionner et agir sur les formes dessinées
 *
 */
public class OutilSelection extends Outil implements KeyListener{
	
	private VueForme vueFormeActuel;
	private VueForme vueEncadrant;
	private VueCoin vueCoinHG;
	private VueCoin vueCoinHD;
	private VueCoin vueCoinBD;
	private VueCoin vueCoinBG;
	private MouseManagerMain mouseManagerMain;
	
	private boolean commandeClavierFaite;
	
	private ArrayList<VueForme> formeSelectionnee;
	
	private boolean multiChoix;
	
	private boolean autorisationDeplacement;
	private double oldX, oldY;
	
	private boolean selectionTermine;
	
	/**
	 * Construit un outil lié à un panneau Dessin
	 * @param panneauDessin Panneau Dessin auquel est associé l'outil construit
	 */
	public OutilSelection(PanneauDessin panneauDessin) {
		super(panneauDessin);
		this.formeSelectionnee = new ArrayList<VueForme>();
	}
	
	/**
	 * Construit un outil non lié à un panneau Dessin
	 */
	public OutilSelection() {
		super();
		this.formeSelectionnee = new ArrayList<VueForme>();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		super.mousePressed(e);
		super.mouseReleased(e);
		
		if(this.vueFormeActuel != null){
			this.getPanneauDessin().getVueFormes().remove(this.vueFormeActuel);
		}
		if(!this.autorisationDeplacement){
			this.rechercheSelection();
			this.affichageSelection();
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1){
			this.selectionTermine = false;
			super.mousePressed(e);
			this.setFin(new Point(e.getX(), e.getY()));
			this.oldX = this.getDebut().getX();
			this.oldY = this.getDebut().getY();
			this.vueFormeActuel = this.creerVueForme();
			this.getPanneauDessin().getVueFormes().add(this.vueFormeActuel);
			if(!this.autorisationDeplacement){
				this.enleverEncadrant();
				this.getPanneauDessin().repaint();
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.selectionTermine = true;
		super.mouseReleased(e);
		this.getPanneauDessin().getVueFormes().remove(this.vueFormeActuel);
		this.getPanneauDessin().repaint();
		
		if(e.getButton() == MouseEvent.BUTTON1  && (Math.abs(e.getX() - super.getDebut().getX()) > 1) && (Math.abs(e.getY() - super.getDebut().getY()) > 1) && !this.autorisationDeplacement){
			this.rechercheSelection();
			this.affichageSelection();
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if(e.getModifiers() == MouseEvent.BUTTON1_MASK){
			super.mouseDragged(e);
			if(this.autorisationDeplacement){
				double dx = e.getX() - this.oldX;
				double dy = e.getY() - this.oldY;
				this.oldX = e.getX();
				this.oldY = e.getY();
				
				if(this.mouseManagerMain != null){
					if(!(this.mouseManagerMain.getLocalisation().equals("") || this.mouseManagerMain.getLocalisation().equals("CENTER"))){
						double xMin = (this.mouseManagerMain.getLocalisation().contains("W")) ? e.getX() : this.vueEncadrant.getForme().getX();
						double xMax = (this.mouseManagerMain.getLocalisation().contains("E")) ? e.getX() : this.vueEncadrant.getForme().getX() + this.vueEncadrant.getForme().getLargeur();
						double yMin = (this.mouseManagerMain.getLocalisation().contains("N")) ? e.getY() : this.vueEncadrant.getForme().getY();
						double yMax = (this.mouseManagerMain.getLocalisation().contains("S")) ? e.getY() : this.vueEncadrant.getForme().getY() + this.vueEncadrant.getForme().getHauteur();
						
						this.rafraichirNouvelEncadrant(xMin, xMax, yMin, yMax);
					}
					else if(this.mouseManagerMain.getLocalisation().equals("CENTER")){
						for(VueForme vue : this.formeSelectionnee){
							vue.getForme().deplacerDe(dx, dy);
						}
					}
				}
			}
			else {
				this.getFin().deplacerVers(e.getX(), e.getY());
				this.rechercheSelection();
				this.vueFormeActuel.rafraichirAffichage(this.getDebut(), this.getFin());
			}
			this.rafraichirAffichage();
		}
		else if (e.getModifiers() == MouseEvent.BUTTON1_MASK + KeyEvent.CTRL_MASK){
			this.getFin().deplacerVers(e.getX(), e.getY());
			this.rechercheSelection();
			this.vueFormeActuel.rafraichirAffichage(this.getDebut(), this.getFin());
			this.rafraichirAffichage();
		}
	}
	
	/**
	 * Créé la vue du rectangle de sélection
	 * @return La vue de la sélection
	 */
	private VueForme creerVueForme() {
		double xOrigine = Math.min(this.getDebut().getX(), this.getFin().getX());
		double yOrigine = Math.min(this.getDebut().getY(), this.getFin().getY());
		double largeur = Math.abs(this.getDebut().getX() - this.getFin().getX());
		double hauteur = Math.abs(this.getDebut().getY() - this.getFin().getY());
		
		Rectangle rectangleSelection = new Rectangle(xOrigine, yOrigine, largeur, hauteur);
		rectangleSelection.setCouleurLigne(Color.BLACK);
		return new VueSelection(rectangleSelection);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getModifiers() == KeyEvent.CTRL_MASK && !this.commandeClavierFaite){
			this.commandeClavierFaite = true;
			this.multiChoix = true;		
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		this.commandeClavierFaite = false;
		this.multiChoix = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}
	
	/**
	 * Affiche dans la console les formes sélectionnées, et rafraichie l'affichage
	 */
	public void affichageSelection(){
		if(this.formeSelectionnee.size() == 0){
			System.out.println("Rien de sélectionné");
			this.rafraichirAffichage();
		}
		else{
			this.rafraichirAffichage();
			for(VueForme vue : this.formeSelectionnee){
				System.out.println(vue.getForme());
			}
			
		}
		System.out.println("-----------------------------------------------");
	}
	
	/**
	 * Met, modifie ou enlève l'encadrant, selon s'il y a des forme sélectionnées ou non, puis réaffiche le panneau Dessin
	 */
	public void rafraichirAffichage(){
		
		if(this.formeSelectionnee.size() != 0 && (this.selectionTermine || this.autorisationDeplacement)){
			double xMin = this.getPanneauDessin().getWidth(), xMax = 0, yMin = this.getPanneauDessin().getHeight(), yMax = 0;
			for(VueForme vue : this.formeSelectionnee){
				xMin = (vue.getForme().getCadreMinX() < xMin) ? vue.getForme().getCadreMinX() : xMin;
				xMax = (vue.getForme().getCadreMaxX() > xMax) ? vue.getForme().getCadreMaxX() : xMax; 
				yMin = (vue.getForme().getCadreMinY() < yMin) ? vue.getForme().getCadreMinY() : yMin; 
				yMax = (vue.getForme().getCadreMaxY() > yMax) ? vue.getForme().getCadreMaxY() : yMax; 
			}
			
			this.mettreEncadrant(xMin, xMax, yMin, yMax);
		}
		else{
			this.enleverEncadrant();
		}
		this.getPanneauDessin().repaint();
	}
	
	/**
	 * Modifie la couleur du trait des formes sélectionnées
	 * @param couleur La nouvelle couleur du trait des formes sélectionnées
	 */
	public void setCouleurTrait(Color couleur){
		if(this.formeSelectionnee.size() != 0){
			for(VueForme vue : this.formeSelectionnee){
				vue.getForme().setCouleurLigne(couleur);
			}
		}
	}
	
	/**
	 * 
	 * @return formeSelectionnee, la liste des formes sélectionnées
	 */
	public ArrayList<VueForme> getFormeSelectionnee(){
		return this.formeSelectionnee;
	}	

	/**
	 * Recherche les formes à sélectionner/désélectionner selon les point de début et de fin de la sourie
	 */
	private void rechercheSelection(){
		
		//coordonnées du rectangle de recherche
		int xmin = (int) Math.min(this.getDebut().getX(), this.getFin().getX());
		int xmax = (int) Math.max(this.getDebut().getX(), this.getFin().getX());
		int ymin = (int) Math.min(this.getDebut().getY(), this.getFin().getY());
		int ymax = (int) Math.max(this.getDebut().getY(), this.getFin().getY());
		
		int x = xmin;
		int y = ymin;
		
		//On détermine si c'est un clique ou non
		int dx = xmax - xmin;
		int dy = ymax - ymin;
		
		boolean click = dx==0 && dy==0;
		
		boolean trouve = false; //indique si une forme contient le point de recherche
		boolean auMoinsUnMatch = false;	//indique si au moins un des points dans la zone de recherche est contenu dans une des formes
		
		boolean[] trouveUneFois = new boolean[this.getPanneauDessin().getVueFormes().size()]; //indique si la ième forme contient au moins un des points de la zone de recherche
				
		while((y<=ymax)){
			x = xmin;
			while((x<=xmax)){
				//Pour chaque vue vue du panneau on regarde si elle contient le point (x,y) et si elle est déja selectionnée
				for(int i=this.getPanneauDessin().getVueFormes().size()-1; i>=0; i--){
					VueForme vueForme = this.getPanneauDessin().getVueFormes().get(i);
					//on ne traite que les formes, et non les vues de l'outil de sélection
					if(!(vueForme instanceof VueSelection) && !(vueForme instanceof VueCoin)){
						trouve = vueForme.getForme().contient(x, y);
						if(!auMoinsUnMatch && trouve){
							auMoinsUnMatch = true;
						}
						if(trouve){
							trouveUneFois[i] = true;
						}
						boolean dejaSelectionne = vueForme.estSelectionne(); //indique si la forme est déjà sélectionnée
						if(trouve && !dejaSelectionne){
							//si on clique sur une forme non-sélectionnée, sans appuyer sur la touche ctrl, il faut alors d'abord désélectionner toutes les autres formes 
							if(click && !this.multiChoix){
								this.annulerSelection();
							}
							this.selectionnerForme(vueForme);
						}
						else if(trouve && dejaSelectionne && click){
							//si on clique sur une forme déja sélectionnée et qui est la seule alors il faut la désélectionner, ou qu'on appuie sur ctrl
							if(this.formeSelectionnee.size() != 1 && !this.multiChoix){
								this.annulerSelection();
								this.selectionnerForme(vueForme);
							}
							else{
								this.deselectionnerForme(vueForme);
							}
						}
					}
				}
				x++;
			}
			y++;
		}
		//si aucune forme n'est dans la zone de recherche et que la sélection est terminée (clique relachée), on désélectionne toutes les formes
		if(!auMoinsUnMatch && this.selectionTermine){
			this.annulerSelection();
		}
		//si la sélection n'est pas terminée (cliquer-gliser en cours) et qu'une forme est sélectionnée, bien qu'elle est hors de la zone de recherche, alors on la désélectionne
		if(!this.selectionTermine){
			for(int i = 0; i<trouveUneFois.length; i++){
				if(!trouveUneFois[i] && this.getPanneauDessin().getVueFormes().get(i).estSelectionne()){
					this.deselectionnerForme(this.getPanneauDessin().getVueFormes().get(i));
				}
			}
		}
	}
	
	/**
	 * Désélectionne toute les formes du panneau Dessin
	 */
	public void annulerSelection(){
		for(VueForme vue : this.getPanneauDessin().getVueFormes()){
			this.deselectionnerForme(vue);
		}
		this.formeSelectionnee.clear();
		if(this.vueEncadrant != null){
			this.getPanneauDessin().removeMouseMotionListener(this.mouseManagerMain);
			this.getPanneauDessin().setCursor(Cursor.getDefaultCursor());
		}
		this.rafraichirAffichage();
	}
	
	/**
	 * Retire toutes les formes sélectionnées du panneau dessin
	 */
	public void effacerSelection(){

		for(VueForme vue : this.formeSelectionnee){
			this.getPanneauDessin().getVueFormes().remove(vue);
		}
		this.annulerSelection();
	}
	
	/**
	 * Sélectionne toute les formes du panneau Dessin
	 */
	public void toutSelectionner(){
		for(VueForme vue : this.getPanneauDessin().getVueFormes()){
			this.selectionnerForme(vue);
		}
		this.selectionTermine = true;
		this.affichageSelection();
	}
	
	/**
	 * Sélectionne et ajoute à la liste des formes sélectionnées
	 * @param vueForme la forme à sélectionner
	 */
	
	private void selectionnerForme(VueForme vueForme){
		vueForme.setSelectionne(true);
		if(!this.formeSelectionnee.contains(vueForme)){
			this.formeSelectionnee.add(vueForme);
		}
	}
	
	/**
	 * Désélectionne et retire de la liste des formes sélectionnées
	 * @param vueForme la forme à désélectionner
	 */
	private void deselectionnerForme(VueForme vueForme){
		vueForme.setSelectionne(false);
		this.formeSelectionnee.remove(vueForme);
	}
	
	/**
	 * 
	 * Met le rectangle encadrant, et ses coins, sur le panneau Dessin
	 *
	 * @param xMin l'abscisse minimum du rectangle encadrant
	 * @param xMax l'abscisse maximum du rectangle encadrant
	 * @param yMin l'ordonnée minimum du rectangle encadrant
	 * @param yMax l'ordonnée maximum du rectangle encadrant
	 */
	private void mettreEncadrant(double xMin, double xMax, double yMin, double yMax){
		double largeur = xMax - xMin;
		double hauteur = yMax - yMin;
		
		if(!this.getPanneauDessin().getVueFormes().contains(this.vueEncadrant)){
			this.vueEncadrant = new VueSelection(new Rectangle(xMin, yMin, xMax - xMin, yMax - yMin));
			this.vueCoinHG = new VueCoin(new Carre(xMin - VueCoin.cote/2, yMin - VueCoin.cote/2, VueCoin.cote), "NW");
			this.vueCoinHG.getForme().setCouleurRemplissage(Color.WHITE);
			this.vueCoinHD = new VueCoin(new Carre((xMin - VueCoin.cote/2) + largeur, yMin - VueCoin.cote/2, VueCoin.cote), "NE");
			this.vueCoinHD.getForme().setCouleurRemplissage(Color.WHITE);
			this.vueCoinBD = new VueCoin(new Carre((xMin - VueCoin.cote/2) + largeur, (yMin - VueCoin.cote/2) + hauteur, VueCoin.cote), "SE");
			this.vueCoinBD.getForme().setCouleurRemplissage(Color.WHITE);
			this.vueCoinBG = new VueCoin(new Carre(xMin - VueCoin.cote/2, (yMin - VueCoin.cote/2) + hauteur, VueCoin.cote), "SW");
			this.vueCoinBG.getForme().setCouleurRemplissage(Color.WHITE);
			
			this.mouseManagerMain = new MouseManagerMain(this.getPanneauDessin(), this.vueCoinHG, this.vueCoinHD, this.vueCoinBD, this.vueCoinBG);
			
			this.getPanneauDessin().ajouterVueForme(this.vueEncadrant);
			this.getPanneauDessin().ajouterVueForme(this.vueCoinHG);
			this.getPanneauDessin().ajouterVueForme(this.vueCoinHD);
			this.getPanneauDessin().ajouterVueForme(this.vueCoinBD);
			this.getPanneauDessin().ajouterVueForme(this.vueCoinBG);
			this.getPanneauDessin().addMouseMotionListener(this.mouseManagerMain);
		}
		else{
			this.vueEncadrant.rafraichirAffichage(new Point(xMin, yMin), new Point(xMax, yMax));
			this.vueCoinHG.rafraichirAffichage(new Point(xMin, yMin));
			this.vueCoinHD.rafraichirAffichage(new Point(xMax, yMin));
			this.vueCoinBD.rafraichirAffichage(new Point(xMax, yMax));
			this.vueCoinBG.rafraichirAffichage(new Point(xMin, yMax));
		}
	}
	
	/**
	 * Enlève le rectangle encadrant, et ses coins, du panneau Dessin
	 */
	private void enleverEncadrant(){
		this.getPanneauDessin().getVueFormes().remove(this.vueEncadrant);
		this.getPanneauDessin().getVueFormes().remove(this.vueCoinHG);
		this.getPanneauDessin().getVueFormes().remove(this.vueCoinHD);
		this.getPanneauDessin().getVueFormes().remove(this.vueCoinBD);
		this.getPanneauDessin().getVueFormes().remove(this.vueCoinBG);
	}
	
	/**
	 * Modifie la largeur des formes sélectionnée
	 * @param largeur la nouvelle largeur de l'ensemble des formes sélectionnées
	 */
	
	private void setLargeurSelection(double largeur){
		double oldLargeur = this.vueEncadrant.getForme().getLargeur();
		if(oldLargeur > 0 && largeur > 0){
			double ratio = largeur/oldLargeur;
	 		for (VueForme vue : this.formeSelectionnee){
	 			double distanceAZero = vue.getForme().getCadreMinX() - this.vueEncadrant.getForme().getX();
 				double newX = ratio*distanceAZero + this.vueEncadrant.getForme().getX();
 				if(vue instanceof VueLigne && ((Ligne)vue.getForme()).getLargeur() < 0){
 					newX += Math.abs(vue.getForme().getLargeur());
 				}
 				vue.getForme().setX(newX);
 				vue.getForme().setLargeur(ratio*vue.getForme().getLargeur());
 			}
		}
		else{
			for (VueForme vue : this.formeSelectionnee){
				vue.getForme().setX(this.vueEncadrant.getForme().getX());
				vue.getForme().setLargeur(1);
			}
		}
	}
	
	/**
	 * Modifie la hauteur des formes sélectionnée
	 * @param hauteur la nouvelle hauteur de l'ensemble des formes sélectionnées
	 */
	
	private void setHauteurSelection(double hauteur){
		double oldHauteur = this.vueEncadrant.getForme().getHauteur();
		if(oldHauteur > 0 && hauteur > 0){
			double ratio = hauteur/oldHauteur;
	 		for (VueForme vue : this.formeSelectionnee){
	 			double distanceAZero = vue.getForme().getCadreMinY() - this.vueEncadrant.getForme().getY();
 				double newY = ratio*distanceAZero + this.vueEncadrant.getForme().getY();
 				if(vue instanceof VueLigne && ((Ligne)vue.getForme()).getHauteur() < 0){
 					newY += vue.getForme().getHauteur();
 				}
 				vue.getForme().setY(newY);
 				vue.getForme().setHauteur(ratio*vue.getForme().getHauteur());
			}
		}
		else{
			for (VueForme vue : this.formeSelectionnee){
				vue.getForme().setY(this.vueEncadrant.getForme().getY());
				vue.getForme().setHauteur(1);
			}
		}
	}
	
	/**
	 * Modifie la position des formes sélectionnée
	 * @param x l'abscisse du nouveau point de repaire de l'ensemble des formes sélectionnées
	 * @param y l'ordonnée du nouveau point de repaire de l'ensemble des formes sélectionnées
	 */
	private void setPositionSelection(double xMin , double xMax, double yMin, double yMax){		
		double oldX;
		double oldY;
		double dx;
		double dy;
		
		for (VueForme vue : this.formeSelectionnee){
			if(vue instanceof VueLigne){
				Ligne ligne = (Ligne)vue.getForme();
				oldX = (ligne.getP1().getX() < ligne.getP2().getX()) ? this.vueEncadrant.getForme().getX() : this.vueEncadrant.getForme().getX()+this.vueEncadrant.getForme().getLargeur();
				oldY = (ligne.getP1().getY() < ligne.getP2().getY()) ? this.vueEncadrant.getForme().getY() : this.vueEncadrant.getForme().getY()+this.vueEncadrant.getForme().getHauteur();
				dx = (ligne.getP1().getX() < ligne.getP2().getX()) ? xMin - oldX : xMax - oldX;
				dy = (ligne.getP1().getY() < ligne.getP2().getY()) ? yMin - oldY : yMax - oldY;
			}
			else{
				oldX = this.vueEncadrant.getForme().getX();
				oldY = this.vueEncadrant.getForme().getY();
				dx = xMin - oldX;
				dy = yMin - oldY;
			}
			vue.getForme().deplacerDe(dx, dy);
		}		
	}
	
	/**
	 * Modifie la position, la largeur et la hauteur des formes sélectionnées pour qu'elles soient dans le nouveau encadrant
	 * @param xMin l'abscisse minimum du nouvelle encadrant
	 * @param xMax l'abscisse maximum du nouvelle encadrant
	 * @param yMin l'ordonnée minimum du nouvelle encadrant
	 * @param yMax l'ordonnée maximum du nouvelle encadrant
	 */
	private void rafraichirNouvelEncadrant(double xMin, double xMax, double yMin, double yMax){
		this.setLargeurSelection(xMax - xMin);
		this.setHauteurSelection(yMax - yMin);
		this.setPositionSelection(xMin, xMax, yMin, yMax);
		this.mettreEncadrant(xMin, xMax, yMin, yMax);
		this.getPanneauDessin().repaint();
	}

	/**
	 * 
	 * @return mouseManagerMain Le MouseManagerMain de l'outil
	 */
	public MouseManagerMain getMouseManagerMain() {
		return this.mouseManagerMain;
	}
	
	/**
	 * 
	 * @param deplacement Indique si l'outil a l'autorisation d'agir sur les formes
	 */
	
	public void autoriserDeplacement(boolean deplacement){
		this.autorisationDeplacement = deplacement;
		if(deplacement){
			this.getPanneauDessin().addMouseMotionListener(this.mouseManagerMain);
		}
		else{
			 this.getPanneauDessin().removeMouseMotionListener(this.mouseManagerMain);
		}
	}
	
	/**
	 * 
	 * @return autorisationDeplacement Indique si l'outil a l'autorisation d'agir sur les formes
	 */
	public boolean isAutoriseDeplacer(){
		return this.autorisationDeplacement;
	}
}
