package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueTrace;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

/**
 * Classe modélisant un outil pour faire des tracé à main levée
 *
 */
public class OutilTrace extends OutilForme {

	public OutilTrace(PanneauDessin panneauDessin) {
		super(panneauDessin);
	}
	
	public OutilTrace() {
		super();
	}
	
	@Override
	protected VueForme creerVueForme() {
		Trace trace = new Trace(new Point(this.getDebut().getX(), this.getDebut().getY()));
		trace.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueTrace(trace);
	}
	
	@Override
	protected VueForme creerVueFormeDefaut() {
		Trace trace = new Trace(new Point(this.getDebut().getX(), this.getDebut().getY()));
		trace.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return new VueTrace(trace);
	}
}
