package fr.eseo.gpi.beanartist.modele.formes;
import fr.eseo.gpi.beanartist.modele.formes.Point;;

/**
 * Classe représentant une Carre
 * @author Guibu
 */
public class Carre extends Rectangle{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final double COTE_PAR_DEFAUT = Forme.LARGEUR_PAR_DEFAUT;
	
	//Constructeur
	/**
	 * Crée une Carre
	 * @param position La position du Carre
	 * @param cote Le cote du Carre
	 */
	public Carre(Point position, double cote){
		super(position, cote, cote);
	}
	
	/**
	 * Crée une Carre
	 * @param x l'abscisse de la position du Carre
	 * @param y l'ordonnée de la position du Carre
	 * @param cote Le côté du Carre
	 */
	public Carre(double x, double y, double cote){
		this(new Point(x,y), cote);
	}
	
	/**
	 * Crée une Carre
	 * @param position La position du Carre
	 * 
	 * Le côté est celui par défaut: {@value #COTE_PAR_DEFAUT}
	 */
	public Carre(Point position){
		this(position, Carre.COTE_PAR_DEFAUT);
	}
	/**
	 * Crée une Carre en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT})
	 * @param cote Le côté du Carre
	 * 
	 */
	public Carre(double cote){
		this(new Point(), cote);
	}
	/**
	 * Crée une Carre [{@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}] en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}) 
	 *
	 * Le côté est celui par défaut: {@value #COTE_PAR_DEFAUT}
	 */
	public Carre(){
		this(new Point(), Carre.COTE_PAR_DEFAUT);
	}
	/**
	 * Modifie le côté du Carre
	 * @param cote Le nouveau côté du Carre
	 */
	public void setCote(double cote){
		super.setLargeur(cote);
		super.setHauteur(cote);
	}
	/**
	 * Modifie la largeur du Carre
	 * @param largeur La nouvelle largeur du Carre
	 */
	public void setLargeur(double largeur){
			this.setCote(largeur);
	}
	/**
	 * Modifie la hauteur du Carre
	 * @param hauteur La nouvelle hauteur du Carre
	 */
	public void setHauteur(double hauteur){
			this.setCote(hauteur);
	}
	/**
	 * Renvoie le côté du Carre
	 * @return Le côté du Carre
	 */
	public double getCote(){
		return this.getLargeur();
	}
}
