package fr.eseo.gpi.beanartist.modele.formes;
import fr.eseo.gpi.beanartist.modele.formes.Point;;

/**
 * Classe représentant une Cercle
 * @author Guibu
 */
public class Cercle extends Ellipse{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final double DIAMETRE_PAR_DEFAUT = Forme.LARGEUR_PAR_DEFAUT;
	
	//Constructeur
	/**
	 * Crée une Cercle
	 * @param position La position du Cercle
	 * @param diametre Le diamètre du Cercle
	 */
	public Cercle(Point position, double diametre){
		super(position, diametre, diametre);
	}
	
	/**
	 * Crée une Cercle
	 * @param x l'abscisse de la position du Cercle
	 * @param y l'ordonnée de la position du Cercle
	 * @param diametre Le diamètre du Cercle
	 */
	public Cercle(double x, double y, double diametre){
		this(new Point(x,y), diametre);
	}
	
	/**
	 * Crée une Cercle
	 * @param position La position du Cercle
	 * 
	 * Le rayon est celui par défaut: {@value #DIAMETRE_PAR_DEFAUT}
	 */
	public Cercle(Point position){
		this(position, Cercle.DIAMETRE_PAR_DEFAUT);
	}
	/**
	 * Crée une Cercle en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT})
	 * @param diametre Le diamètre du Cercle
	 * 
	 */
	public Cercle(double diametre){
		this(new Point(), diametre);
	}
	/**
	 * Crée une Cercle [{@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}] en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}) 
	 *
	 * Le rayon est celui par défaut: {@value #DIAMETRE_PAR_DEFAUT}
	 */
	public Cercle(){
		this(new Point(), Cercle.DIAMETRE_PAR_DEFAUT);
	}
	/**
	 * Modifie le rayon du Cercle
	 * @param rayon Le nouveau rayon du Cercle
	 */
	public void setRayon(double rayon){
			super.setLargeur(rayon*2);
			super.setHauteur(rayon*2);
	}
	/**
	 * Modifie la largeur du Cercle
	 * @param largeur La nouvelle largeur du Cercle
	 */
	public void setLargeur(double largeur){
			this.setRayon(largeur/2);
	}
	/**
	 * Modifie la hauteur du Cercle
	 * @param hauteur La nouvelle hauteur du Cercle
	 */
	public void setHauteur(double hauteur){
			this.setRayon(hauteur/2);
	}
	/**
	 * Renvoie le rayon du Cercle
	 * @return Le rayon du Cercle
	 */
	public double getRayon(){
		return this.getLargeur()/2;
	}
	/**
	 * Calcule le périmètre du Cercle
	 * @return Le périmètre du Cercle
	 */
	public double perimetre(){
		return 2*Math.PI*this.getRayon();
	}
}
