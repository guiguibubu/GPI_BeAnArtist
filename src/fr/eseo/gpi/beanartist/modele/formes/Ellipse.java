package fr.eseo.gpi.beanartist.modele.formes;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.util.Locale;

import fr.eseo.gpi.beanartist.modele.formes.Point;;

/**
 * Classe représentant une Ellipse
 * @author Guibu
 */
public class Ellipse extends Forme{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Attributs
	//demi-grand axe
	private double a;
	//demi-petit axe
	private double b;
	//Distance centre/foyer
	private double c;
	//Foyers et Centre
	protected Point centre;
	private Point foyer1;
	private Point foyer2;
	
	//Constructeur
	/**
	 * Crée une Ellipse
	 * @param pointOrigine Le Point d'origine de l'Ellipse
	 * @param largeur La largeur de l'Ellipse
	 * @param hauteur La hauteur de l'Ellipse
	 */
	public Ellipse(Point pointOrigine, double largeur, double hauteur) throws IllegalArgumentException {
		super(pointOrigine, Math.abs(largeur), Math.abs(hauteur));
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
	}
	
	/**
	 * Crée une Ellipse
	 * @param x l'abscisse du Point d'origine de l'Ellipse
	 * @param y l'ordonnée du Point d'origine de l'Ellipse
	 * @param largeur La largeur de l'Ellipse
	 * @param hauteur La hauteur de l'Ellipse
	 */
	public Ellipse(double x, double y, double largeur, double hauteur) throws IllegalArgumentException {
		this(new Point(x, y), Math.abs(largeur), Math.abs(hauteur));
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
	}
	
	/**
	 * Crée une Ellipse
	 * @param pointOrigine Le Point d'origine du Ellipse
	 * 
	 * La largeur est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}
	 * La hauteur est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}
	 */
	public Ellipse(Point pointOrigine){
		this(pointOrigine, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	/**
	 * Crée une Ellipse
	 * @param largeur La largeur du Ellipse
	 * @param hauteur La hauteur du Ellipse
	 * 
	 * Le Point d'origine est celui par défaut en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT})
	 */
	public Ellipse(double largeur, double hauteur) throws IllegalArgumentException {
		this(new Point(), Math.abs(largeur), Math.abs(hauteur));
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
	}
	/**
	 * Crée une Ellipse [{@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}] en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}) 
	 */
	public Ellipse(){
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	/**
	 * Modifie la largeur de l'Ellipse
	 * @param largeur La nouvelle largeur de l'Ellipse
	 */
	public void setLargeur(double largeur) throws IllegalArgumentException {
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		else{
			super.setLargeur(largeur);
		}
	}
	/**
	 * Modifie la hauteur de l'Ellipse
	 * @param hauteur La nouvelle hauteur de l'Ellipse
	 */

	public void setHauteur(double hauteur) throws IllegalArgumentException {
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
		else{
			super.setHauteur(hauteur);
		}
	}
	/**
	 * Calcule le périmètre du Ellipse
	 * @return Le périmètre du Ellipse
	 */
	public double perimetre(){
		double a = this.getHauteur()/2;
		double b = this.getLargeur()/2;
		double h = Math.pow((a-b)/(a+b), 2);
		return Math.PI*(a+b)*(1+(3*h)/(10+Math.sqrt(4-3*h)));
	}
	/**
	 * Calcule l'aire du Ellipse
	 * @return L'aire du Ellipse
	 */
	public double aire(){
		return Math.PI*(this.getLargeur()/2)*(this.getHauteur()/2);
	}
	
	private void calculFoyers(){
		this.a = (this.getHauteur() > this.getLargeur()) ? this.getHauteur()/2 : this.getLargeur()/2;
		this.b = (this.getHauteur() < this.getLargeur()) ? this.getHauteur()/2 : this.getLargeur()/2;
		this.c = Math.sqrt(Math.pow(a, 2) - Math.pow(b, 2));
		this.centre = new Point(this.getPosition().getX()+this.getLargeur()/2,this.getPosition().getY()+this.getHauteur()/2);
		if(this.getLargeur() > this.getHauteur()){
			this.foyer1 = new Point(centre.getX()+c,centre.getY());
			this.foyer2 = new Point(centre.getX()-c,centre.getY());
		}
		else{
			this.foyer1 = new Point(centre.getX(),centre.getY()+c);
			this.foyer2 = new Point(centre.getX(),centre.getY()-c);
		}
	}
	
	public boolean contient(Point point){
		return this.contient(point.getX(), point.getY());
	}
	
	public boolean contient(double x, double y){
		this.calculFoyers();
		return this.distanceBifocale(x, y) - 2*this.a < Forme.EPSILON;
	}
	private double distanceBifocale(double x, double y){
		return Forme.distance(this.foyer1, new Point(x,y)) + Forme.distance(this.foyer2, new Point(x,y));
	}
	
	/**
	 * Renvoie la fiche d'identité du Ellipse
	 * @return La fiche d'identité du Ellipse
	 */
	public String toString(){
		String identite = new String();
		DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.getDefault()));
		String valeurAire = decimalFormat.format(this.aire(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		String valeurPerimetre = decimalFormat.format(this.perimetre(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		identite = super.toString()+" périmètre : "+valeurPerimetre+" aire : "+valeurAire;
		return identite;
	}
}
