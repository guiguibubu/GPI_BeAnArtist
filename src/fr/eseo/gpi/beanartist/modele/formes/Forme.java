package fr.eseo.gpi.beanartist.modele.formes;

import java.awt.Color;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Classe abstraite représentant un Forme
 * @author Guibu
 */
public abstract class Forme implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Précision des calculs
	 */
	public static final double EPSILON = 1e-7;
	
	// Constante de Classe
	
	/**
	 * Largeur par défaut d'une Forme
	 */
	public static final double LARGEUR_PAR_DEFAUT = 20;
	
	/**
	 * Hauteur par defaut d'une Forme
	 */
	public static final double HAUTEUR_PAR_DEFAUT = 10;
	
	public static final Color COULEUR_LIGNE_PAR_DEFAUT = new Color(51,51,51);
	
	public static final float EPAISSEUR_BORDURE_PAR_DEFAUT = 1.0f;
	
	//Attribut
	
	/**
	 * Point d'origine de la Forme
	 */
	private Point position;
	
	/**
	 * Largeur de la Forme
	 */
	private double largeur;
	
	/**
	 * Hauteur de la Forme
	 */
	private double hauteur;
	
	private Color couleurLigne;
	private Color couleurRemplissage;
	private double epaisseurBordure;
	
	//Constructeur
	
	/**
	 * Crée une Forme
	 * @param position Le Point d'origine de la Point
	 * @param largeur La largeur de la Forme
	 * @param hauteur La hauteur de la Forme
	 */
	public Forme(Point position, double largeur, double hauteur){
		this.position = new Point(position.getX(), position.getY());
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.couleurLigne = Forme.COULEUR_LIGNE_PAR_DEFAUT;
		this.epaisseurBordure = Forme.EPAISSEUR_BORDURE_PAR_DEFAUT;
	}
	
	/**
	 * Crée une Forme
	 * @param x l'abscisse du Point d'origine de la Forme
	 * @param y l'ordonnée du Point d'origine de la Forme
	 * @param largeur La largeur de la Forme
	 * @param hauteur La hauteur de la Forme
	 */
	public Forme(double x, double y, double largeur, double hauteur){
		this(new Point(x, y), largeur, hauteur);
	}
	
	/**
	 * Crée une Forme
	 * @param position Le Point d'origine de la Forme
	 * 
	 * La largeur est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}
	 * La hauteur est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}
	 */
	public Forme(Point position){
		this(position, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	/**
	 * Crée une Forme
	 * @param largeur La largeur de la Forme
	 * @param hauteur La hauteur de la Forme
	 * 
	 * Le Point d'origine est celui par défaut en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT})
	 */
	public Forme(double largeur, double hauteur){
		this(new Point(), largeur, hauteur);
	}
	/**
	 * Crée une Forme [{@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}] en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}) 
	 */
	public Forme(){
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	
	//Modificateur
	/**
	 * Modifie les coordonnées du Point d'origine
	 * @param x La nouvelle abscisse du Point d'origine de la Forme
	 * @param y La nouvelle ordonnée du Point d'origine de la Forme
	 */
	public void setPosition(double x, double y){
		this.position = new Point(x, y);
	}
	/**
	 * Affecte un nouveau Point d'origine à la Forme
	 * @param point Le nouveau d'origine
	 */
	public void setPosition(Point point){
		this.position = new Point(point.getX(), point.getY());
	}
	/**
	 * Modifie l'abscisse du Point d'origine de la Forme 
	 * @param x La nouvelle abscisse du Point d'origine de la Forme 
	 */
	public void setX(double x){
		this.position.setX(x);
	}
	/**
	 * Modifie l'ordonnées du Point d'origine de la Forme 
	 * @param y La nouvelle ordonnée du Point d'origine de la Forme 
	 */
	public void setY(double y){
		this.position.setY(y);
	}
	/**
	 * Modifie la largeur de la Forme
	 * @param largeur La nouvelle largeur de la Forme
	 */
	public void setLargeur(double largeur){
		this.largeur = largeur;
	}
	/**
	 * Modifie la hauteur de la Forme
	 * @param hauteur La nouvelle hauteur de la Forme
	 */
	public void setHauteur(double hauteur){
		this.hauteur = hauteur;
	}
	
	public void setCouleurLigne(Color couleur){
		this.couleurLigne = couleur;
	}
	
	public void setCouleurRemplissage(Color couleur){
		this.couleurRemplissage = couleur;
	}
	
	//Accesseur
	/**
	 * Renvoie l'abscisse du Point d'origine
	 * @return L'abscisse du Point d'origine 
	 */
	public double getX(){
		return this.position.getX();
	}
	/**
	 * Renvoie l'ordonnée du Point d'origine
	 * @return L'ordonnée du Point d'origine 
	 */
	public double getY(){
		return this.position.getY();
	}
	/**
	 * Renvoie le Point d'origine de la Forme
	 * @return Le Point d'origine de la Forme
	 */
	public Point getPosition(){
		return this.position;
	}
	/**
	 * Renvoie la largeur de la Forme
	 * @return La largeur de la Forme
	 */
	public double getLargeur(){
		return this.largeur;
	}
	
	/**
	 * Renvoie la hauteur de la Forme
	 * @return La hauteur de la Forme
	 */
	public double getHauteur(){
		return this.hauteur;
	}
	
	public Color getCouleurLigne(){
		return this.couleurLigne;
	}
	
	public Color getCouleurRemplissage(){
		return this.couleurRemplissage;
	}
	
	//Methode
	/**
	 * Déplace la Forme en <b>(x,y)</b>
	 * @param x La nouvelle abscisse de la Forme
	 * @param y La nouvelle ordonnée de la Forme
	 */
	public void deplacerVers(double x, double y){
		this.position.deplacerVers(x, y);
	}
	/**
	 * Déplace le Forme de <b>dx</b> et de <b>dy</b>
	 * @param dx Le déplacement selon l'axe des abscisses
	 * @param dy La déplacement selon l'axe des ordonnées
	 */
	public void deplacerDe(double dx, double dy){
		this.position.deplacerDe(dx, dy);
	}
	/**
	 * Augmente/Diminue la largeur de la Forme de <b>dL</b>
	 * @param dL La variation de largeur
	 */
	public void variationLargeur(double dL){
		this.largeur += dL;
	}
	/**
	 * Augmente/Diminue la hauteur de la Forme de <b>dH</b>
	 * @param dH La variation de hauteur
	 */
	public void variationHauteur(double dH){
		this.hauteur += dH;
	}
	/**
	 * Renvoie l'abscisse la plus basse de la Forme
	 * @return L'abscisse la plus basse de la Forme
	 */
	public double getCadreMinX(){
		return (this.largeur < 0) ? this.position.getX()+this.largeur: this.position.getX();
	}
	/**
	 * Renvoie l'abscisse la plus élevée de la Forme
	 * @return L'abscisse la plus élevée de la Forme
	 */
	public double getCadreMaxX(){
		return (this.largeur > 0) ? this.position.getX()+this.largeur: this.position.getX();
	}
	/**
	 * Renvoie l'ordonnée la plus basse de la Forme
	 * @return L'ordonnée la plus basse de la Forme
	 */
	public double getCadreMinY(){
		return (this.hauteur < 0) ? this.position.getY()+this.hauteur: this.position.getY();
	}
	/**
	 * Renvoie l'ordonnée la plus élevée de la Forme
	 * @return L'ordonnée la plus élevée de la Forme
	 */
	public double getCadreMaxY(){
		return (this.hauteur > 0) ? this.position.getY()+this.hauteur: this.position.getY();
	}
	
	/**
	 * Calcule le périmètre de la Forme
	 * @return Le périmètre de la Forme
	 */
	public abstract double perimetre();
	
	/**
	 * Calcule l'aire de la Forme
	 * @return L'aire de la Forme
	 */
	public abstract double aire();
	
	/**
	 * Indique si un Point appartient à la Forme
	 * 
	 * @param position Le Point dont on veut vérifier l'appartenance à la Forme
	 * @return Indique la position appartient à la Forme
	 */
	public abstract boolean contient(Point position);
	
	/**
	 * Indique si un Point appartient à la Forme
	 * 
	 * @param x L'abscisse du point dont on veut vérifier l'appartenance à la Forme
	 * @param y L'ordonné du point dont on veut vérifier l'appartenance à la Forme
	 * @return Indique la position appartient à la Forme
	 */
	public abstract boolean contient(double x, double y);
	
	/**
	 * Calcule la distance entre deux Points P1 et P2
	 * @return La distance entre les Points P1 et P2
	 */
	protected static double distance(Point p1, Point p2){
		return Math.sqrt(Math.pow(p1.getX()-p2.getX(), 2)+Math.pow(p1.getY()-p2.getY(), 2)); 
	}
	
	/**
	 * Renvoie la fiche d'identité de la Forme
	 * @return La fiche d'identité de la Forme
	 */
	public String toString(){
		String identite = new String();
		DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.getDefault()));
		String valeurPoint = this.getPosition().toString();
		String valeurLargeur = decimalFormat.format(this.getLargeur(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		String valeurHauteur = decimalFormat.format(this.getHauteur(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		identite = "["+this.getClass().getSimpleName()+"] pos "+valeurPoint+" dim "+valeurLargeur+" x "+valeurHauteur;
		return identite;
	}

	public double getEpaisseurBordure() {
		return this.epaisseurBordure;
	}

	public void setEpaisseurBordure(double epaisseurBordure) {
		this.epaisseurBordure = epaisseurBordure;
	}
}
