package fr.eseo.gpi.beanartist.modele.formes;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Classe représentant une Ligne
 * 
 * @author buchlegu
 *
 */
public class Ligne extends Forme {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Précision des calculs
	 */
	public static final double EPSILON = 1e-5;
	
	//Attributs
	/**
	 * Point d'origine de la Ligne
	 */
	private Point p1;
	
	/**
	 * Point d'arrivé de la Ligne
	 */
	private Point p2;
	
	/**
	 * Créé une Ligne entre deux points
	 * 
	 * @param position La position d'origine de la ligne
	 * @param largeur  La largeur de la Ligne selon les abscisses 
	 * @param hauteur  La hauteur de la Ligne selon les ordonnées
	 */
	public Ligne(Point position, double largeur, double hauteur) {
		super(position, largeur, hauteur);
		this.p1 = new Point(position.getX(), position.getY());
		this.p2 = new Point(p1.getX()+largeur, p1.getY()+hauteur);
	}
	
	/**
	 * Créé une Ligne entre deux points
	 * @param x	L'abscisse du point d'origine
	 * @param y L'ordonnée du point d'origine
	 * @param largeur La largeur de la Ligne selon les abscisses 
	 * @param hauteur La hauteur de la Ligne selon les ordonnées
	 */
	public Ligne(double x, double y, double largeur, double hauteur) {
		this(new Point(x, y), largeur, hauteur);
	}
	
	/**
	 * Créé une Ligne entre deux points
	 * 
	 * @param position La position d'origine de la ligne
	 * 
	 * La largeur de la Ligne selon les abscisses est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}
	 * La hauteur de la Ligne selon les ordonnées est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}
	 */
	public Ligne(Point position) {
		this(position, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	
	/**
	 * Créé une Ligne entre deux points
	 * 
	 * @param p1 Le Point de départ
	 * @param p2 Le Point d'arrivé
	 */
	public Ligne(Point p1, Point p2) {
		this(p1, p2.getX()-p1.getX(), p2.getY()-p1.getY());
	}
	
	/**
	 * Créé une Ligne entre deux points
	 * 
	 * @param largeur La largeur de la Ligne selon les abscisses 
	 * @param hauteur La hauteur de la Ligne selon les ordonnées
	 * 
	 * L'abscisse du point d'origine est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}
	 * L'ordonnée du point d'origine est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}
	 */
	public Ligne(double largeur, double hauteur) {
		this(new Point(), largeur, hauteur);
	}
	
	/**
	 * Créé une Ligne entre deux points
	 * 
	 * L'abscisse du point d'origine est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}
	 * L'ordonnée du point d'origine est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}
	 * La largeur de la Ligne selon les abscisses est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}
	 * La hauteur de la Ligne selon les ordonnées est celle par défaut : {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT} 
	 */
	public Ligne() {
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	
	/**
	 * Renvoie le périmètre de la Ligne, donc sa longueur
	 */
	public double perimetre(){
		return Forme.distance(this.p1, this.p2);
	}
	
	/**
	 * Renvoie l'aire de la Ligne, qui est nécessairement nulle
	 */
	public double aire(){
		return 0;
	}
	
	/**
	 * Renvoie le Point d'origine de la Ligne
	 * 
	 * @return Le Point d'origine de la Ligne
	 */
	public Point getP1(){
		return this.p1;
	}
	
	/**
	 * Renvoie le Point d'arrivé de la Ligne
	 * 
	 * @return Le Point d'arrivé de la Ligne
	 */
	public Point getP2(){
		return this.p2;
	}
	
	/**
	 * Modifie l'abscisse du Point d'origine de la Ligne 
	 * @param x La nouvelle abscisse du Point d'origine de la Ligne
	 */
	public void setX(double x){
		super.setX(x);
		this.p1.setX(x);
	}
	/**
	 * Modifie l'ordonnées du Point d'origine de la Ligne 
	 * @param y La nouvelle ordonnée du Point d'origine de la Ligne 
	 */
	public void setY(double y){
		super.setY(y);
		this.p1.setY(y);
	}
	
	/**
	 * Modifie les coordonnées du Point d'origine
	 * @param x La nouvelle abscisse du Point d'origine de la Ligne
	 * @param y La nouvelle ordonnée du Point d'origine de la Ligne
	 */
	public void setPosition(double x, double y){
		this.getPosition().deplacerVers(x, y);
		this.p1.deplacerVers(x, y);
	}

	/**
	 * Déplace le point d'origine
	 * 
	 * @param point Le nouveau Point d'origine
	 */
	public void setPosition(Point point){
		super.setPosition(point.getX(), point.getX());
		this.setP1(point);
	}
	/**
	 * Déplace le point d'origine
	 * 
	 * @param point Le nouveau Point d'origine
	 */
	public void setP1(Point point){
		this.p1.deplacerVers(point.getX(), point.getY());
		//On met à jour les donnés de l'objet en temps que Forme
		super.setPosition(p1);
		super.setLargeur(p2.getX()-p1.getX());
		super.setHauteur(p2.getY()-p1.getY());
	}
	
	/**
	 * Déplace le point d'arrivé
	 * 
	 * @param point Le nouveau Point d'arrivé
	 */
	public void setP2(Point point){
		this.setP2(point.getX(), point.getY());
	}
	
	/**
	 * Déplace le point d'arrivé
	 * 
	 * @param x La nouvelle abscisse du Point d'arrivé de la Ligne
	 * @param y La nouvelle ordonnée du Point d'arrivé de la Ligne
	 */
	public void setP2(double x, double y){
		this.p2.deplacerVers(x, y);
		//On met à jour les donnés de l'objet en temps que Forme
		super.setLargeur(p2.getX()-p1.getX());
		super.setHauteur(p2.getY()-p1.getY());
	}
	/**
	 * Déplace la Ligne en <b>(x,y)</b>
	 * @param x La nouvelle abscisse de la Ligne
	 * @param y La nouvelle ordonnée de la Ligne
	 */
	public void deplacerVers(double x, double y){
		super.deplacerVers(x, y);
		this.p1.deplacerVers(x, y);
		this.p2.deplacerVers(x + this.getLargeur(), y+ this.getHauteur());
	}
	/**
	 * Déplace la Ligne de <b>dx</b> et de <b>dy</b>
	 * @param dx Le déplacement selon l'axe des abscisses
	 * @param dy La déplacement selon l'axe des ordonnées
	 */
	public void deplacerDe(double dx, double dy){
		super.deplacerDe(dx, dy);
		this.p1.deplacerDe(dx, dy);
		this.p2.deplacerDe(dx, dy);
	}
	/**
	 * Déplace le point d'arrivé afin que la Ligne soit à la largeur voulue
	 * 
	 * @param largeur La nouvelle Largeur
	 */
	public void setLargeur(double largeur){
		this.setP2(new Point(this.getP1().getX() + largeur, this.getP2().getY()));
	}
	
	/**
	 * Déplace le point d'arrivé afin que la Ligne soit à la largeur voulue
	 * 
	 * @param hauteur La nouvelle hauteur
	 */
	public void setHauteur(double hauteur){
		this.setP2(new Point(this.getP2().getX(), this.getP1().getY()+ hauteur));
	}
	
	/**
	 * Renvoie l'abscisse la plus basse de la Ligne
	 * @return L'abscisse la plus basse de la Ligne
	 */
	public double getCadreMinX(){
		return (this.p1.getX() < this.p2.getX()) ? this.p1.getX() : this.p2.getX();
	}
	
	/**
	 * Renvoie l'abscisse la plus élevée de la Ligne
	 * @return L'abscisse la plus élevée de la Ligne
	 */
	public double getCadreMaxX(){
		return (this.p1.getX() > this.p2.getX()) ? this.p1.getX() : this.p2.getX();
	}
	
	/**
	 * Renvoie l'ordonnée la plus basse de la Ligne
	 * @return L'ordonnée la plus basse de la Ligne
	 */
	public double getCadreMinY(){
		return (this.p1.getY() < this.p2.getY()) ? this.p1.getY() : this.p2.getY();
	}
	
	/**
	 * Renvoie l'ordonnée la plus élevée de la Ligne
	 * @return L'ordonnée la plus élevée de la Ligne
	 */
	public double getCadreMaxY(){
		return (this.p1.getY() > this.p2.getY()) ? this.p1.getY() : this.p2.getY();
	}
	
	/**
	 * Indique si la Ligne contient le Point
	 * 
	 * @param point Le point dont on veut savoir s'il appartient à la Ligne
	 */
	public boolean contient(Point point){
		return this.contient(point.getX(), point.getY());
	}
	
	/**
	 * Indique si la Ligne contient le Point en (x,y)
	 * 
	 * @param x L'abscisse du point dont on veut savoir s'il appartient à la Ligne
	 * @param y L'ordonné du point dont on veut savoir s'il appartient à la Ligne
	 */
	public boolean contient(double x, double y){
		double distanceP1P2 = Forme.distance(this.p1, this.p2);
		double distancePointP1 = Forme.distance(new Point(x,y), this.p1);
		double distancePointP2 = Forme.distance(new Point(x,y), this.p2);
		return ((distancePointP1 + distancePointP2) - distanceP1P2) < EPSILON;
	}
	
	/**
	 * Renvoie la fiche d'identité de la Ligne
	 * @return La fiche d'identité de la Ligne
	 */
	public String toString(){
		String identite = new String();
		DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.getDefault()));
		String valeurLongueur = decimalFormat.format(this.perimetre(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		identite = "["+this.getClass().getSimpleName()+"] p1 : "+this.p1.toString()+" p2 : "+this.p2.toString()+" longueur : "+valeurLongueur;
		return identite;
	}

}
