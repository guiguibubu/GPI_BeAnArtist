package fr.eseo.gpi.beanartist.modele.formes;

import java.io.Serializable;
import java.lang.StringBuffer;
import java.text.DecimalFormat;
import java.util.Locale;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;

/**
 * Classe représentant un Point
 * @author Guibu
 */
public class Point implements Serializable {
	
	//Constante de classe
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'abscisse par défaut d'un Point
	 */
	public static final double ABSCISSE_PAR_DEFAUT = 0;
	
	/**
	 * L'ordonné par défaut d'un Point
	 */
	public static final double ORDONNEE_PAR_DEFAUT = 0;
	
	//Attribut
	
	/**
	 * Abscisse du Point
	 */
	private double x;
	
	/**
	 * Ordonnée du Point
	 */
	private double y;
	
	//Constructeur
	
	/**
	 * Créé un Point
	 * @param x L'abscisse du Point
	 * @param y L'ordonnée du Point
	 */
	public Point(double x, double y){
		this.x = x;
		this.y = y;
	}
	/**
	 * Créé un Point
	 * <br>L'abscisse du Point est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}
	 * <br>L'ordonnée du Point est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}
	 */
	public Point(){
		this(Point.ABSCISSE_PAR_DEFAUT,Point.ORDONNEE_PAR_DEFAUT);
	}
	
	//Accesseur
	/**
	 * Renvoie l'abscisse du Point
	 * @return L'abscisse du Point
	 */
	public double getX(){
		return this.x;
	}
	/**
	 * Renvoie l'ordonnée du Point
	 * @return L'ordonnée du Point
	 */
	public double getY(){
		return this.y;
	}
	/**
	 * Modifie l'abscisse du Point
	 * @param x La nouvelle abscisse du Point
	 */
	public void setX(double x){
		this.x = x;
	}
	/**
	 * Modifie l'ordonnées du Point
	 * @param y La nouvelle ordonnée du Point
	 */
	public void setY(double y){
		this.y = y;
	}

	//Méthode d'instance
	
	/**
	 * Déplace le Point en <b>(x,y)</b>
	 * @param x La nouvelle abscisse du Point
	 * @param y La nouvelle ordonnée du Point
	 */
	public void deplacerVers(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Déplace le Point de <b>dx</b> et de <b>dy</b>
	 * @param dx Déplacement selon l'axe des abscisses
	 * @param dy Déplacement selon l'axe des ordonnées
	 */
	public void deplacerDe(double dx, double dy){
		this.x += dx;
		this.y += dy;
	} 
	
	/**
	 * Renvoie la fiche d'identité de la Forme
	 * @return La fiche d'identité de la Forme
	 */
	public String toString(){
		String identite = new String();
		DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.getDefault()));
		String valeurX = decimalFormat.format(this.getX(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		String valeurY = decimalFormat.format(this.getY(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		identite = "("+valeurX+" , "+valeurY+")";
		return identite;
	}
}
