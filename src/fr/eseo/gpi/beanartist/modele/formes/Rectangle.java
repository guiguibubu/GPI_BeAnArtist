package fr.eseo.gpi.beanartist.modele.formes;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Classe représentant un Rectangle
 * @author Guibu
 */
public class Rectangle extends Forme{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Constructeur
	/**
	 * Crée un Rectangle
	 * @param pointOrigine Le Point d'origine du Point
	 * @param largeur La largeur du Point
	 * @param hauteur La hauteur du Point
	 */
	public Rectangle(Point pointOrigine, double largeur, double hauteur) throws IllegalArgumentException {
		super(pointOrigine, Math.abs(largeur), Math.abs(hauteur));
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
	}
	
	/**
	 * Crée un Rectangle
	 * @param x l'abscisse du Point double igine de la Rectangle
	 * @param y l'ordonnée du Point d'origine de la Rectangle
	 * @param largeur La largeur du Point
	 * @param hauteur La hauteur du Point
	 */
	public Rectangle(double x, double y, double largeur, double hauteur) throws IllegalArgumentException {
		this(new Point(x, y), Math.abs(largeur), Math.abs(hauteur));
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
	}
	
	/**
	 * Crée un Rectangle
	 * @param pointOrigine Le Point d'origine du Rectangle
	 * 
	 * La largeur est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}
	 * La hauteur est celle par défaut: {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}
	 */
	public Rectangle(Point pointOrigine){
		this(pointOrigine, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	/**
	 * Crée un Rectangle
	 * @param largeur La largeur du Rectangle
	 * @param hauteur La hauteur du Rectangle
	 * 
	 * Le Point d'origine est celui par défaut en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT})
	 */
	public Rectangle(double largeur, double hauteur) throws IllegalArgumentException {
		this(new Point(), Math.abs(largeur), Math.abs(hauteur));
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
	}
	/**
	 * Crée un Rectangle [{@value fr.eseo.gpi.beanartist.modele.formes.Forme#LARGEUR_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Forme#HAUTEUR_PAR_DEFAUT}] en ({@value fr.eseo.gpi.beanartist.modele.formes.Point#ABSCISSE_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.modele.formes.Point#ORDONNEE_PAR_DEFAUT}) 
	 */
	public Rectangle(){
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	/**
	 * Modifie la largeur du Rectangle
	 * @param largeur La nouvelle largeur du Rectangle
	 */
	public void setLargeur(double largeur) throws IllegalArgumentException {
		if (largeur < 0){
			throw new IllegalArgumentException("largeur < 0");
		}
		else{
			super.setLargeur(largeur);
		}
		
	}
	/**
	 * Modifie la hauteur du Rectangle
	 * @param hauteur La nouvelle hauteur du Rectangle
	 */
	public void setHauteur(double hauteur) throws IllegalArgumentException{
		if(hauteur < 0){
			throw new IllegalArgumentException("hauteur < 0");
		}
		else{
			super.setHauteur(hauteur);
		}
	}
	/**
	 * Calcule le périmètre du Rectangle
	 * @return Le périmètre du Rectangle
	 */
	public double perimetre(){
		return 2*(this.getLargeur()+this.getHauteur());
	}
	/**
	 * Calcule l'aire du Rectangle
	 * @return L'aire du Rectangle
	 */
	public double aire(){
		return this.getLargeur()*this.getHauteur();
	}
	
	public boolean contient(Point point){
		return this.contient(point.getX(), point.getY());
	}
	
	public boolean contient(double x, double y){
		return ((x >= this.getCadreMinX()) && (x <= this.getCadreMaxX()) && (y >= this.getCadreMinY()) && (y <= this.getCadreMaxY()));
	}
	/**
	 * Renvoie la fiche d'identité du Rectangle
	 * @return La fiche d'identité du Rectangle
	 */
	public String toString(){
		String identite = new String();
		DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.getDefault()));
		String valeurAire = decimalFormat.format(this.aire(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		String valeurPerimetre = decimalFormat.format(this.perimetre(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		identite = super.toString()+" périmètre : "+valeurPerimetre+" aire : "+valeurAire;
		return identite;
	}
}
