package fr.eseo.gpi.beanartist.modele.formes;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Classe représentant un Tracé
 * 
 * @author buchlegu
 *
 */
public class Trace extends Forme {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Liste des Point dans le Trace
	 */
	private List<Point> points;
	
	/**
	 * Créé un Trace avec les deux premiers Points
	 * @param p1 Le premier Point du Trace
	 * @param p2 Le second Point du Trace
	 */
	public Trace(Point p1, Point p2) {
		super(Math.min(p1.getX(), p2.getX()),Math.min(p1.getY(), p2.getY()), Math.abs(p2.getX()-p1.getX()), Math.abs(p2.getY()-p1.getY()));
		
		this.points = new ArrayList<Point>();
		this.points.add(p1);
		this.points.add(p2);
	}
	
	/**
	 * Créé un Trace avec son premier Point
	 * 
	 * @param point Le premier Point du Trace
	 */
	public Trace(Point point) {
		super(point,0,0);
		
		this.points = new ArrayList<Point>();
		this.points.add(point);
	}
	
	/**
	 * Renvoie la liste de Point du Trace
	 * 
	 * @return La liste de Point du Trace
	 */
	public List<Point> getPoints(){
		return this.points;
	}
	
	/**
	 * Ajoute un Point au Trace
	 * 
	 * @param point Le Point à ajouter au Trace
	 */
	public void ajouterPoint(Point point){
		this.points.add(point);
		
		double xmin = Math.min(super.getCadreMinX(), point.getX());
		double xmax = Math.max(super.getCadreMaxX(), point.getX());
		double ymin = Math.min(super.getCadreMinY(), point.getY());
		double ymax = Math.max(super.getCadreMaxY(), point.getY());
		
		super.setPosition(xmin,ymin);
		super.setLargeur(xmax-xmin);
		super.setHauteur(ymax-ymin);
	}
	
	/**
	 * Déplace le Trace selon l'axe des abscisse
	 * 
	 * @param x La nouvelle abscisse du Trace
	 */
	public void setX(double x){
		double dx = x - super.getX();
		this.deplacerDe(dx, 0);
	}
	
	public void setY(double y){
		double dy = y - super.getY();
		this.deplacerDe(0, dy);
	}
	
	public void setPosition(Point position){
		this.setPosition(position.getX(), position.getY());
	}
	
	public void setPosition(double x, double y){
		this.deplacerVers(x, y);
	}
	
	public void deplacerDe(double dx, double dy){
		super.setPosition(super.getX()+dx, super.getY()+dy);
		for (Point point : this.points){
			point.deplacerDe(dx, dy);
		}
	}
	
	public void deplacerVers(double x, double y){
		double dx = x - super.getX();
		double dy = y - super.getY();
		this.deplacerDe(dx, dy);
	}
	
	public void setLargeur(double largeur){
		double oldLargeur = this.getLargeur();
		super.setLargeur(largeur);
		if (oldLargeur != 0){
			double ratio = largeur/oldLargeur;
	 		for (Point point : this.points){
					point.setX(ratio*(point.getX()-super.getX()) + super.getX());
			}
		}
		double xmin = Math.min(super.getCadreMinX(), this.getX());
		double ymin = Math.min(super.getCadreMinY(), this.getY());
		
		super.setPosition(xmin,ymin);
		super.setLargeur(Math.abs(largeur));
	}
	
	public void setHauteur(double hauteur){
		double oldHauteur = this.getHauteur();
		super.setHauteur(hauteur);
		if (oldHauteur != 0){
			double ratio = hauteur/oldHauteur;
			for (Point point : this.points){
				point.setY(ratio*(point.getY()-this.getY())+this.getY());
			}
		}
		double xmin = Math.min(super.getCadreMinX(), this.getX());
		double ymin = Math.min(super.getCadreMinY(), this.getY());
		
		super.setPosition(xmin,ymin);
		super.setHauteur(Math.abs(hauteur));
	}
	
	public double perimetre(){
		double perimetre;
		int nbPoint = this.points.size();
		Point p1 = this.points.get(0);
		Point p2 = this.points.get(1);
		Ligne ligneP1P2 = new Ligne(p1, p2.getX()-p1.getX(), p2.getY()-p1.getY());
		perimetre = ligneP1P2.perimetre();
		for(int i = 1; i<nbPoint-1; i++){
			p1 = this.points.get(i);
			p2 = this.points.get(i+1);
			ligneP1P2.setP1(p1);
			ligneP1P2.setP2(p2);
			perimetre += ligneP1P2.perimetre();
		}
		return perimetre;
	}
	
	public double aire(){
		return 0;
	}
	public boolean contient(Point point){
		return this.contient(point.getX(), point.getY());
	}
	
	public boolean contient(double x, double y){
		boolean contient;
		int nbPoint = this.points.size();
		Point p1 = this.points.get(0);
		Point p2 = this.points.get(1);
		Ligne ligneP1P2 = new Ligne(p1, p2.getX()-p1.getX(), p2.getY()-p1.getY());
		contient = ligneP1P2.contient(x, y);
		int i = 1;
		while((i<nbPoint-1) && (!contient)){
			p1 = this.points.get(i);
			p2 = this.points.get(i+1);
			ligneP1P2.setP1(p1);
			ligneP1P2.setP2(p2);
			contient = ligneP1P2.contient(x, y);
			i++;
		}
		return contient;
	}
	
	/**
	 * Renvoie la fiche d'identité de la Forme
	 * @return La fiche d'identité de la Forme
	 */
	public String toString(){
		String identite = new String();

		DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.getDefault()));
		String valeurPoint = super.getPosition().toString();
		String valeurLargeur = decimalFormat.format(super.getLargeur(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		String valeurHauteur = decimalFormat.format(super.getHauteur(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		String valeurLongueur = decimalFormat.format(this.perimetre(), new StringBuffer(identite), new FieldPosition(NumberFormat.FRACTION_FIELD)).toString();
		identite = "["+this.getClass().getSimpleName()+"] pos : "+valeurPoint+" dim : "+valeurLargeur+" x "+valeurHauteur+" longueur : "+valeurLongueur+" nbSegments : "+(this.points.size()-1);
		return identite;
	}
}
