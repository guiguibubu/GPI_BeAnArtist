package fr.eseo.gpi.beanartist.mouseManager;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.vue.formes.VueCoin;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

/**
 * Manager gérant le l'aspect du curseur de la sourie selon l'endroit où est la sourie dans un rectangle encadrant
 * @author guiguibubu
 *
 */
public class MouseManagerMain implements MouseMotionListener {
	
	private PanneauDessin panneauDessin;
	private VueCoin coinHG, coinHD, coinBD, coinBG;
	private String localisation;

	/**
	 * Construit le manager de gestion de l'aspect du curseur de la sourie
	 * @param panneauDessin Le panneau Dessin qui va l'écouter
	 * @param coinHG	La vue du coin Haut-Gauche du rectangle encadrant
	 * @param coinHD	La vue du coin Haut-Droit du rectangle encadrant
	 * @param coinBD	La vue du coin Bas-Droit du rectangle encadrant
	 * @param coinBG	La vue du coin Bas-Gauche du rectangle encadrant
	 */
	public MouseManagerMain(PanneauDessin panneauDessin, VueCoin coinHG, VueCoin coinHD, VueCoin coinBD, VueCoin coinBG) {
		this.panneauDessin = panneauDessin;
		this.coinHG = coinHG;
		this.coinHD = coinHD;
		this.coinBD = coinBD;
		this.coinBG = coinBG;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
		if(this.panneauDessin.getOutilCourant() instanceof OutilSelection){
			if(((OutilSelection)this.panneauDessin.getOutilCourant()).isAutoriseDeplacer()){
				if(this.estSurvole(e, this.coinHG)){
					this.localisation = this.coinHG.getLocalisation();
				}
				else if(this.estSurvole(e, this.coinHD)){
					this.localisation = this.coinHD.getLocalisation();
				}
				else if(this.estSurvole(e, this.coinBD)){
					this.localisation = this.coinBD.getLocalisation();
				}
				else if(this.estSurvole(e, this.coinBG)){
					this.localisation = this.coinBG.getLocalisation();
				}
				else if(this.estEntre(e, this.coinHG, this.coinHD, true)){
					this.localisation = "N";
				}
				else if(this.estEntre(e, this.coinHD, this.coinBD, false)){
					this.localisation = "E";
				}
				else if(this.estEntre(e, this.coinBG, this.coinBD, true)){
					this.localisation = "S";
				}
				else if(this.estEntre(e, this.coinHG, this.coinBG, false)){
					this.localisation = "W";
				}
				else if(this.estEntre(e, this.coinHG, this.coinHD, this.coinBD, this.coinBG)){
					this.localisation = "CENTER";
				}
				else{
					this.localisation ="";
				}
				
				switch(this.localisation){
					case "N" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
					break;
					case "E" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
					break;
					case "W" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
					break;
					case "S" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
					break;
					case "NW" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
					break;
					case "NE" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
					break;
					case "SE" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
					break;
					case "SW" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
					break;
					case "CENTER" : this.panneauDessin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					break;
					default : this.panneauDessin.setCursor(Cursor.getDefaultCursor());
					break;
				}
			}
		}
	}
	
	private boolean estSurvole(MouseEvent e, VueCoin coin){
		Forme forme = coin.getForme();
		return ((e.getX()>forme.getX())&&(e.getX()<forme.getX()+forme.getLargeur())&&(e.getY()>forme.getY())&&(e.getY()<forme.getY()+forme.getHauteur()));
	}
	
	/**
	 * Renvoie la localisation de la sourie par rapport au cadre décrite par les coins
	 * @return N, E, W, S, NW, NE, SE, SW ou CENTER
	 */
	public String getLocalisation(){
		return this.localisation;
	}
	
	private boolean estEntre(MouseEvent e, VueCoin coinA, VueCoin coinB, boolean horizontal){
		boolean estEntreX;
		boolean estEntreY;
		double xA = coinA.getForme().getX();
		double xB = coinB.getForme().getX();
		double yA = coinA.getForme().getY();
		double yB = coinB.getForme().getY();
		double xMin = (xA<xB) ? xA : xB;
		double xMax = (xA<xB) ? xB : xA;
		double yMin = (yA<yB) ? yA : yB;
		double yMax = (yA<yB) ? yB : yA;
		
		estEntreX = (horizontal) ? (e.getX() > xMin+VueCoin.cote/2)&&(e.getX()<xMax-VueCoin.cote/2) : (e.getX() > xMin-VueCoin.cote/2)&&(e.getX()<xMin+VueCoin.cote/2);
		estEntreY = (!horizontal) ? (e.getY() > yMin+VueCoin.cote/2)&&(e.getY()<yMax-VueCoin.cote/2) : (e.getY() > yMin-VueCoin.cote/2)&&(e.getY()<yMin+VueCoin.cote/2);;
		return estEntreX && estEntreY;
	}
	
	private boolean estEntre(MouseEvent e, VueCoin coinHG, VueCoin coinHD, VueCoin coinBD, VueCoin coinBG){
		boolean estEntreX;
		boolean estEntreY;
		double xHG = coinHG.getForme().getX();
		double xHD = coinHD.getForme().getX();
		double xBG = coinBG.getForme().getX();
		double xBD = coinBD.getForme().getX();
		double yHG = coinHG.getForme().getY();
		double yHD = coinHD.getForme().getY();
		double yBG = coinBG.getForme().getY();
		double yBD = coinBD.getForme().getY();
		
		double xMin = Math.min(Math.min(xHG, xHD), Math.min(xBG, xBD));
		double xMax = Math.max(Math.max(xHG, xHD), Math.max(xBG, xBD));
		double yMin = Math.min(Math.min(yHG, yHD), Math.min(yBG, yBD));
		double yMax = Math.max(Math.max(yHG, yHD), Math.max(yBG, yBD));
		
		estEntreX = (e.getX()>xMin + VueCoin.cote/2) && (e.getX()<xMax - VueCoin.cote/2);
		estEntreY = (e.getY()>yMin + VueCoin.cote/2) && (e.getY()<yMax - VueCoin.cote/2);
		return estEntreX && estEntreY;
	}
	
}