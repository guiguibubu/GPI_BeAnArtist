package fr.eseo.gpi.beanartist.popup;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * 
 * Popup permettant le choix de l'épaisseur du trait des formes sélectionnées
 * (Classe provenant d'un tuto officiel Oracle pour java, que j'ai modifiée, voila pourquoi il reste des commentaire en anglais)
 *
 */
public class PopupEpaisseurTrait extends JDialog
                   implements ActionListener,
                              PropertyChangeListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String texteSaisi = null;
    private JTextField zoneDeTexte;

    private JOptionPane optionPane;

    private String texteBouttonOk = "OK";
    private String texteBouttonAnnuler = "Annuler";

    /**
     * Returns null if the typed string was invalid;
     * otherwise, returns the string as the user entered it.
     * @return La nouvelle épaisseur demandée, null sinon
     */
    public Integer getNouvelleEpaisseur() {
    	try {
            return new Integer(texteSaisi);
    	}
    	catch (Exception e){
    		return null;
    	}
    }

    /** 
     * Créé une fenetre Popup réutilisable permettant le choix de la nouvelle épaisseur de trait des formes sélectionnées
     * @param fenetre La fenetre à laquelle appartient le Popup créé
     */
    public PopupEpaisseurTrait(Frame fenetre){
    	super(fenetre, true);

    	this.setLocationRelativeTo(fenetre);
    	this.setPreferredSize(new Dimension(500,300));
    	
        setTitle("Epaisseur du trait");

        zoneDeTexte = new JTextField(10);

        //Create an array of the text and components to be displayed.
        String msgString1 = "Quel nouvelle épaisseur voulez vous ?";

        Object[] array = {msgString1, zoneDeTexte};

        //Create an array specifying the number of dialog buttons
        //and their text.
        Object[] options = {texteBouttonOk, texteBouttonAnnuler};

        //Create the JOptionPane.
        optionPane = new JOptionPane(array,
                                    JOptionPane.QUESTION_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null,
                                    options,
                                    options[0]);

        //Make this dialog display it.
        setContentPane(optionPane);

        //Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(
                                        JOptionPane.CLOSED_OPTION));
            }
        });

        //Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                zoneDeTexte.requestFocusInWindow();
            }
        });

        //Register an event handler that puts the text into the option pane.
        zoneDeTexte.addActionListener(this);

        //Register an event handler that reacts to option pane state changes.
        optionPane.addPropertyChangeListener(this);
    }

    /** This method handles events for the text field. */
    public void actionPerformed(ActionEvent e) {
        optionPane.setValue(texteBouttonOk);
    }

    /** This method reacts to state changes in the option pane. */
    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();

        if (isVisible() && (e.getSource() == optionPane) && (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
          
        	Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE) {
                //ignore reset
                return;
            }

            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            optionPane.setValue(
                    JOptionPane.UNINITIALIZED_VALUE);
            
            if (texteBouttonOk.equals(value)) {
                    texteSaisi = zoneDeTexte.getText();
                    try { 
                        if(Integer.parseInt(texteSaisi) <= 0){
                        	this.texteSaisi = null;
                        	//le texte tapé n'est pas un entier strictement positif
                            zoneDeTexte.selectAll();
                            JOptionPane.showMessageDialog(
                            		PopupEpaisseurTrait.this,
                                            "Il faut un entier strictement positif",
                                            "Mauvais entré",
                                            JOptionPane.ERROR_MESSAGE);
                            texteSaisi = null;
                            zoneDeTexte.requestFocusInWindow();
                        }
                        else{
                        	clearAndHide();
                        }
                    } catch(NumberFormatException | NullPointerException exception) { 
                    	//le texte tapé n'est pas un chiffre
                        zoneDeTexte.selectAll();
                        JOptionPane.showMessageDialog(
                        		PopupEpaisseurTrait.this,
                                        "Il faut un entier strictement positif",
                                        "Mauvais entré",
                                        JOptionPane.ERROR_MESSAGE);
                        texteSaisi = null;
                        zoneDeTexte.requestFocusInWindow();
                    }
                    
            } 
            else { //user closed dialog or clicked cancel
                texteSaisi = null;
                clearAndHide();
            }
        }
    }

    /** This method clears the dialog and hides it. */
    public void clearAndHide() {
        zoneDeTexte.setText(null);
        setVisible(false);
    }
}