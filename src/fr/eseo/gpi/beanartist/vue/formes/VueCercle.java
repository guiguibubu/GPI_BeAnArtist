package fr.eseo.gpi.beanartist.vue.formes;

import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Point;

/**
 * Classe modélisant l'aspect graphique d'un cercle
 *
 */
public class VueCercle extends VueEllipse {

	public VueCercle(Cercle cercle) {
		super(cercle);
	}
	
	public void rafraichirAffichage(Point debut, Point fin){
		double xDebut = debut.getX();
		double xFin = fin.getX();
		double yDebut = debut.getY();
		double yFin = fin.getY();
		
		double largeur = Math.abs(xDebut - xFin);
		double hauteur = Math.abs(yDebut - yFin);
		double cote = Math.max(largeur, hauteur);
		double xOrigine = (xDebut < xFin) ? xDebut : xDebut - cote;
		double yOrigine = (yDebut < yFin) ? yDebut : yDebut - cote;
		
		this.forme.setX(xOrigine);
		this.forme.setY(yOrigine);
		this.forme.setHauteur(cote);
	}
}
