package fr.eseo.gpi.beanartist.vue.formes;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Point;

/**
 * Classe modélisant l'aspect graphique d'un coin du rectangle englobant des formes sélectionnées
 *
 */
public class VueCoin extends VueCarre {

	public static int cote = 15;
	private String localisation;
	
	/**
	 * Construit la vue d'un coin du rectangle englobant des formes sélectionnées
	 * @param carre La forme du coin
	 * @param localisation La localisation cardinale du coin
	 */
	public VueCoin(Carre carre, String localisation) {
		super(carre);
		this.localisation = localisation;
	}
	
	/**
	 * Centre le coin à une nouvelle position
	 * @param coin La nouvelle position du coin
	 */
	public void rafraichirAffichage(Point coin){
		double xOrigine = coin.getX() - VueCoin.cote/2;
		double yOrigine = coin.getY() - VueCoin.cote/2;
		
		this.forme.getPosition().deplacerVers(xOrigine, yOrigine);
	}
	
	/**
	 * Renvoie la localisation du coin
	 * @return la localisation du coin
	 */
	public String getLocalisation(){
		return this.localisation;
	}
}
