package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Point;

/**
 * Classe modélisant l'aspect graphique d'une forme
 *
 */
abstract public class VueForme {
	
	protected final Forme forme;
	
	protected boolean selectionne;
	
	/**
	 * Construit la vue d'une forme
	 * @param forme La forme à visualiser
	 */
	public VueForme(Forme forme) {
		this.forme = forme;
	}
	
	/**
	 * Renvoie la forme visualisée
	 * @return La forme visualisée
	 */
	public Forme getForme(){
		return this.forme;
	}
	
	/**
	 * Indique si la forme est sélectionnée
	 * @return true sila forme est sélectionnée, false sinon
	 */
	public boolean estSelectionne(){
		return this.selectionne;
	}
	
	/**
	 * Sélectionne/Désélectionne la forme
	 * @param selectionne Indique si la forme est maintenant sélectionnée ou non
	 */
	public void setSelectionne(boolean selectionne){
		this.selectionne = selectionne;
	}
	
	/**
	 * Affiche, dans un contexte graphique, la forme
	 * @param g2D Le contexte graphique où la forme sera dessinée
	 */
	abstract public void affiche(Graphics2D g2D);

	/**
	 * Modifie les propriétés de la forme selon les points de clique et de relache
	 * @param debut Le point où le boutton de sourie a été pressé
	 * @param fin Le point où le boutton de sourie a été relaché
	 */
	abstract public void rafraichirAffichage(Point debut, Point fin);
}
