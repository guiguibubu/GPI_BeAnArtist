package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.BasicStroke;
import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;

/**
 * Classe modélisant l'aspect graphique d'une ligne
 *
 */
public class VueLigne extends VueForme {

	/**
	 * Construit la vue d'une ligne
	 * @param ligne La ligne à afficher
	 */
	public VueLigne(Ligne ligne) {
		super(ligne);
	}

	@Override
	public void affiche(Graphics2D g2D) {
		g2D.setColor(this.forme.getCouleurLigne());
		if(this.estSelectionne()){
			//On dessine en gras
			BasicStroke traitGras = new BasicStroke((float)4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
			g2D.setStroke(traitGras);
		}
		else{
			g2D.setStroke(new BasicStroke((float)this.forme.getEpaisseurBordure()));
		}
		g2D.drawLine((int)Math.round(this.forme.getX()), (int)Math.round(this.forme.getY()), (int)Math.round(this.forme.getX()+this.forme.getLargeur()), (int)Math.round(this.forme.getY()+this.forme.getHauteur()));
	}

	@Override
	public void rafraichirAffichage(Point debut, Point fin){
		double xDebut = debut.getX();
		double xFin = fin.getX();
		double yDebut = debut.getY();
		double yFin = fin.getY();
		
		double largeur = xFin - xDebut;
		double hauteur = yFin - yDebut;
		double xOrigine = xDebut;
		double yOrigine = yDebut;
		
		this.forme.setX(xOrigine);
		this.forme.setY(yOrigine);
		this.forme.setHauteur(hauteur);
		this.forme.setLargeur(largeur);
	}
}
