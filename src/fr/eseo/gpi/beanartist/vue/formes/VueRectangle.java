package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.BasicStroke;
import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;

/**
 * Classe modélisant l'aspect graphique d'un rectangle
 *
 */
public class VueRectangle extends VueForme {

	/**
	 * Construit la vue d'un recatngle
	 * @param rectangle Le rectangle à afficher
	 */
	public VueRectangle(Rectangle rectangle) {
		super(rectangle);
	}

	@Override
	public void affiche(Graphics2D g2D) {
		if(this.forme.getCouleurRemplissage() != null){
			g2D.setColor(this.forme.getCouleurRemplissage());
			g2D.fillRect((int)Math.round(this.forme.getX()), (int)Math.round(this.forme.getY()), (int)Math.round(this.forme.getLargeur()), (int)Math.round(this.forme.getHauteur()));
		}
		g2D.setColor(this.forme.getCouleurLigne());
		if(this.estSelectionne()){
			//On dessine en gras
			BasicStroke traitGras = new BasicStroke((float)4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
			g2D.setStroke(traitGras);
		}
		else{
			g2D.setStroke(new BasicStroke((float)this.forme.getEpaisseurBordure()));
		}
		g2D.drawRect((int)Math.round(this.forme.getX()), (int)Math.round(this.forme.getY()), (int)Math.round(this.forme.getLargeur()), (int)Math.round(this.forme.getHauteur()));
	}
	
	public void rafraichirAffichage(Point debut, Point fin){
		double xDebut = debut.getX();
		double xFin = fin.getX();
		double yDebut = debut.getY();
		double yFin = fin.getY();
		
		double largeur = Math.abs(xDebut - xFin);
		double hauteur = Math.abs(yDebut - yFin);
		double xOrigine = (xDebut < xFin) ? xDebut : xFin;
		double yOrigine = (yDebut < yFin) ? yDebut : yFin;
		
		this.forme.setX(xOrigine);
		this.forme.setY(yOrigine);
		this.forme.setHauteur(hauteur);
		this.forme.setLargeur(largeur);
	}
}
