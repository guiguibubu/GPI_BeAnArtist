package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;

/**
 * Classe modélisant l'aspect graphique du rectangle de sélection
 *
 */
public class VueSelection extends VueForme {
	
	/**
	 * Construit la vue d'une forme
	 * @param forme La forme à visualiser
	 */
	public VueSelection(Rectangle forme) {
		super(forme);
	}

	@Override
	public void affiche(Graphics2D g2D) {
		g2D.setColor(Color.black);
		//On dessine en pointillé
		float[] pointille = new float[]{(float)10, (float)10};
		BasicStroke traitPointille = new BasicStroke((float)1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, (float)1, pointille, (float)0);
		g2D.setStroke(traitPointille);
		int x = (int)Math.round(this.forme.getX());
		int y = (int)Math.round(this.forme.getY());
		int largeur = (int)Math.round(this.forme.getLargeur());
		int hauteur = (int)Math.round(this.forme.getHauteur());
		g2D.drawRect(x, y ,largeur, hauteur);
	}

	@Override
	public void rafraichirAffichage(Point debut, Point fin) {
		double xDebut = debut.getX();
		double xFin = fin.getX();
		double yDebut = debut.getY();
		double yFin = fin.getY();
		
		double largeur = Math.abs(xDebut - xFin);
		double hauteur = Math.abs(yDebut - yFin);
		double xOrigine = (xDebut < xFin) ? xDebut : xFin;
		double yOrigine = (yDebut < yFin) ? yDebut : yFin;
		
		this.forme.setX(xOrigine);
		this.forme.setY(yOrigine);
		this.forme.setHauteur(hauteur);
		this.forme.setLargeur(largeur);
	}
}
