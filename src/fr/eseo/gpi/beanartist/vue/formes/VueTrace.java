package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.util.List;

import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.modele.formes.Point;

/**
 * Classe modélisant l'aspect graphique d'un tracé à main levée
 *
 */
public class VueTrace extends VueForme {
	
	/**
	 * Construit la vue d'un tracé à main levé
	 * @param trace Le tracé à main levée à visualiser
	 */
	public VueTrace(Trace trace) {
		super(trace);
	}

	@Override
	public void affiche(Graphics2D g2D) {
		g2D.setColor(this.forme.getCouleurLigne());
		if(this.estSelectionne()){
			//On dessine en gras
			BasicStroke traitGras = new BasicStroke((float)4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
			g2D.setStroke(traitGras);
		}
		else{
			g2D.setStroke(new BasicStroke((float)this.forme.getEpaisseurBordure()));
		}
		List<Point> listePoints = ((Trace)(this.forme)).getPoints();
		int nbPoint = listePoints.size();
		Point p1;
		Point p2;
		
		for(int i = 0; i<nbPoint-1; i++){
			p1 = listePoints.get(i);
			p2 = listePoints.get(i+1);
			g2D.drawLine((int)Math.round(p1.getX()), (int)Math.round(p1.getY()), (int)Math.round(p2.getX()), (int)Math.round(p2.getY()));
		}
	}
	
	public void rafraichirAffichage(Point debut, Point fin){
		this.rafraichirAffichage(fin);
	}

	/**
	 * Ajoute un point au tracé
	 * @param fin Le point ajouté au tracé
	 */
	public void rafraichirAffichage(Point fin){
		((Trace)this.forme).ajouterPoint(new Point(fin.getX(), fin.getY()));
	}
}
