package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import fr.eseo.gpi.beanartist.controleur.actions.ActionAPropos;
import fr.eseo.gpi.beanartist.controleur.actions.ActionAnnulerSelection;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoisirCouleurLigne;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixCouleurFond;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixCouleurRemplissage;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixCouleurTrait;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixEpaisseurBordure;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEffacer;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEffacerSelection;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrer;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrerFichierDejaCree;
import fr.eseo.gpi.beanartist.controleur.actions.ActionExporter;
import fr.eseo.gpi.beanartist.controleur.actions.ActionImporter;
import fr.eseo.gpi.beanartist.controleur.actions.ActionNouveauFichier;
import fr.eseo.gpi.beanartist.controleur.actions.ActionOuvrir;
import fr.eseo.gpi.beanartist.controleur.actions.ActionQuitter;
import fr.eseo.gpi.beanartist.controleur.actions.ActionToutSelectionner;

import javax.swing.ImageIcon;
import javax.swing.JMenu;

/**
 * Barre de Menu de la fenêtre BeAnArtist
 * @author guiguibubu
 *
 */
public class BarreDeMenu extends JMenuBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PanneauDessin panneauDessin;
	
	private JMenu menuFichier;
	private JMenu menuEdition;
	private JMenu menuAPropos;
	
	private JMenu menuCouleurFond;
	private JMenu menuCouleurLigne;
	private JMenu menuCouleurTraitSelection;
	private JMenu menuCouleurRemplissage;
	private JMenu menuEpaisseurLigne;
	
	private JMenuItem itemNouveauFichier;
	private JMenuItem itemOuvrir;
	private JMenuItem itemEnregistrer;
	private JMenuItem itemEnregistrerSous;
	private JMenuItem itemImporter;
	private JMenuItem itemExporter;
	private JMenuItem itemQuitter;
	
	private JMenuItem itemToutSelectionner;
	private JMenuItem itemToutEffacer;
	private JMenuItem itemEffacerSelection;
	private JMenuItem itemAnnulerSelection;
	
	private JMenuItem itemCouleurFondParDefaut;
	private JMenuItem itemCouleurFondChoix;
	private JMenuItem itemCouleurLigneParDefaut;
	private JMenuItem itemCouleurLigneChoix;
	
	private JMenuItem itemCouleurTraitSelectionParDefaut;
	private JMenuItem itemCouleurTraitSelectionChoix;
	private JMenuItem itemCouleurRemplissageParDefaut;
	private JMenuItem itemCouleurRemplissageChoix;
	private JMenuItem itemEpaisseurLigneChoix;
	private JMenuItem itemEpaisseurLigneParDefaut;
	
	private JMenuItem itemAPropos;
	
	private ActionEnregistrerFichierDejaCree actionEnregistrerFichierDejaCree;
	
	/**
	 * Construit la barre de menu lié au panneau Dessin
	 * @param panneauDessin Le panneau Dessin auquel est lié la barre de menu
	 * @throws HeadlessException Exception levée si l'environnement ne supporte pas le clavier, la sourie ou l'écran
	 */
	public BarreDeMenu(PanneauDessin panneauDessin) throws HeadlessException {
		super();
		this.panneauDessin = panneauDessin;
		this.initMenu();
	}
	
	private void initMenu(){
		this.menuFichier = new JMenu("Fichier");
		this.menuEdition = new JMenu("Edition");
		this.menuAPropos = new JMenu("?");
		
		this.menuCouleurFond = new JMenu("Couleur de fond");
		this.menuCouleurLigne = new JMenu("Couleur du trait Dessin");
		this.menuCouleurTraitSelection = new JMenu("Couleur du trait sélection");
		this.menuCouleurRemplissage = new JMenu("Couleur remplissage sélection");
		this.menuEpaisseurLigne = new JMenu("Epaisseur Bordure Sélection");
		
		this.itemNouveauFichier = new JMenuItem("Nouveau Fichier", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeNouveauFichierMini.png")));
		this.itemOuvrir = new JMenuItem("Ouvrir", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeOuvrirMini.png")));
		this.itemEnregistrer = new JMenuItem("Enregistrer", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeSauvegarderMini.png")));
		this.itemEnregistrerSous = new JMenuItem("Enregistrer sous ...", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeSauvegarderSousMini.png")));
		this.itemImporter = new JMenuItem("Importer ...", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeImporterMini.png")));
		this.itemExporter = new JMenuItem("Exporter ...", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeExportMini.png")));
		this.itemQuitter = new JMenuItem("Quitter", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeQuitterMini.png")));
		this.itemToutEffacer = new JMenuItem("Tout Effacer", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeEffacerMini.png")));
		this.itemToutSelectionner = new JMenuItem("Tout Selectionner");
		this.itemEffacerSelection = new JMenuItem("Effacer Sélection");
		this.itemAnnulerSelection = new JMenuItem("Annuler Sélection");
		this.itemCouleurFondParDefaut = new JMenuItem("Couleur de fond par défaut");
		this.itemCouleurFondChoix = new JMenuItem("Choix couleur de fond", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeFondMini.png")));
		this.itemCouleurLigneParDefaut = new JMenuItem("Couleur du trait Dessin par défaut");
		this.itemCouleurLigneChoix = new JMenuItem("Choix couleur du trait Dessin");
		this.itemCouleurTraitSelectionParDefaut = new JMenuItem("Couleur du trait sélection par défaut");
		this.itemCouleurTraitSelectionChoix = new JMenuItem("Choix couleur du trait sélection");
		this.itemCouleurRemplissageChoix = new JMenuItem("Choix couleur remplissage");
		this.itemCouleurRemplissageParDefaut = new JMenuItem("Pas de remplissage");
		this.itemEpaisseurLigneChoix = new JMenuItem("Choix épaisseur");
		this.itemEpaisseurLigneParDefaut = new JMenuItem("Epaisseur par défaut");
		
		this.itemNouveauFichier.addActionListener(new ActionNouveauFichier());
		this.itemOuvrir.addActionListener(new ActionOuvrir());
		
		this.actionEnregistrerFichierDejaCree = new ActionEnregistrerFichierDejaCree();//on en a besoin en dehors de l'initialisation
		this.itemEnregistrer.addActionListener(new ActionEnregistrerFichierDejaCree());
		
		this.itemEnregistrerSous.addActionListener(new ActionEnregistrer());
		this.itemImporter.addActionListener(new ActionImporter());
		this.itemExporter.addActionListener(new ActionExporter(this.panneauDessin));
		this.itemQuitter.addActionListener(new ActionQuitter());
		this.itemToutEffacer.addActionListener(new ActionEffacer());
		
		this.itemToutSelectionner.addActionListener(new ActionToutSelectionner(this.panneauDessin));
		this.itemEffacerSelection.addActionListener(new ActionEffacerSelection(this.panneauDessin));
		this.itemAnnulerSelection.addActionListener(new ActionAnnulerSelection(this.panneauDessin));
		this.itemCouleurFondParDefaut.addActionListener(new ActionChoixCouleurFond(this.panneauDessin, true));
		this.itemCouleurFondChoix.addActionListener(new ActionChoixCouleurFond(this.panneauDessin, false));
		this.itemCouleurLigneParDefaut.addActionListener(new ActionChoixCouleurTrait(this.panneauDessin, true));
		this.itemCouleurLigneChoix.addActionListener(new ActionChoixCouleurTrait(this.panneauDessin, false));
	
		ActionListener actionChoixCouleurTraitSelection = new ActionChoixCouleurTrait(this.panneauDessin, false);
		ActionListener actionChoixCouleurTraitSelectionParDefaut = new ActionChoixCouleurTrait(this.panneauDessin, true);
		ActionListener actionChoixCouleurRemplissageSelection = new ActionChoixCouleurRemplissage(this.panneauDessin, false);
		ActionListener actionChoixCouleurRemplissageSelectionParDefaut = new ActionChoixCouleurRemplissage(this.panneauDessin, true);
		ActionListener actionChoixEpaisseurBordureSelection = new ActionChoixEpaisseurBordure(this.panneauDessin, false);
		ActionListener actionChoixEpaisseurBordureSelectionParDefaut = new ActionChoixEpaisseurBordure(this.panneauDessin, true);
		this.itemCouleurTraitSelectionParDefaut.addActionListener(actionChoixCouleurTraitSelectionParDefaut);
		this.itemCouleurTraitSelectionChoix.addActionListener(actionChoixCouleurTraitSelection);
		this.itemCouleurRemplissageParDefaut.addActionListener(actionChoixCouleurRemplissageSelectionParDefaut);
		this.itemCouleurRemplissageChoix.addActionListener(actionChoixCouleurRemplissageSelection);
		this.itemEpaisseurLigneChoix.addActionListener(actionChoixEpaisseurBordureSelection);
		this.itemEpaisseurLigneParDefaut.addActionListener(actionChoixEpaisseurBordureSelectionParDefaut);
		
		this.itemNouveauFichier.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
		this.itemOuvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
		this.itemEnregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK));
		this.itemEnregistrerSous.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK + KeyEvent.SHIFT_MASK));
		this.itemQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
		this.itemToutSelectionner.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK));
		this.itemEffacerSelection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		this.itemAnnulerSelection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		this.itemToutEffacer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.CTRL_MASK + KeyEvent.SHIFT_MASK));
		
		this.menuFichier.add(this.itemNouveauFichier);
		this.menuFichier.add(this.itemOuvrir);
		this.menuFichier.add(this.itemEnregistrer);
		this.menuFichier.add(this.itemEnregistrerSous);
		this.menuFichier.add(this.itemImporter);
		this.menuFichier.add(this.itemExporter);
		this.menuFichier.add(this.itemQuitter);
		
		this.menuEdition.add(this.itemToutSelectionner);
		this.menuEdition.add(this.itemAnnulerSelection);
		this.menuEdition.add(this.itemEffacerSelection);
		this.menuEdition.add(this.itemToutEffacer);
		this.menuEdition.add(this.menuCouleurFond);
		this.menuEdition.add(this.menuCouleurLigne);
		this.menuEdition.add(this.menuCouleurTraitSelection);
		this.menuEdition.add(this.menuCouleurRemplissage);
		this.menuEdition.add(this.menuEpaisseurLigne);
		
		this.menuCouleurFond.add(this.itemCouleurFondParDefaut);
		this.menuCouleurFond.add(this.itemCouleurFondChoix);
		
		this.menuCouleurLigne.add(this.itemCouleurLigneParDefaut);
		this.menuCouleurLigne.add(this.itemCouleurLigneChoix);
		
		this.menuCouleurTraitSelection.add(this.itemCouleurTraitSelectionParDefaut);
		this.menuCouleurTraitSelection.add(this.itemCouleurTraitSelectionChoix);
		
		this.menuCouleurRemplissage.add(this.itemCouleurRemplissageParDefaut);
		this.menuCouleurRemplissage.add(this.itemCouleurRemplissageChoix);

		this.menuEpaisseurLigne.add(this.itemEpaisseurLigneParDefaut);
		this.menuEpaisseurLigne.add(this.itemEpaisseurLigneChoix);
		
		this.itemAPropos = new JMenuItem("A propos", new ImageIcon(BarreDeMenu.class.getResource("/fr/eseo/gpi/beanartist/res/iconeInfo.png")));
		this.itemAPropos.setActionCommand(ActionAPropos.NOM_ACTION);
		this.itemAPropos.addActionListener(new ActionAPropos(FenetreBeAnArtist.getInstance()));
		this.menuAPropos.add(this.itemAPropos);
		
		this.add(this.menuFichier);
		this.add(this.menuEdition);
		this.add(this.menuAPropos);
	}
	
	/**
	 * Met la barre de menu dans la situation où un "enregistrer sous" à été fait, et que le fichier n'est plus un nouveau fichier, autorisant alors l'enregistrement simple
	 */
	public void autoriserEnregistrer(){
		this.itemEnregistrer.setEnabled(true);
		this.actionEnregistrerFichierDejaCree.setCheminSauvegarde(FenetreBeAnArtist.getInstance().getCheminSauvegarde());
	}
	
	/**
	 * Met la barre de menu dans la situation où un nouveau fichier est ouvert
	 */
	public void nouveauFichier(){
		this.itemEnregistrer.setEnabled(false);
		this.actionEnregistrerFichierDejaCree.setCheminSauvegarde(null);
	}
	
	/**
	 * Met la barre de menu dans la situation où l'outil de sélection est choisie ou non. On active/désactive les actions "annuler sélection", "effacer sélection", "Tout Selectionner","Couleur Trait Selection" et "Couleur Remplissage".
	 * @param modeSelection Indique si on doit être dans le mode de sélection ou non
	 */
	public void modeSelection(boolean modeSelection){
		if(modeSelection){
			this.itemAnnulerSelection.setEnabled(true);
			this.itemEffacerSelection.setEnabled(true);
			this.itemToutSelectionner.setEnabled(true);
			this.menuCouleurTraitSelection.setEnabled(true);
			this.menuCouleurRemplissage.setEnabled(true);
			this.menuEpaisseurLigne.setEnabled(true);
		}
		else{
			
			this.itemAnnulerSelection.setEnabled(false);
			this.itemEffacerSelection.setEnabled(false);
			this.itemToutSelectionner.setEnabled(false);
			this.menuCouleurTraitSelection.setEnabled(false);
			this.menuCouleurRemplissage.setEnabled(false);
			this.menuEpaisseurLigne.setEnabled(false);
		}
	}
	
	/**
	 * Met la barre de menu dans la situation où un outil de dessin est choisie ou non. Met la barre de menu dans la situation où l'outil de sélection est choisie ou non. On active/désactive l'action "couleur trait dessin".
	 * @param modeDessin Indique si on doit être dans le mode dessin ou non
	 */
	public void modeDessin(boolean modeDessin){
		if(modeDessin){
			if(this.itemCouleurLigneChoix.getActionListeners().length != 0 && this.itemCouleurLigneParDefaut.getActionListeners().length != 0){
				this.itemCouleurLigneChoix.removeActionListener(this.itemCouleurLigneChoix.getActionListeners()[0]);
				this.itemCouleurLigneParDefaut.removeActionListener(this.itemCouleurLigneParDefaut.getActionListeners()[0]);
			}
			PanneauDessin panneauDessin = FenetreBeAnArtist.getInstance().getPanneauDessin();
			this.itemCouleurLigneParDefaut.addActionListener(new ActionChoisirCouleurLigne(panneauDessin, true));
			this.itemCouleurLigneChoix.addActionListener(new ActionChoisirCouleurLigne(panneauDessin, false));
			
			this.menuCouleurLigne.setEnabled(true);
		}
		else{
			this.menuCouleurLigne.setEnabled(false);
		}
	}
}
