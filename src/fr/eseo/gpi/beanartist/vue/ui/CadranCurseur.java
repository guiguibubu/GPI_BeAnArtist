package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * Cadran indiquant où se trouve le curseur sur un panneau de Dessin
 * @author guiguibubu
 *
 */
public class CadranCurseur extends JPanel implements MouseMotionListener, MouseListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int LARGEUR_PAR_DEFAUT = PanneauBarreOutils.LARGEUR_PAR_DEFAUT;
	public static final int HAUTEUR_PAR_DEFAUT = PanneauDessin.HAUTEUR_PAR_DEFAUT - PanneauBarreOutils.HAUTEUR_PAR_DEFAUT;
	public static final Color COULEUR_FOND_PAR_DEFAUT = Color.WHITE;
	
	private JTextArea zoneTexte;
	
	private int sourieX;
	private int sourieY;
	
	/**
	 * Construit le cadran avec les dimensions par défaut [{@value fr.eseo.gpi.beanartist.vue.ui.CadranCurseur#LARGEUR_PAR_DEFAUT}, {@value fr.eseo.gpi.beanartist.vue.ui.CadranCurseur#HAUTEUR_PAR_DEFAUT}]
	 */
	public CadranCurseur(){
		this(CadranCurseur.LARGEUR_PAR_DEFAUT, CadranCurseur.HAUTEUR_PAR_DEFAUT);
	}
	
	/**
	 * Construit le cadran avec ces dimensions
	 * @param largeur La largeur du cadran
	 * @param hauteur La hauteur du cadran
	 */
	public CadranCurseur(int largeur, int hauteur) {
		super();
		this.setPreferredSize(new Dimension(largeur, hauteur));
		this.setBackground(CadranCurseur.COULEUR_FOND_PAR_DEFAUT);
		this.initComposant();
	}

	private void initComposant(){
				
		this.zoneTexte = new JTextArea("Bonjour");
		this.zoneTexte.setAlignmentX(JTextArea.CENTER_ALIGNMENT);
		this.zoneTexte.setAlignmentY(JTextArea.CENTER_ALIGNMENT);
		this.zoneTexte.setBackground(CadranCurseur.COULEUR_FOND_PAR_DEFAUT);
		this.zoneTexte.setEditable(false);
		this.zoneTexte.setFont(new Font("Serif", Font.PLAIN, 16));
	
		this.setLayout(new BorderLayout());
        this.add(this.zoneTexte, BorderLayout.CENTER);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		this.sourieX = e.getX();
		this.sourieY = e.getY();
		this.zoneTexte.setText("x : "+this.sourieX+" y : "+this.sourieY);
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		//this.zoneTexte.setText("x : "+this.sourieX+" y : "+this.sourieY);
		
		TitledBorder titleBorder = BorderFactory.createTitledBorder("Coordonnées Curseur");
		titleBorder.setTitlePosition(TitledBorder.TOP);
		int epaisseurTitreHaut = titleBorder.getBorderInsets(this).top;
		int epaisseurTitreBas = titleBorder.getBorderInsets(this).bottom;
		int epaisseurTitreGauche = titleBorder.getBorderInsets(this).left;
		int epaisseurTitreDroite = titleBorder.getBorderInsets(this).right;
		
		FontMetrics fm = g.getFontMetrics(g.getFont());
		int longueurTexte = fm.stringWidth(this.zoneTexte.getText());
		int hauteurTexte = fm.getAscent();
		int xDecal = (this.getWidth() > longueurTexte + epaisseurTitreGauche + epaisseurTitreDroite) ? (this.getWidth() - fm.stringWidth(this.zoneTexte.getText()))/2 - epaisseurTitreGauche : 0;
		int yDecal = (this.getHeight() > hauteurTexte + epaisseurTitreHaut + epaisseurTitreBas) ? (this.getHeight() - fm.getAscent())/2 - epaisseurTitreHaut : 0;

		Border compound;
		
		Border emptyBorder = BorderFactory.createEmptyBorder(yDecal, xDecal, 0, 0);
		compound = BorderFactory.createCompoundBorder(titleBorder, emptyBorder);
		this.setBorder(compound);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.zoneTexte.setText("Sourie hors du Panneau Dessin");
	}
}
