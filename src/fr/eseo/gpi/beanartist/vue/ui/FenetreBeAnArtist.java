package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrer;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Classe modélisant la fenetre principale de l'application BeAnArtist
 * @author guiguibubu
 *
 */
public class FenetreBeAnArtist extends JFrame implements KeyListener, WindowListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TITRE_PAR_DEFAUT = "Nouveau Fichier";
	// Assure qu'il n'y a qu'une seule et unique instance de FenetreBeAnArtist
	private static FenetreBeAnArtist instance;
	private PanneauDessin panneauDessin;
	private PanneauBarreOutils panneauBarreOutils;
	private BarreDeMenu menuBarre;
	
	private String cheminFichierSauvegarde;
	
	private boolean commandeClavierFaite;
	
	private boolean fenetreReduite;
	
	/**
	 * Construit la fenetre BeAnArtist
	 */
	private FenetreBeAnArtist(){
		super(FenetreBeAnArtist.TITRE_PAR_DEFAUT);
		
		this.setIconImage(new ImageIcon(FenetreBeAnArtist.class.getResource("/fr/eseo/gpi/beanartist/res/iconeFenetre.png")).getImage());
		FenetreBeAnArtist.instance = this;
		
		this.cheminFichierSauvegarde = null;
		this.fenetreReduite = false;
		
		this.panneauDessin = new PanneauDessin(this);
		this.panneauBarreOutils = new PanneauBarreOutils(this);
		this.menuBarre = new BarreDeMenu(this.panneauDessin);
		this.menuBarre.modeSelection(false);
		this.menuBarre.modeDessin(false);
		this.panneauDessin.addMouseMotionListener(this.panneauBarreOutils.getCadranCurseur());
		this.panneauDessin.addMouseListener(this.panneauBarreOutils.getCadranCurseur());
		this.panneauBarreOutils.modeSelection(false);
		this.add(this.panneauDessin, BorderLayout.CENTER);
		this.add(this.panneauBarreOutils, BorderLayout.EAST);
		this.setJMenuBar(this.menuBarre);
			
		this.panneauDessin.addKeyListener(this);
		this.panneauBarreOutils.addKeyListener(this);
		this.menuBarre.nouveauFichier();
		
		this.setMinimumSize(new Dimension(this.panneauDessin.getWidth() + this.panneauBarreOutils.getWidth(), this.panneauDessin.getHeight()));
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(this);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		Toolkit tool = getToolkit();
		int largeurEcran = (int) tool.getScreenSize().getWidth(); 
		int hauteurEcran = (int) tool.getScreenSize().getHeight();
		
		this.pack();
		this.setLocation((largeurEcran - this.getWidth())/2, (hauteurEcran - this.getHeight())/2);
	}
	
	/**
	 * Retourne la seule et unique instance de la classe FenetreBeAnArtist
	 * @return Lunique instance de FenetreBeAnArtist
	 */
	public static FenetreBeAnArtist getInstance(){
		if (FenetreBeAnArtist.instance == null){
			FenetreBeAnArtist.instance = new FenetreBeAnArtist();
		}
		return FenetreBeAnArtist.instance;
	}
	
	/**
	 * Renvoie le {@link fr.eseo.gpi.beanartist.vue.ui.PanneauDessin} de la fenetre BeAnArtist
	 * @return Le {@link fr.eseo.gpi.beanartist.vue.ui.PanneauDessin} de la fenetre BeAnArtist
	 */
	public PanneauDessin getPanneauDessin(){
		return this.panneauDessin;
	}
	
	/**
	 * Renvoie le {@link fr.eseo.gpi.beanartist.vue.ui.PanneauBarreOutils} de la fenetre BeAnArtist
	 * @return Le {@link fr.eseo.gpi.beanartist.vue.ui.PanneauBarreOutils} de la fenetre BeAnArtist
	 */
	public PanneauBarreOutils getPanneauBarreOutils() {
		return this.panneauBarreOutils;
	}

	/**
	 * Renvoie la barre de menu {@link fr.eseo.gpi.beanartist.vue.ui.BarreDeMenu} de la fenetre BeAnArtist
	 * @return Le {@link fr.eseo.gpi.beanartist.vue.ui.BarreDeMenu} de la fenetre BeAnArtist
	 */
	public BarreDeMenu getMenuBarre(){
		return this.menuBarre;
	}
	
	/**
	 * Renvoie le chemin utilisé actuellement pour sauvegarder le fichier ouvert 
	 * @return Le chemin utilisé actuellement pour sauvegarder le fichier ouvert, null si le fichier est nouveau
	 */
	public String getCheminSauvegarde(){
		return this.cheminFichierSauvegarde;
	}
	
	/**
	 * Modifie le chemin utilisé actuellement pour sauvegarder le fichier ouvert 
	 * @param cheminSauvegarde Le nouveau chemin de sauvegarde du fichier
	 */
	public void setCheminSauvegarde(String cheminSauvegarde){
		this.cheminFichierSauvegarde = cheminSauvegarde;
		if(this.cheminFichierSauvegarde == null){
			this.menuBarre.nouveauFichier();
		}
		else{
			this.menuBarre.autoriserEnregistrer();
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getModifiers() == KeyEvent.CTRL_MASK && e.getKeyCode() == KeyEvent.VK_S && !this.commandeClavierFaite && this.cheminFichierSauvegarde == null){
			this.commandeClavierFaite = true;
			new ActionEnregistrer().actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ActionEnregistrer.NOM_ACTION));
			
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		this.commandeClavierFaite = false;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		if(this.getTitle().equals(FenetreBeAnArtist.TITRE_PAR_DEFAUT)){
			int retour;
			if(this.fenetreReduite){
				retour = JOptionPane.showConfirmDialog(null, "Etes-vous sûr de vouloir quitter sans sauvegarder ?", "Quitter sans sauvegarder", JOptionPane.YES_NO_OPTION);
			}
			else{
				retour = JOptionPane.showConfirmDialog(this, "Etes-vous sûr de vouloir quitter sans sauvegarder ?", "Quitter sans sauvegarder", JOptionPane.YES_NO_OPTION);
			}
			if(retour == JOptionPane.OK_OPTION){
				this.dispose();
				System.exit(0);
			}
		}
		else{
			this.dispose();
			System.exit(0);
		}
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowIconified(WindowEvent e) {
		this.fenetreReduite = true;
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		this.fenetreReduite = false;
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}
