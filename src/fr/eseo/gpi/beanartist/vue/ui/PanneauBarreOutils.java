package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import fr.eseo.gpi.beanartist.controleur.actions.ActionChoisirCouleurLigne;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEffacer;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrer;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrerSVG;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrerXML;
import fr.eseo.gpi.beanartist.controleur.actions.ActionForme;
import fr.eseo.gpi.beanartist.controleur.actions.ActionImporter;
import fr.eseo.gpi.beanartist.controleur.actions.ActionOuvrir;
import fr.eseo.gpi.beanartist.controleur.actions.ActionSelectionner;

import java.util.Enumeration;

/**
 * Classe modélisant le panneau des outils à droite de la fenetre BeAnArtist
 * @author guiguibubu
 *
 */
public class PanneauBarreOutils extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int LARGEUR_PAR_DEFAUT = 200;
	public static final int HAUTEUR_PAR_DEFAUT = 600;
	public static final Color COULEUR_FOND_PAR_DEFAUT = Color.LIGHT_GRAY;
	private static final int HAUTEUR_BOUTTON = 40;
	private static Dimension dimensionBoutton = new Dimension(PanneauBarreOutils.LARGEUR_PAR_DEFAUT,PanneauBarreOutils.HAUTEUR_BOUTTON);
	
	private CadranCurseur cadranCurseur;
	
	private JButton bouttonEffacer;
	
	private ButtonGroup groupeBouttons;
	
	private JToggleButton bouttonRectangle;
	private JToggleButton bouttonEllipse;
	private JToggleButton bouttonCarre;
	private JToggleButton bouttonCercle;
	private JToggleButton bouttonLigne;
	private JToggleButton bouttonTrace;
	
	private JToggleButton bouttonSelection;
	private JToggleButton bouttonMain;
	
	private JButton bouttonCouleur;
	
	private JButton bouttonSauvegarde;
	private JButton bouttonOuverture;
	private JButton bouttonImporter;
	private JButton bouttonExporterXML;
	private JButton bouttonExporterSVG;
	
	private BoxLayout layout;
	
	private FenetreBeAnArtist fenetre;
	/**
	 * Construit le panneau des outils pour la {@link fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist}
	 */
	public PanneauBarreOutils(FenetreBeAnArtist fenetre) {
		super();
		this.fenetre= fenetre;
		this.setPreferredSize(new Dimension(PanneauBarreOutils.LARGEUR_PAR_DEFAUT, PanneauBarreOutils.HAUTEUR_PAR_DEFAUT ));
		this.setBackground(PanneauBarreOutils.COULEUR_FOND_PAR_DEFAUT);
		this.layout = new BoxLayout(this,BoxLayout.Y_AXIS);
		this.setLayout(this.layout);
		this.initComponents();
	}
	
	private void initComponents(){
		
		this.bouttonEffacer = new JButton();
		this.bouttonEffacer.setAction(new ActionEffacer());
		this.bouttonEffacer.setText("Tout Effacer");
		this.bouttonEffacer.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeEffacer.png")));
		this.bouttonEffacer.setActionCommand(ActionEffacer.NOM_ACTION);
		this.bouttonEffacer.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.bouttonEffacer.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		
		this.bouttonRectangle = new JToggleButton();
		this.bouttonRectangle.setAction(new ActionForme());
		this.bouttonRectangle.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeRectangle.png")));
		this.bouttonRectangle.setText("Rectangle");
		this.bouttonRectangle.setActionCommand(ActionForme.NOM_ACTION_RECTANGLE);
		
		this.bouttonCarre = new JToggleButton();
		this.bouttonCarre.setAction(new ActionForme());
		this.bouttonCarre.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeCarre.png")));
		this.bouttonCarre.setText("Carré");
		this.bouttonCarre.setActionCommand(ActionForme.NOM_ACTION_CARRE);
		
		this.bouttonEllipse = new JToggleButton();
		this.bouttonEllipse.setAction(new ActionForme());
		this.bouttonEllipse.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeEllipse.png")));
		this.bouttonEllipse.setText("Ellipse");
		this.bouttonEllipse.setActionCommand(ActionForme.NOM_ACTION_ELLIPSE);
		
		this.bouttonCercle = new JToggleButton();
		this.bouttonCercle.setAction(new ActionForme());
		this.bouttonCercle.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeCercle.png")));
		this.bouttonCercle.setText("Cercle");
		this.bouttonCercle.setActionCommand(ActionForme.NOM_ACTION_CERCLE);
		
		this.bouttonLigne = new JToggleButton();
		this.bouttonLigne.setAction(new ActionForme());
		this.bouttonLigne.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeLigne.png")));
		this.bouttonLigne.setText("Ligne");
		this.bouttonLigne.setActionCommand(ActionForme.NOM_ACTION_LIGNE);
		
		this.bouttonTrace = new JToggleButton();
		this.bouttonTrace.setAction(new ActionForme());
		this.bouttonTrace.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeTrace.png")));
		this.bouttonTrace.setText("Tracé");
		this.bouttonTrace.setActionCommand(ActionForme.NOM_ACTION_TRACE);
		
		ActionSelectionner actionSelectionner = new ActionSelectionner();
		this.bouttonSelection = new JToggleButton();
		this.bouttonSelection.setAction(actionSelectionner);
		this.bouttonSelection.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeSelection.png")));
		this.bouttonSelection.setText("Sélection");
		this.bouttonSelection.setActionCommand(ActionSelectionner.NOM_ACTION);
		
		this.bouttonMain = new JToggleButton();
		this.bouttonMain.setAction(actionSelectionner);
		this.bouttonMain.setText("Outil Main");
		this.bouttonMain.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeMain.png")));
		this.bouttonMain.setActionCommand(ActionSelectionner.NOM_ACTION_DEPLACEMENT);
		
		this.bouttonCouleur = new JButton();
		this.bouttonCouleur.setAction(new ActionChoisirCouleurLigne(this.fenetre.getPanneauDessin(), false));
		this.bouttonCouleur.setText("Choix Couleur");
		this.bouttonCouleur.setActionCommand(ActionChoisirCouleurLigne.NOM_ACTION);
		this.bouttonCouleur.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.bouttonCouleur.setPreferredSize(PanneauBarreOutils.dimensionBoutton);
		this.bouttonCouleur.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		
		this.bouttonOuverture = new JButton();
		this.bouttonOuverture.setAction(new ActionOuvrir());
		this.bouttonOuverture.setText("Ouvrir fichier");
		this.bouttonOuverture.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeOuvrir.png")));
		this.bouttonOuverture.setActionCommand(ActionOuvrir.NOM_ACTION);
		this.bouttonOuverture.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.bouttonOuverture.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		
		this.bouttonSauvegarde = new JButton();
		this.bouttonSauvegarde.setAction(new ActionEnregistrer());
		this.bouttonSauvegarde.setText("Sauvegarde");
		this.bouttonSauvegarde.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeSauvegarder.png")));
		this.bouttonSauvegarde.setActionCommand(ActionEnregistrer.NOM_ACTION);
		this.bouttonSauvegarde.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.bouttonSauvegarde.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		
		this.bouttonImporter = new JButton();
		this.bouttonImporter.setAction(new ActionImporter());
		this.bouttonImporter.setText("Importer XML");
		this.bouttonImporter.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeImporter.png")));
		this.bouttonImporter.setActionCommand(ActionImporter.NOM_ACTION);
		this.bouttonImporter.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.bouttonImporter.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		
		this.bouttonExporterXML = new JButton();
		this.bouttonExporterXML.setAction(new ActionEnregistrerXML());
		this.bouttonExporterXML.setText("Exporter XML");
		this.bouttonExporterXML.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeXML.png")));
		this.bouttonExporterXML.setActionCommand(ActionEnregistrerXML.NOM_ACTION);
		this.bouttonExporterXML.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.bouttonExporterXML.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		
		this.bouttonExporterSVG = new JButton();
		this.bouttonExporterSVG.setAction(new ActionEnregistrerSVG());
		this.bouttonExporterSVG.setText("Exporter SVG");
		this.bouttonExporterSVG.setIcon(new ImageIcon(PanneauBarreOutils.class.getResource("/fr/eseo/gpi/beanartist/res/iconeSVG.png")));
		this.bouttonExporterSVG.setActionCommand(ActionEnregistrerSVG.NOM_ACTION);
		this.bouttonExporterSVG.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.bouttonExporterSVG.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		
		
		this.add(this.bouttonEffacer);
		this.add(this.bouttonRectangle);
		this.add(this.bouttonCarre);
		this.add(this.bouttonEllipse);
		this.add(this.bouttonCercle);
		this.add(this.bouttonLigne);
		this.add(this.bouttonTrace);
		this.add(this.bouttonSelection);
		this.add(this.bouttonMain);
		this.add(this.bouttonCouleur);
		this.add(this.bouttonOuverture);
		this.add(this.bouttonSauvegarde);
		this.add(this.bouttonImporter);
		this.add(this.bouttonExporterXML);
		this.add(this.bouttonExporterSVG);
		
		this.cadranCurseur = new CadranCurseur(PanneauBarreOutils.LARGEUR_PAR_DEFAUT, PanneauBarreOutils.HAUTEUR_BOUTTON);

		this.add(this.cadranCurseur);
		
		this.groupeBouttons = new ButtonGroup();
		
		this.groupeBouttons.add(this.bouttonRectangle);
		this.groupeBouttons.add(this.bouttonCarre);
		this.groupeBouttons.add(this.bouttonEllipse);
		this.groupeBouttons.add(this.bouttonCercle);
		this.groupeBouttons.add(this.bouttonLigne);
		this.groupeBouttons.add(this.bouttonTrace);
		this.groupeBouttons.add(this.bouttonSelection);
		this.groupeBouttons.add(this.bouttonMain);
		
		AbstractButton boutton;
		boutton = this.bouttonCarre;
		for (Enumeration<AbstractButton> enumBoutton = this.groupeBouttons.getElements(); enumBoutton.hasMoreElements();){
			boutton = enumBoutton.nextElement();
			boutton.setAlignmentX(Component.CENTER_ALIGNMENT);
			boutton.setMaximumSize(PanneauBarreOutils.dimensionBoutton);
		}
	}
	
	/**
	 * Renvoie le groupe de bouttons de choix des formes à dessiner
	 * @return Le groupe de bouttons de choix des formes à dessiner
	 */
	public ButtonGroup getGroupeBoutton(){
		return this.groupeBouttons;
	}
	
	/**
	 * Renvoie le {@link fr.eseo.gpi.beanartist.vue.ui.CadranCurseur} de la fenetre BeAnArtist
	 * @return Le {@link fr.eseo.gpi.beanartist.vue.ui.CadranCurseur} de la fenetre BeAnArtist
	 */
	public CadranCurseur getCadranCurseur(){
		return this.cadranCurseur;
	}
	
	/**
	 * Active/désactive le bouton Main
	 * @param modeSelection true, active le boutton Main, et false le désactive
	 */
	public void modeSelection(boolean modeSelection){
		if(modeSelection){
			this.bouttonMain.setEnabled(true);
		}
		else{
			this.bouttonMain.setEnabled(false);
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.cadranCurseur.paintComponent(g);
	}
}
