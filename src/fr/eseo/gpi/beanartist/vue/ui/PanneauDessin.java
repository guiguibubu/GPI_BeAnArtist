package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;

import fr.eseo.gpi.beanartist.controleur.actions.ActionAnnulerSelection;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoisirCouleurLigne;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixCouleurFond;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixCouleurRemplissage;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixCouleurTrait;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoixEpaisseurBordure;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEffacerSelection;
import fr.eseo.gpi.beanartist.controleur.actions.ActionToutSelectionner;
import fr.eseo.gpi.beanartist.controleur.outils.Outil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilForme;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueCoin;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.formes.VueSelection;

/**
 * Classe modélisant le panneau sur lequel on dessine
 * @author guiguibubu
 *
 */
public class PanneauDessin extends JPanel implements MouseListener, MouseWheelListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int LARGEUR_PAR_DEFAUT = 600;
	public static final int HAUTEUR_PAR_DEFAUT = 700;
	public static final Color COULEUR_FOND_PAR_DEFAUT = Color.WHITE;
	public static final Color COULEUR_LIGNE_PAR_DEFAUT = Forme.COULEUR_LIGNE_PAR_DEFAUT;
	public static final int ZOOM_PAR_DEFAUT = 1;
	
	private FenetreBeAnArtist fenetreBeAnArtist;
	private int zoom;
	private Rectangle rectangleEnglobantForme;
	
	private Outil outilCourant;
	
	private List<VueForme> vueFormes;
	
	private Color couleurLigneCourante;
	
	private JPopupMenu menuContextuel;
	private JMenu menuCouleurFond;
	private JMenu menuCouleurLigne;
	private JMenu menuCouleurTraitSelection;
	private JMenu menuCouleurRemplissage;
	private JMenu menuEpaisseurLigne;
	
	private JMenuItem itemCouleurFondParDefaut;
	private JMenuItem itemCouleurFondChoix;
	private JMenuItem itemCouleurLigneParDefaut;
	private JMenuItem itemCouleurLigneChoix;
	private JMenuItem itemCouleurTraitSelectionParDefaut;
	private JMenuItem itemCouleurTraitSelectionChoix;
	private JMenuItem itemCouleurRemplissageParDefaut;
	private JMenuItem itemCouleurRemplissageChoix;
	private JMenuItem itemEpaisseurLigneChoix;
	private JMenuItem itemEpaisseurLigneParDefaut;
	private JMenuItem itemEffacerSelection;
	private JMenuItem itemToutSelectionner;
	private JMenuItem itemAnnulerSelection;	
	
	/**
	 * Construit un panneau de dessin
	 * @param largeur	La largeur du panneau créé
	 * @param hauteur	La hauteur du panneau créé
	 * @param fond		La couleur de fond du panneau créé
	 */
	public PanneauDessin(int largeur, int hauteur, Color fond) {
		super();
		this.vueFormes = new ArrayList<VueForme>(); 
		this.setPreferredSize(new Dimension(largeur, hauteur));
		this.setBackground(fond);
		this.couleurLigneCourante = PanneauDessin.COULEUR_LIGNE_PAR_DEFAUT;
		this.initMenuContextextuel();
		this.addMouseListener(this);
		this.modeSelection(false);
		this.zoom = PanneauDessin.ZOOM_PAR_DEFAUT;
		this.addMouseWheelListener(this);
		this.rectangleEnglobantForme = new Rectangle();
	}

	/**
	 * Construit un panneau de dessin avec les dimensions par défaut 
	 * ({@value fr.eseo.gpi.beanartist.vue.ui.PanneauDessin#LARGEUR_PAR_DEFAUT},{@value fr.eseo.gpi.beanartist.vue.ui.PanneauDessin#LARGEUR_PAR_DEFAUT}), 
	 * et la couleur de fond par défaut
	 */
	public PanneauDessin() {
		this(PanneauDessin.LARGEUR_PAR_DEFAUT, PanneauDessin.HAUTEUR_PAR_DEFAUT,PanneauDessin.COULEUR_FOND_PAR_DEFAUT);
	}

	/**
	 * Construit un panneau de dessin avec les dimensions par défaut 
	 * ({@value fr.eseo.gpi.beanartist.vue.ui.PanneauDessin#LARGEUR_PAR_DEFAUT},{@value fr.eseo.gpi.beanartist.vue.ui.PanneauDessin#LARGEUR_PAR_DEFAUT}), 
	 * et la couleur de fond par défaut
	 * @param fenetre La fenetre BeAnArtist dans laquelle est le panneau dessin
	 */
	public PanneauDessin(FenetreBeAnArtist fenetre){
		this();
		this.fenetreBeAnArtist = fenetre;
	}
	
	private void initMenuContextextuel(){
		
		this.menuContextuel = new JPopupMenu();
		this.menuCouleurFond = new JMenu("Couleur de fond");
		this.menuCouleurLigne = new JMenu("Couleur du trait");
		this.itemCouleurFondParDefaut = new JMenuItem("Couleur de fond par défaut");
		this.itemCouleurFondChoix = new JMenuItem("Choix couleur de fond", new ImageIcon(PanneauDessin.class.getResource("/fr/eseo/gpi/beanartist/res/iconeFondMini.png")));
		this.itemCouleurLigneParDefaut = new JMenuItem("Couleur du trait par défaut");
		this.itemCouleurLigneChoix = new JMenuItem("Choix couleur du trait");
		this.itemEffacerSelection = new JMenuItem("Effacer sélection");
		this.itemToutSelectionner = new JMenuItem("Tout sélectionner");
		this.itemAnnulerSelection = new JMenuItem("Annuler Sélection");
		
		ActionListener actionChoixCouleurFond = new ActionChoixCouleurFond(this, false);
		ActionListener actionChoixCouleurFondParDefaut = new ActionChoixCouleurFond(this, true);
		ActionListener actionChoixCouleurTraitDessin = new ActionChoisirCouleurLigne(this, false);
		ActionListener actionChoixCouleurTraitDessinParDefaut = new ActionChoisirCouleurLigne(this, true);
		
		this.itemCouleurFondParDefaut.addActionListener(actionChoixCouleurFondParDefaut);
		this.itemCouleurFondChoix.addActionListener(actionChoixCouleurFond);
		this.itemCouleurLigneChoix.addActionListener(actionChoixCouleurTraitDessin);
		this.itemCouleurLigneParDefaut.addActionListener(actionChoixCouleurTraitDessinParDefaut);
		
		this.menuCouleurFond.add(this.itemCouleurFondParDefaut);
		this.menuCouleurFond.add(this.itemCouleurFondChoix);
		
		this.menuCouleurLigne.add(this.itemCouleurLigneParDefaut);
		this.menuCouleurLigne.add(this.itemCouleurLigneChoix);
		
		this.menuContextuel.add(this.menuCouleurLigne);
		this.menuContextuel.add(this.menuCouleurFond);
		
		this.menuCouleurTraitSelection = new JMenu("Couleur du trait sélection");
		this.menuCouleurRemplissage = new JMenu("Couleur remplissage sélection");
		this.menuEpaisseurLigne = new JMenu("Epaisseur bordure sélection");
		
		this.itemCouleurTraitSelectionParDefaut = new JMenuItem("Couleur du trait sélection par défaut");
		this.itemCouleurTraitSelectionChoix = new JMenuItem("Choix couleur du trait sélection");
		this.itemCouleurRemplissageChoix = new JMenuItem("Choix couleur remplissage");
		this.itemCouleurRemplissageParDefaut = new JMenuItem("Pas de remplissage");
		this.itemEffacerSelection = new JMenuItem("Effacer sélection");
		this.itemToutSelectionner = new JMenuItem("Tout sélectionner");
		this.itemAnnulerSelection = new JMenuItem("Annuler Sélection");
		this.itemEpaisseurLigneChoix = new JMenuItem("Choix épaisseur");
		this.itemEpaisseurLigneParDefaut = new JMenuItem("Epaisseur par défaut");

		ActionListener actionChoixCouleurTraitSelection = new ActionChoixCouleurTrait(this, false);
		ActionListener actionChoixCouleurTraitSelectionParDefaut = new ActionChoixCouleurTrait(this, true);
		ActionListener actionChoixCouleurRemplissageSelection = new ActionChoixCouleurRemplissage(this, false);
		ActionListener actionChoixCouleurRemplissageSelectionParDefaut = new ActionChoixCouleurRemplissage(this, true);
		ActionListener actionChoixEpaisseurBordureSelection = new ActionChoixEpaisseurBordure(this, false);
		ActionListener actionChoixEpaisseurBordureSelectionParDefaut = new ActionChoixEpaisseurBordure(this, true);
		
		this.itemCouleurTraitSelectionParDefaut.addActionListener(actionChoixCouleurTraitSelectionParDefaut);
		this.itemCouleurTraitSelectionChoix.addActionListener(actionChoixCouleurTraitSelection);
		this.itemCouleurRemplissageParDefaut.addActionListener(actionChoixCouleurRemplissageSelectionParDefaut);
		this.itemCouleurRemplissageChoix.addActionListener(actionChoixCouleurRemplissageSelection);
		this.itemEffacerSelection.addActionListener(new ActionEffacerSelection(this));
		this.itemToutSelectionner.addActionListener(new ActionToutSelectionner(this));
		this.itemAnnulerSelection.addActionListener(new ActionAnnulerSelection(this));
		this.itemEpaisseurLigneChoix.addActionListener(actionChoixEpaisseurBordureSelection);
		this.itemEpaisseurLigneParDefaut.addActionListener(actionChoixEpaisseurBordureSelectionParDefaut);
		
		this.itemToutSelectionner.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK));
		this.itemEffacerSelection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));	
		
		this.menuCouleurTraitSelection.add(this.itemCouleurTraitSelectionParDefaut);
		this.menuCouleurTraitSelection.add(this.itemCouleurTraitSelectionChoix);
		
		this.menuCouleurRemplissage.add(this.itemCouleurRemplissageParDefaut);
		this.menuCouleurRemplissage.add(this.itemCouleurRemplissageChoix);
		
		this.menuEpaisseurLigne.add(this.itemEpaisseurLigneParDefaut);
		this.menuEpaisseurLigne.add(this.itemEpaisseurLigneChoix);
	}
	
	/**
	 * Renvoie le menu contextuel s'affichant sur le panneau dessin
	 * @return Le menu contextuel s'affichant sur le panneau dessin
	 */
	public JPopupMenu getMenuContextuel() {
		return menuContextuel;
	}

	/**
	 * Renvoie la liste des {@link fr.eseo.gpi.beanartist.vue.formes.VueForme} dessinées sur le panneau
	 * @return La liste des {@link fr.eseo.gpi.beanartist.vue.formes.VueForme} dessinées sur le panneau
	 */
	public List<VueForme> getVueFormes(){
		return this.vueFormes;
	}
	
	/**
	 * Ajoute une {@link fr.eseo.gpi.beanartist.vue.formes.VueForme} au panneau de dessin
	 * @param vueForme La {@link fr.eseo.gpi.beanartist.vue.formes.VueForme} à ajouter
	 */
	public void ajouterVueForme(VueForme vueForme){
		this.vueFormes.add(vueForme);
	}
	
	/**
	 * Renvoie l'{@link fr.eseo.gpi.beanartist.controleur.outils.Outil} actuellement lié au panneau de dessin
	 * @return	L'{@link fr.eseo.gpi.beanartist.controleur.outils.Outil} courant lié au panneau, null si aucun n'est lié
	 */
	public Outil getOutilCourant(){
		return this.outilCourant;
	}
	
	/**
	 * Modifie l'{@link fr.eseo.gpi.beanartist.controleur.outils.Outil} lié au panneau dessin
	 * @param outil Le nouvel {@link fr.eseo.gpi.beanartist.controleur.outils.Outil} lié au panneau
	 */
	public void setOutilCourant(Outil outil){
		if(outil != null){
			//on libère l'outil de ses précédentes attaches
			if(outil.getPanneauDessin() != this){
				if(outil.getPanneauDessin() != null){
					outil.associer(null);
				}
			}		
		}
		this.annuleSelection();
		this.outilCourant = outil;
		
		if(this.fenetreBeAnArtist != null){
			if(this.outilCourant != null && this.outilCourant instanceof OutilSelection){
				this.fenetreBeAnArtist.getMenuBarre().modeSelection(true);
			}
			else{
				this.fenetreBeAnArtist.getMenuBarre().modeSelection(false);
			}
			if(this.outilCourant != null && this.outilCourant instanceof OutilForme){
				this.fenetreBeAnArtist.getMenuBarre().modeDessin(true);
			}
			else{
				this.fenetreBeAnArtist.getMenuBarre().modeDessin(false);
			}
		}
	}

	/**
	 * Désélectionne toutes les formes du panneau
	 */
	public void annuleSelection(){
		for(VueForme vue : this.vueFormes){
			vue.setSelectionne(false);
		}
		if(this.outilCourant instanceof OutilSelection){
			((OutilSelection)this.outilCourant).annulerSelection();
		}
		this.repaint();
	}
	
	/**
	 * Indique au panneau si on est dans le mode de sélection ou de dessin qui alors ajoute/retire les items nécessaires du menu contextuel
	 * @param modeSelection Indique si on veux dessiner ou sélectionner sur le panneau
	 */
	public void modeSelection(boolean modeSelection){
		if(modeSelection){
			this.ajouteMenuContextuel();
		}
		else{
			this.retireMenuContextuel();
		}
	}
	
	private void ajouteMenuContextuel(){
		this.menuContextuel.add(this.menuCouleurTraitSelection);
		this.menuContextuel.add(this.menuEpaisseurLigne);
		this.menuContextuel.add(this.menuCouleurRemplissage);
		
		this.menuContextuel.add(this.itemToutSelectionner);
		this.menuContextuel.add(this.itemAnnulerSelection);
		this.menuContextuel.add(this.itemEffacerSelection);
	}
	
	private void retireMenuContextuel(){
		this.menuContextuel.remove(this.menuCouleurTraitSelection);
		this.menuContextuel.remove(this.menuEpaisseurLigne);
		this.menuContextuel.remove(this.menuCouleurRemplissage);
		
		this.menuContextuel.remove(this.itemToutSelectionner);
		this.menuContextuel.remove(this.itemAnnulerSelection);
		this.menuContextuel.remove(this.itemEffacerSelection);
	}
	
	/**
	 * Renvoie la couleur de dessin des traits
	 * @return La couleur de dessin des traits
	 */
	public Color getCouleurLigneCourante(){
		return this.couleurLigneCourante;
	}
	
	/**
	 * Modifie la couleur de dessin des traits
	 * @param couleur La nouvelle couleur de dessin des traits
	 */
	public void setCouleurLigneCourante(Color couleur){
		this.couleurLigneCourante = couleur;
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D)g.create();
		for (VueForme vueForme : this.vueFormes){
			vueForme.affiche(g2D);
		}
		g2D.dispose();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.isPopupTrigger()){
			this.menuContextuel.show(this, e.getX(), e.getY());
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		for(VueForme vue : this.vueFormes){
			if(!(vue instanceof VueCoin) && !(vue instanceof VueSelection)){
				this.rafraichirNouvelEncadrant(-e.getWheelRotation()*this.zoom);
			}
		}
		this.repaint();
	}

	/**
	 * Renvoie la vitesse de zoom
	 * @return La vitesse de zoom
	 */
	public int getZoom() {
		return this.zoom;
	}

	/**
	 * Modifie la vitesse de zoom
	 * @param zoom La nouvelle vitesse de zoom
	 */
	public void setZoom(int zoom) {
		this.zoom = zoom;
	}
	
	/**
	 * Modifie la largeur des formes sur le panneau
	 * @param newLargeur la nouvelle largeur de l'ensemble des formes du panneau
	 */
	
	private void setLargeurSelection(double newLargeur){
		double oldLargeur = this.rectangleEnglobantForme.getLargeur();
		if(oldLargeur > 0 && newLargeur > 0){
			double ratio = newLargeur/oldLargeur;
	 		for (VueForme vue : this.vueFormes){
	 			double distanceAZero = vue.getForme().getCadreMinX() - this.rectangleEnglobantForme.getX();
 				double newX = ratio*distanceAZero + this.rectangleEnglobantForme.getX();
 				if(vue instanceof VueLigne && ((Ligne)vue.getForme()).getLargeur() < 0){
 					newX += Math.abs(vue.getForme().getLargeur());
 				}
 				vue.getForme().setX(newX);
 				if(!(vue instanceof VueCoin)){
 					vue.getForme().setLargeur(ratio*vue.getForme().getLargeur());
 				}
 			}
		}
		else{
			for (VueForme vue : this.vueFormes){
				vue.getForme().setX(this.rectangleEnglobantForme.getX());
				vue.getForme().setLargeur(1);
			}
		}
	}
	
	/**
	 * Modifie la hauteur des formes du panneau
	 * @param newHauteur la nouvelle hauteur de l'ensemble des formes du panneau
	 */
	
	private void setHauteurSelection(double newHauteur){
		double oldHauteur = this.rectangleEnglobantForme.getHauteur();
		if(oldHauteur > 0 && newHauteur > 0){
			double ratio = newHauteur/oldHauteur;
	 		for (VueForme vue : this.vueFormes){
	 			double distanceAZero = vue.getForme().getCadreMinY() - this.rectangleEnglobantForme.getY();
 				double newY = ratio*distanceAZero + this.rectangleEnglobantForme.getY();
 				if(vue instanceof VueLigne && ((Ligne)vue.getForme()).getHauteur() < 0){
 					newY += vue.getForme().getHauteur();
 				}
 				vue.getForme().setY(newY);
 				if(!(vue instanceof VueCoin)){		
 					vue.getForme().setHauteur(ratio*vue.getForme().getHauteur());
 				}
			}
		}
		else{
			for (VueForme vue : this.vueFormes){
				vue.getForme().setY(this.rectangleEnglobantForme.getY());
				vue.getForme().setHauteur(1);
			}
		}
	}
	
	/**
	 * Modifie la position, la largeur et la hauteur des formes du panneau pour qu'elles soient dans le nouveau encadrant
	 * @param xMin l'abscisse minimum du nouvelle encadrant
	 * @param xMax l'abscisse maximum du nouvelle encadrant
	 * @param yMin l'ordonnée minimum du nouvelle encadrant
	 * @param yMax l'ordonnée maximum du nouvelle encadrant
	 */
	private void rafraichirNouvelEncadrant(double zoom){
		double xMin = this.getWidth(), xMax = 0, yMin = this.getHeight(), yMax = 0;
		for(VueForme vue : this.vueFormes){
			xMin = (vue.getForme().getCadreMinX() < xMin) ? vue.getForme().getCadreMinX() : xMin;
			xMax = (vue.getForme().getCadreMaxX() > xMax) ? vue.getForme().getCadreMaxX() : xMax; 
			yMin = (vue.getForme().getCadreMinY() < yMin) ? vue.getForme().getCadreMinY() : yMin; 
			yMax = (vue.getForme().getCadreMaxY() > yMax) ? vue.getForme().getCadreMaxY() : yMax; 
		}
		this.rectangleEnglobantForme.setPosition(xMin, yMin);
		this.rectangleEnglobantForme.setHauteur(yMax-yMin);
		this.rectangleEnglobantForme.setLargeur(xMax-xMin);
		this.setLargeurSelection((xMax - xMin) + zoom);
		this.setHauteurSelection((yMax - yMin) + zoom);
		this.repaint();
	}
}