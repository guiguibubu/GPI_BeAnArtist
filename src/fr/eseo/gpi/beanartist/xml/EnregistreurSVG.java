package fr.eseo.gpi.beanartist.xml;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.List;

import org.w3c.dom.Element;

import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

/**
 * Un enregistreur SVG est un processeur DOM responsable de l'enregistrement
 * d'un dessin au format SVG standard.
 * 
 * Il suit exactement les mêmes principes que ceux de l'enregistreur XML (classe
 * EnregistreurXML). Les méthodes enregistreDessin et creeElementXxxx devront
 * être complétées. Des méthodes utilitaires pourront venir compléter celles
 * définies par la classe ProcesseurDOM ; elles devront dans ce cas être
 * OBLIGATOIREMENT définies en "private" à la fin de la classe EnregistreurXML.
 *
 */
public class EnregistreurSVG extends ProcesseurDOM {

	/**
	 * Lance le test d'enristrement (méthode teste) avec le fichier XML d'entrée
	 * nommé "S30-Dessin-in.xml" et le fichier SVG de sortie nommé
	 * "S30-Dessin-out.svg".
	 * 
	 * Vérifier la conformité du fichier de sortie en l'affichant avec un
	 * navigateur Web et en comparant cet affichage avec celui fourni par la
	 * méthode LecteurXML.main.
	 * 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		teste("S30-Dessin-in.xml", "S30-Dessin-out.svg");
	}

	/**
	 * Teste l'enregistrement du dessin dans un fichier SVG. Le fichier XML
	 * d'entrée est préalablement lu, puis sauvagardé dans un fichier de sortie
	 * au format SVG.
	 * 
	 * @param nomFichierEntrée le nom du fichier XML d'entrée lu
	 * @param nomFichierSortie le nom du fichier SVG de sortie écrit
	 * @throws FileNotFoundException si l'un des noms des fichiers n'est pas
	 *             valide
	 */
	public static void teste(String nomFichierEntrée, String nomFichierSortie) throws FileNotFoundException {
		LecteurXML lecteur = new LecteurXML();
		LecteurXML.teste(nomFichierEntrée);
		final List<VueForme> dessin = lecteur.lisDessin(nomFichierEntrée);
		EnregistreurSVG enregistreur = new EnregistreurSVG();
		enregistreur.enregistreDessin(nomFichierSortie, dessin);
	}

	/**
	 * Enregistre le dessin donné dans un fichier SVG.
	 * @param nomFichier le nom du fichier SVG de sauvegarde
	 * @param dessin le dessin formé de la liste des vues des formes
	 * @throws FileNotFoundException si le nom du fichier n'est pas valide
	 */
	public void enregistreDessin(String nomFichier, List<VueForme> dessin) throws FileNotFoundException {
		this.creeDocumentSVG();
		Element racine = getDocument().getDocumentElement();
		this.ecrisAttribut(racine, "width", FenetreBeAnArtist.getInstance().getPanneauDessin().getWidth());
		this.ecrisAttribut(racine, "height", FenetreBeAnArtist.getInstance().getPanneauDessin().getHeight());
		// Pour chaque vue du dessin, créer un élément DOM associé et l'ajouter
		// dans l'élément racine du document.
		for(VueForme vue : dessin){
			racine.appendChild(this.creeElementForme(vue.getForme()));
		}
		this.enregistreDocument(nomFichier);
	}

	/**
	 * Crée un élément DOM au format SVG représentant la forme donnée et
	 * retourne cet élément. Cette méthode invoque les méthodes
	 * creeElement<Forme> en fonction du type de la forme.
	 * @param forme la forme
	 * @return l'élément DOM représentant la forme
	 */
	public Element creeElementForme(Forme forme) {
		Element element;
		String nom = forme.getClass().getSimpleName();
		System.out.println(nom);
		if (nom.equals("Rectangle") || nom.equals("Carre")) {
			element = creeElementRectangle((Rectangle) forme);
		} else if (nom.equals("Ellipse")) {
			element = creeElementEllipse((Ellipse) forme);
		} else if (nom.equals("Cercle")) {
			element = creeElementCercle((Cercle) forme);
		} else if (nom.equals("Ligne")) {
			element = creeElementLigne((Ligne) forme);
		} else if (nom.equals("Trace")) {
			element = creeElementTrace((Trace) forme);
		} else {
			throw new Error("Forme non gérée");
		}
		
		element.setAttribute("stroke", this.ColortoStringRVB(forme.getCouleurLigne()));
		element.setAttribute("stroke-width", ""+forme.getEpaisseurBordure());
		return element;
	}

	/**
	 * Renvoie un nouvel élément DOM au format SVG représentant le rectangle
	 * donné.
	 * @param forme le rectangle
	 * @return élément DOM représentant le rectangle
	 */
	public Element creeElementRectangle(Rectangle forme) {
		Element element = this.getDocument().createElement("rect");
		this.ecrisAttribut(element, "x", forme.getX());
		this.ecrisAttribut(element, "y", forme.getY());
		this.ecrisAttribut(element, "width", forme.getLargeur());
		this.ecrisAttribut(element, "height", forme.getHauteur());
		if(forme.getCouleurRemplissage() != null){
			this.ecrisAttribut(element, "fill", this.ColortoStringRVB(forme.getCouleurRemplissage()));
		}
		else{
			this.ecrisAttribut(element, "fill", "none");
		}
		this.ecrisAttribut(element, "stroke-width", forme.getEpaisseurBordure());
	
		return element;
	}

	/**
	 * Renvoie un nouvel élément DOM au format SVG représentant l'ellipse
	 * donnée.
	 * @param forme l'ellipse
	 * @return élément DOM représentant l'ellipse
	 */
	public Element creeElementEllipse(Ellipse forme) {
		Element element = this.getDocument().createElement("ellipse");
		this.ecrisAttribut(element, "cx", forme.getX()+forme.getLargeur()/2);
		this.ecrisAttribut(element, "cy", forme.getY()+forme.getHauteur()/2);
		this.ecrisAttribut(element, "rx", forme.getLargeur()/2);
		this.ecrisAttribut(element, "ry", forme.getHauteur()/2);
		if(forme.getCouleurRemplissage() != null){
			this.ecrisAttribut(element, "fill", this.ColortoStringRVB(forme.getCouleurRemplissage()));
		}
		else{
			this.ecrisAttribut(element, "fill", "none");
		}
		this.ecrisAttribut(element, "stroke-width", forme.getEpaisseurBordure());
		
		return element;
	}

	/**
	 * Renvoie un nouvel élément DOM au format SVG représentant le cercle donné.
	 * @param forme le cercle
	 * @return élément DOM représentant le cercle
	 */
	public Element creeElementCercle(Cercle forme) {
		Element element = this.getDocument().createElement("circle");
		this.ecrisAttribut(element, "cx", forme.getX()+forme.getLargeur()/2);
		this.ecrisAttribut(element, "cy", forme.getY()+forme.getHauteur()/2);
		this.ecrisAttribut(element, "r", forme.getLargeur()/2);
		if(forme.getCouleurRemplissage() != null){
			this.ecrisAttribut(element, "fill", this.ColortoStringRVB(forme.getCouleurRemplissage()));
		}
		else{
			this.ecrisAttribut(element, "fill", "none");
		}
		this.ecrisAttribut(element, "stroke-width", forme.getEpaisseurBordure());
		
		return element;
	}

	/**
	 * Renvoie un nouvel élément DOM au format SVG représentant la ligne donnée.
	 * @param forme la ligne
	 * @return élément DOM représentant la ligne
	 */
	public Element creeElementLigne(Ligne forme) {
		Element element = this.getDocument().createElement("line");
		this.ecrisAttribut(element, "x1", forme.getP1().getX());
		this.ecrisAttribut(element, "y1", forme.getP1().getY());
		this.ecrisAttribut(element, "x2", forme.getP2().getX());
		this.ecrisAttribut(element, "y2", forme.getP2().getY());
		this.ecrisAttribut(element, "stroke-width", forme.getEpaisseurBordure());
		
		return element;
	}

	/**
	 * Renvoie un nouvel élément DOM au format SVG représentant le tracé donné.
	 * @param forme le tracé
	 * @return élément DOM représentant le tracé
	 */
	public Element creeElementTrace(Trace forme) {
		Element element = this.getDocument().createElement("path");
		String attribut = "M";
		attribut += ((Trace)forme).getPoints().get(0).getX()+" "+((Trace)forme).getPoints().get(0).getY();
		for(int i = 1; i < ((Trace)forme).getPoints().size(); i++){
			Point point = ((Trace)forme).getPoints().get(i);
			attribut += "L"+point.getX()+" "+point.getY()+" ";
		}
		this.ecrisAttribut(element, "d", attribut);
		this.ecrisAttribut(element, "fill", "none");
		return element;
	}
	
	private String ColortoStringRVB(Color couleur){
		System.out.println(couleur);
		return "rgb(" + couleur.getRed() + "," + couleur.getGreen() + "," + couleur.getBlue() + ")";
	}
	
	private void ecrisAttribut(Element element, String nomAttribut, String valeurAttribut) {
		element.setAttribute(nomAttribut, valeurAttribut);
	}

}
