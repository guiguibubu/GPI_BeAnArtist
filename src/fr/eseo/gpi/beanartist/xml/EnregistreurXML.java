package fr.eseo.gpi.beanartist.xml;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Text;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;

/**
 * Un enregistreur XML est un processeur DOM responsable de l'enregistrement
 * d'un dessin au format XML défini par l'application.
 * 
 * Il utilise les méthodes héritées de la classe ProcesseurDOM pour créer un
 * document DOM et l'enregistrer dans un fichier XML, ainsi que les méthodes
 * d'écriture des entiers.
 * 
 * Les méthodes enregistreDessin et creeElementXxxx devront être complétées. Des
 * méthodes utilitaires pourront venir compléter celles définies par la classe
 * ProcesseurDOM ; elles devront dans ce cas être OBLIGATOIREMENT définies en
 * "private" à la fin de la classe EnregistreurXML.
 *
 */
public class EnregistreurXML extends ProcesseurDOM {

	/**
	 * Lance le test d'enristrement (méthode teste) avec le fichier XML d'entrée
	 * nommé "S30-Dessin-in.xml" et le fichier XML de sortie nommé
	 * "S30-Dessin-out.xml".
	 * 
	 * Ce test MANUEL doit OBLIGATOIREMENT passer avant de commencer la gestion
	 * de l'enregistrement en SVG (classe EnregistreurSVG). Il est INDISPENSABLE
	 * de vérifier la conformité entre les fichiers d'entrée et de sortie. Il
	 * peut subsister des différentces normales entre ces deux fichiers (par
	 * exemple pour les valeurs par défaut telles que la couleur de remplissage
	 * des vues). De plus, l'affichage du fichier de sortie doit être conforme à
	 * ce qui est attendu.
	 */
	public static void main(String[] args) throws FileNotFoundException {
		teste("S30-Dessin-in.xml", "S30-Dessin-out.xml");
	}

	/**
	 * Teste l'enregistrement du dessin dans un fichier XML. Le fichier XML
	 * d'entrée est préalablement lu, puis sauvagardé dans un fichier de sortie.
	 * Le fichier de sortie est ensuite chargé et visualisé par l'application.
	 * 
	 * @param nomFichierEntrée le nom du fichier XML d'entrée lu
	 * @param nomFichierSortie le nom du fichier XML de sortie écrit puis
	 *            affiché
	 * @throws FileNotFoundException si l'un des noms des fichiers n'est pas
	 *             valide
	 */
	public static void teste(String nomFichierEntrée, String nomFichierSortie) throws FileNotFoundException {
		LecteurXML lecteur = new LecteurXML();
		final List<VueForme> dessin = lecteur.lisDessin(nomFichierEntrée);
		EnregistreurXML enregistreur = new EnregistreurXML();
		enregistreur.enregistreDessin(nomFichierSortie, dessin);
		LecteurXML.teste(nomFichierSortie);
	}

	/**
	 * Enregistre le dessin donné dans un fichier.
	 * @param nomFichier le nom du fichier de sauvegarde
	 * @param dessin le dessin formé de la liste des vues des formes
	 * @throws FileNotFoundException si le nom du fichier n'est pas valide
	 */
	public void enregistreDessin(String nomFichier, List<VueForme> dessin) throws FileNotFoundException {
		creeDocumentXML("BeAnArtist");
		Element racine = getDocument().getDocumentElement();
		Element formes = getDocument().createElement("formes");
		racine.appendChild(formes);
		for (VueForme vueForme : dessin) {
			formes.appendChild(creeElementForme(vueForme.getForme()));
		}		
		enregistreDocument(nomFichier);
	}

	/**
	 * Crée un élément DOM représentant la forme donnée et retourne cet élément.
	 * Cette méthode invoque les méthodes creeElement<Forme> en fonction du type
	 * de la forme.
	 * @param forme la forme
	 * @return l'élément DOM représentant la forme
	 */
	public Element creeElementForme(Forme forme) {
		Element element;
		String nom = forme.getClass().getSimpleName();
		if (nom.equals("Rectangle")) {
			element = creeElementRectangle((Rectangle) forme);
		} else if (nom.equals("Carre")) {
			element = creeElementCarre((Carre) forme);
		} else if (nom.equals("Ellipse")) {
			element = creeElementEllipse((Ellipse) forme);
		} else if (nom.equals("Cercle")) {
			element = creeElementCercle((Cercle) forme);
		} else if (nom.equals("Ligne")) {
			element = creeElementLigne((Ligne) forme);
		} else if (nom.equals("Trace")) {
			element = creeElementTrace((Trace) forme);
		} else {
			throw new Error("Forme non gérée");
		}
		//Couleur de la bordure
		Color couleurBordure = forme.getCouleurLigne();
		Element elementCouleur = getDocument().createElement("couleurBordure");
		Text texte = getDocument().createTextNode("0x" + 
		String.format("%02x%02x%02x", couleurBordure.getRed(), couleurBordure.getGreen(), couleurBordure.getBlue()).toUpperCase());
		elementCouleur.appendChild(texte);
		element.appendChild(elementCouleur);
		//Epaisseur de la bordure
		this.ecrireBaliseValeur(element, "epaisseurBordure", ""+forme.getEpaisseurBordure());
		return element;
	}

	/**
	 * Renvoie un nouvel élément DOM représentant le rectangle donné.
	 * @param forme le rectangle
	 * @return élément DOM représentant le rectangle
	 */
	public Element creeElementRectangle(Rectangle forme) {
		return creeElementType1(forme);
	}

	/**
	 * Renvoie un nouvel élément DOM représentant le carré donné.
	 * @param forme le carré
	 * @return élément DOM représentant le carré
	 */
	public Element creeElementCarre(Rectangle forme) {
		return creeElementType2(forme);
	}

	/**
	 * Renvoie un nouvel élément DOM représentant l'ellipse donnée.
	 * @param forme l'ellipse
	 * @return élément DOM représentant l'ellipse
	 */
	public Element creeElementEllipse(Ellipse forme) {
		return creeElementType1(forme);
	}

	/**
	 * Renvoie un nouvel élément DOM représentant le cercle donné.
	 * @param forme le cercle
	 * @return élément DOM représentant le cercle
	 */
	public Element creeElementCercle(Cercle forme) {
		return creeElementType2(forme);
	}

	/**
	 * Renvoie un nouvel élément DOM représentant la ligne donnée.
	 * @param forme la ligne
	 * @return élément DOM représentant la ligne
	 */
	public Element creeElementLigne(Ligne forme) {
		return creeElementType1(forme);
	}

	/**
	 * Renvoie un nouvel élément DOM représentant le tracé donné.
	 * @param forme le tracé
	 * @return élément DOM représentant le tracé
	 */
	public Element creeElementTrace(Trace forme) {
		Element element = getDocument().createElement("forme");
		element.setAttribute("type", forme.getClass().getSimpleName().toLowerCase());
		Element elementPoint = null;
		for(int i = 0 ; i < forme.getPoints().size() ; i++) {
			elementPoint = getDocument().createElement("point");
			this.ecrireBaliseValeur(elementPoint,"x","" + forme.getPoints().get(i).getX());
			this.ecrireBaliseValeur(elementPoint,"y","" + forme.getPoints().get(i).getY());
			element.appendChild(elementPoint);
		}
		return element;
	}
	
	private void ecrireBaliseValeur(Element element, String nomBalise,String valeur) {
		Element elementFils = getDocument().createElement(nomBalise);
		Text texte = getDocument().createTextNode(valeur);
		elementFils.appendChild(texte);
		element.appendChild(elementFils);
	}
	
	private Element creeElementType1(Forme forme) {
		Element element = getDocument().createElement("forme");
		element.setAttribute("type", forme.getClass().getSimpleName().toLowerCase());
		this.ecrireBaliseValeur(element,"x",""+ forme.getX());
		this.ecrireBaliseValeur(element,"y",""+ forme.getY());
		this.ecrireBaliseValeur(element,"hauteur",""+ forme.getHauteur());
		this.ecrireBaliseValeur(element,"largeur",""+ forme.getLargeur());
		Color couleurRemplissage = forme.getCouleurRemplissage();
		if(couleurRemplissage != null){
			Element elementCouleur = getDocument().createElement("couleurRemplissage");
			Text texte = getDocument().createTextNode("0x" + 
					String.format("%02x%02x%02x", couleurRemplissage.getRed(), couleurRemplissage.getGreen(), couleurRemplissage.getBlue()).toUpperCase());
					elementCouleur.appendChild(texte);
					element.appendChild(elementCouleur);
		}
		return element;
	}
	
	private Element creeElementType2(Forme forme) {
		Element element = getDocument().createElement("forme");
		element.setAttribute("type", forme.getClass().getSimpleName().toLowerCase());
		this.ecrireBaliseValeur(element,"x",""+ forme.getX());
		this.ecrireBaliseValeur(element,"y",""+ forme.getY());
		this.ecrireBaliseValeur(element,"hauteur",""+ forme.getHauteur());
		Color couleurRemplissage = forme.getCouleurRemplissage();
		if(couleurRemplissage != null){
			Element elementCouleur = getDocument().createElement("couleurRemplissage");
			Text texte = getDocument().createTextNode("0x" + 
					String.format("%02x%02x%02x", couleurRemplissage.getRed(), couleurRemplissage.getGreen(), couleurRemplissage.getBlue()).toUpperCase());
					elementCouleur.appendChild(texte);
					element.appendChild(elementCouleur);
		}
		return element;
	}
}
