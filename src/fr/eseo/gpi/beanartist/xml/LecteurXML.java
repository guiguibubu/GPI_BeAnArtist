package fr.eseo.gpi.beanartist.xml;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.vue.formes.VueCercle;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueTrace;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

/**
 * Un lecteur XML est un processeur DOM responsable du chargement d'un dessin au
 * format XML défini par l'application.
 * 
 * Il utilise les méthodes héritées de la classe ProcesseurDOM pour charger le
 * fichier XML dans un document DOM, ainsi que les méthodes de lecture des
 * entiers.
 * 
 * Les méthodes lisDessin et creeXxxx devront être complétées. Des méthodes
 * utilitaires pourront venir compléter celles définies par la classe
 * ProcesseurDOM ; elles devront dans ce cas être OBLIGATOIREMENT définies en
 * "private" à la fin de la classe LecteurXML.
 * 
 */
public class LecteurXML extends ProcesseurDOM {

	/**
	 * Lance le test de chargement (méthode teste) avec le fichier XML nommé
	 * "S30-Dessin-in.xml".
	 * 
	 * Vous aurez pris soin de le copier prélablablement dans le ficher
	 * "S30-Dessin-in.xml".
	 * 
	 * Ce test MANUEL doit OBLIGATOIREMENT passer avant de commencer la gestion
	 * de l'enregistrement en XML (classe EnregistreurXML).
	 */
	public static void main(String[] args) throws FileNotFoundException {
		teste("S30-Dessin-in.xml");
	}

	/**
	 * Teste le chargement du fichier XML. Le contenu du fichier est ensuite
	 * affiché dans la fenêtre de l'application (classe FenetreBeAnArtist).
	 * @param nomFichier le fichier d'entrée à lire
	 * @throws FileNotFoundException si le fichier n'existe pas
	 */
	public static void teste(String nomFichier) throws FileNotFoundException {
		LecteurXML lecteur = new LecteurXML();
		final List<VueForme> dessin = lecteur.lisDessin(nomFichier);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				FenetreBeAnArtist fenetre = FenetreBeAnArtist.getInstance();
				for (VueForme vueForme : dessin) {
					fenetre.getPanneauDessin().ajouterVueForme(vueForme);
				}
				fenetre.setVisible(true);
			}
		});
	}

	/**
	 * Charge le fichier XML donné dans un document DOM puis renvoie
	 * l'intégralité du dessin sous la forme d'une liste de vues représentant
	 * les formes stockées dans le fichier.
	 * 
	 * @param nomFichier le nom du fichier XML
	 * @return l'intégralité du dessin sous la forme d'une liste de vues
	 * @throws FileNotFoundException si le fichier n'est pas trouvé ou
	 *             accessible
	 */
	public List<VueForme> lisDessin(String nomFichier) throws FileNotFoundException {
		List<VueForme> dessin = new ArrayList<>();
		chargeDocument(nomFichier);
		Element racine = getDocument().getDocumentElement();
		racine = (Element) racine.getElementsByTagName("formes").item(0);
		if(racine != null) {
			for(int i = 0 ; i < racine.getChildNodes().getLength(); i++) {
				if(racine.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE) {
					dessin.add(creeVueForme((Element) racine.getChildNodes().item(i)));
				}
			}
		}
		return dessin;
	}

	/**
	 * Crée une forme et sa vue associée réprésentées par l'élément DOM donné,
	 * puis renvoie cette vue. Cette méthode invoque les méthodes cree<Forme>
	 * définies pour chacune des <Forme> considérée.
	 * @param element l'élément représentant la vue et sa forme
	 * @return la vue stockée dans l'élément considéré
	 */
	public VueForme creeVueForme(Element element) {
		VueForme vue = null;
		String nom = element.getAttribute("type");
		if (nom.equals("rectangle")) {
			Rectangle forme = creeRectangle(element);
			vue = new VueRectangle(forme);
		} else if (nom.equals("carre")) {
			Carre forme = creeCarre(element);
			vue = new VueCarre(forme);
		} else if (nom.equals("ellipse")) {
			Ellipse forme = creeEllipse(element);
			vue = new VueEllipse(forme);
		} else if (nom.equals("cercle")) {
			Cercle forme = creeCercle(element);
			vue = new VueCercle(forme);
		} else if (nom.equals("ligne")) {
			Ligne forme = creeLigne(element);
			vue = new VueLigne(forme);
		} else if (nom.equals("trace")) {
			Trace forme = creeTrace(element);
			vue = new VueTrace(forme);
		} else {
			
		}
		vue.getForme().setCouleurLigne(this.creerCouleur(element,"couleurBordure"));
		vue.getForme().setEpaisseurBordure(this.lisElementSimple(element,"epaisseurBordure"));
		return vue;
	}

	/**
	 * Renvoie un nouveau rectangle représenté par l'élément DOM donné.
	 * @param element l'élément représentant le rectangle
	 * @return le rectangle stocké dans l'élément considéré
	 */
	public Rectangle creeRectangle(Element element) {
		Rectangle rect = new Rectangle();
		this.setXYHauteur(rect,element);
		rect.setLargeur(this.lisElementSimple(element,"largeur"));
		rect.setCouleurRemplissage(this.creerCouleur(element, "couleurRemplissage"));
		return rect;
	}

	/**
	 * Renvoie un nouveau carré représenté par l'élément DOM donné.
	 * @param element l'élément représentant le carré
	 * @return le carré stocké dans l'élément considéré
	 */
	public Carre creeCarre(Element element) {
		Carre carre = new Carre();
		this.setXYHauteur(carre,element);
		carre.setCouleurRemplissage(this.creerCouleur(element, "couleurRemplissage"));
		return carre;
	}

	/**
	 * Renvoie une nouvelle ellipse représentée par l'élément DOM donné.
	 * @param element l'élément représentant l'ellipse
	 * @return l'ellipse stockée dans l'élément considéré
	 */
	public Ellipse creeEllipse(Element element) {
		Ellipse ellipse = new Ellipse();
		this.setXYHauteur(ellipse,element);
		ellipse.setLargeur(this.lisElementSimple(element,"largeur"));
		ellipse.setCouleurRemplissage(this.creerCouleur(element, "couleurRemplissage"));
		return ellipse;
	}

	/**
	 * Renvoie un nouveau cercle représenté par l'élément DOM donné.
	 * @param element l'élément représentant le cercle
	 * @return le cercle stocké dans l'élément considéré
	 */
	public Cercle creeCercle(Element element) {
		Cercle cercle = new Cercle();
		this.setXYHauteur(cercle,element);
		cercle.setCouleurRemplissage(this.creerCouleur(element, "couleurRemplissage"));
		return cercle;
	}

	/**
	 * Renvoie la nouvelle ligne représentée par l'élément DOM donné.
	 * @param element l'élément représentant la ligne
	 * @return la ligne stockée dans l'élément considéré
	 */
	public Ligne creeLigne(Element element) {
		Ligne ligne = new Ligne();
		this.setXYHauteur(ligne,element);
		ligne.setLargeur(this.lisElementSimple(element,"largeur"));
		return ligne;
	}

	/**
	 * Renvoie un nouveau tracé représenté par l'élément DOM donné.
	 * @param element l'élément représentant le tracé
	 * @return le tracé stocké dans l'élément considéré
	 */
	public Trace creeTrace(Element element) {
		// création de la liste des points du tracé
		Trace trace = null;
		Element elementFils = null;
		for(int i = 0 ; i < element.getElementsByTagName("point").getLength(); i++) {
			if(element.getElementsByTagName("point").item(i).getNodeType() == Node.ELEMENT_NODE) {
				elementFils = (Element) element.getElementsByTagName("point").item(i);
				if(i == 0) {
					trace = new Trace(new Point(this.lisElementSimple(elementFils, "x"),this.lisElementSimple(elementFils, "y")));
				} else {
				trace.ajouterPoint(new Point(this.lisElementSimple(elementFils, "x"),this.lisElementSimple(elementFils, "y")));
				}
			}			
		}
		return trace;
	}
	
	private Color creerCouleur(Element elementParent, String nomElementFils) {
		Node premier = elementParent.getElementsByTagName(nomElementFils).item(0);
		return Color.decode("" + premier.getChildNodes().item(0).getNodeValue());
	}
	
	private void setXYHauteur(Forme forme, Element element) {
		forme.setX(this.lisElementSimple(element, "x"));
		forme.setY(this.lisElementSimple(element, "y"));
		forme.setHauteur(this.lisElementSimple(element,"hauteur"));
	}

	public void importe(FenetreBeAnArtist fenetre, String adresseFichier) throws FileNotFoundException {
		List<VueForme> dessin = this.lisDessin(adresseFichier);
		for (VueForme vueForme : dessin) {
			fenetre.getPanneauDessin().ajouterVueForme(vueForme);
		}	
	}

}
