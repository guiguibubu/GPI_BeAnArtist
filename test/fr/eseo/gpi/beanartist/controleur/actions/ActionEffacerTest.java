package fr.eseo.gpi.beanartist.controleur.actions;

import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionEffacerTest {

	public ActionEffacerTest() {
		FenetreBeAnArtist fenetre = FenetreBeAnArtist.getInstance();
		fenetre.setVisible(true);
		fenetre.getPanneauDessin().ajouterVueForme(new VueRectangle(new Rectangle(10,10,100,100)));
	}

	public static void main(String[] args) {
		new ActionEffacerTest();
	}

}
