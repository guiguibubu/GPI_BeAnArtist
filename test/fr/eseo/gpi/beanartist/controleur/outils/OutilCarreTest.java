package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.controleur.outils.OutilCarre;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilCarreTest {

	public OutilCarreTest() {
		new OutilCarre(FenetreBeAnArtist.getInstance().getPanneauDessin());
		FenetreBeAnArtist.getInstance().setVisible(true);
	}

	public static void main(String[] args) {
		new OutilCarreTest();
	}

}
