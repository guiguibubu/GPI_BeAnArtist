package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.controleur.outils.OutilCercle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilCercleTest {

	public OutilCercleTest() {
		new OutilCercle(FenetreBeAnArtist.getInstance().getPanneauDessin());
		FenetreBeAnArtist.getInstance().setVisible(true);
		
	}

	public static void main(String[] args) {
		new OutilCercleTest();
	}

}
