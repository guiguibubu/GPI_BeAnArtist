package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.controleur.outils.OutilEllipse;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilEllipseTest {

	public OutilEllipseTest() {
		new OutilEllipse(FenetreBeAnArtist.getInstance().getPanneauDessin());
		FenetreBeAnArtist.getInstance().setVisible(true);
	}

	public static void main(String[] args) {
		new OutilEllipseTest();
	}

}
