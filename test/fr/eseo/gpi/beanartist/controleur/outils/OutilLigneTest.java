package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.controleur.outils.OutilLigne;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilLigneTest {

	public OutilLigneTest() {
		FenetreBeAnArtist fenetre = FenetreBeAnArtist.getInstance();
		System.out.println("On créer un panneau Dessin 'pd'");
		PanneauDessin pd = new PanneauDessin();
		System.out.println("On créé un outilLigne associé à 'pd'\n");
		OutilLigne outil1 = new OutilLigne(pd);
		
		int IDpd = pd.hashCode();
		int IDpanneauDessinBeAnArtist = fenetre.getPanneauDessin().hashCode();
		int IDoutil1 = outil1.hashCode();
		
		System.out.println("outil1 = pd.getOutilsCourant ? "+(IDoutil1==pd.getOutilCourant().hashCode()));
		System.out.println("outil1.getPanneauDessin = pd ? "+(outil1.getPanneauDessin().hashCode()==IDpd));
		
		System.out.println("\nOn associe outil1 au panneau dessin de la fenetre BeAnArtist\n");
		outil1.associer(fenetre.getPanneauDessin());
		
		System.out.println("outil1.getPanneauDessin() = fenetre.getPanneauDessin ? "+(IDpanneauDessinBeAnArtist==outil1.getPanneauDessin().hashCode()));
		
		System.out.println("pd.getOutilCourant() = null ? "+(pd.getOutilCourant()==null));
		System.out.println("pd.getMouseListener() = 0 ? "+(pd.getMouseListeners().length == 0));
		System.out.println("fenetre.getPanneauDessin().getOutilCourant() = outil1 ? "+(fenetre.getPanneauDessin().getOutilCourant().hashCode()==IDoutil1));
		System.out.println("fenetre.getPanneauDessin().getMouseListener() = 1 ? "+(fenetre.getPanneauDessin().getMouseListeners().length==1));
		System.out.println("--------------------------------------");
		
		System.out.println("\nOn créé un outilLigne associé à 'pd'\n");
		OutilLigne outil2 = new OutilLigne(pd);
		int IDoutil2 = outil2.hashCode();
		
		System.out.println("outil2 = pd.getOutilsCourant ? "+(IDoutil2==pd.getOutilCourant().hashCode()));
		System.out.println("outil2.getPanneauDessin = pd ? "+(outil2.getPanneauDessin().hashCode()==IDpd));
		
		System.out.println("\nOn associe outil2 au panneau dessin de la fenetre BeAnArtist\n");
		outil2.associer(fenetre.getPanneauDessin());
		
		System.out.println("outil1.getPanneauDessin = null ? "+(outil1.getPanneauDessin()==null));
		System.out.println("outil2.getPanneauDessin() = fenetre.getPanneauDessin ? "+(IDpanneauDessinBeAnArtist==outil2.getPanneauDessin().hashCode()));
		
		System.out.println("pd.getOutilCourant() = null ? "+(pd.getOutilCourant()==null));
		System.out.println("pd.getMouseListener() = 0 ? "+(pd.getMouseListeners().length == 0));
		System.out.println("fenetre.getPanneauDessin().getOutilCourant() = outil2 ? "+(fenetre.getPanneauDessin().getOutilCourant().hashCode()==IDoutil2));
		System.out.println("fenetre.getPanneauDessin().getMouseListener() = 1 ? "+(fenetre.getPanneauDessin().getMouseListeners().length==1));
		System.out.println("--------------------------------------");
		
		
		Point debut = new Point(138,41);
		Point fin = new Point(158,51);
		
		outil2.setDebut(debut);
		outil2.setFin(fin);
		
		outil2.getPanneauDessin().ajouterVueForme(outil2.creerVueForme());
		
		debut.deplacerDe(20, 20);
		fin.deplacerDe(20, 20);
		
		fenetre.getPanneauDessin().ajouterVueForme(outil2.creerVueForme());
		
		FenetreBeAnArtist.getInstance().setVisible(true);
	}

	public static void main(String[] args) {
		new OutilLigneTest();
	}
}
