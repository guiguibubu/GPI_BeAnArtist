package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.controleur.outils.OutilRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilRectangleTest {

	public OutilRectangleTest() {
		new OutilRectangle(FenetreBeAnArtist.getInstance().getPanneauDessin());
		FenetreBeAnArtist.getInstance().setVisible(true);
	}

	public static void main(String[] args) {
		new OutilRectangleTest();
	}

}
