package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Carre;

public class CarreTest{
	
	@Test
	public void testConstructeurPositionCote(){
		Point position = new Point(10,20);
		int cote = 30;
		Carre carre = new Carre(position, cote);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), cote, cote, carre);
	}
	
	@Test
	public void testConstructeurCoordCote(){
		Point position = new Point(10,20);
		int cote = 30;
		Carre carre = new Carre(position.getX(), position.getY(), cote);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), cote, cote, carre);
	}
	
	@Test
	public void testConstructeurPosition(){
		Point position = new Point(10,20);
		Carre carre = new Carre(position);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), (int)Carre.COTE_PAR_DEFAUT, (int)Carre.COTE_PAR_DEFAUT, carre);
	}
	
	@Test
	public void testConstructeurCote(){
		int cote = 30;
		Carre carre = new Carre(cote);
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, cote, cote, carre);
	}
	
	@Test
	public void testConstructeurVide(){
		Carre carre = new Carre();
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Carre.COTE_PAR_DEFAUT, (int)Carre.COTE_PAR_DEFAUT, carre);
	}
	
	@Test
	public void testSetLargeur(){
		Carre carre = new Carre();
		carre.setLargeur(Carre.COTE_PAR_DEFAUT + 1);
		assertArrayEquals(new int []{(int) (Carre.COTE_PAR_DEFAUT + 1),(int) (Carre.COTE_PAR_DEFAUT + 1)}, new int[]{(int)carre.getLargeur(), (int)carre.getHauteur()});
	}
	
	@Test
	public void testSetHauteur(){
		Carre carre = new Carre();
		carre.setHauteur(Carre.COTE_PAR_DEFAUT + 1);
		assertArrayEquals(new int []{(int) (Carre.COTE_PAR_DEFAUT + 1),(int) (Carre.COTE_PAR_DEFAUT + 1)}, new int[]{(int)carre.getLargeur(), (int)carre.getHauteur()});
	}
	
	@Test
	public void testSetCote(){
		Carre carre = new Carre();
		carre.setCote(Carre.COTE_PAR_DEFAUT + 1);
		assertArrayEquals(new int []{(int) (Carre.COTE_PAR_DEFAUT + 1),(int) (Carre.COTE_PAR_DEFAUT + 1)}, new int[]{(int)carre.getLargeur(), (int)carre.getHauteur()});
	}
	
	private void verifieCoordonnees(int x, int y, int largeur, int hauteur, Carre carre){
		assertArrayEquals(new int []{x, y, largeur, hauteur}, new int[]{(int) carre.getX(), (int) carre.getY(),(int)carre.getLargeur(), (int)carre.getHauteur()});
	}
}