package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Cercle;

public class CercleTest{
	
	@Test
	public void testConstructeurPositionDiametre(){
		Point position = new Point(10,20);
		int diametre = 30;
		Cercle cercle = new Cercle(position, diametre);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), diametre, diametre, cercle);
	}
	
	@Test
	public void testConstructeurCoordDiametre(){
		Point position = new Point(10,20);
		int diametre = 30;
		Cercle cercle = new Cercle(position.getX(), position.getY(), diametre);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), diametre, diametre, cercle);
	}
	
	@Test
	public void testConstructeurPosition(){
		Point position = new Point(10,20);
		Cercle cercle = new Cercle(position);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), (int)Cercle.DIAMETRE_PAR_DEFAUT, (int)Cercle.DIAMETRE_PAR_DEFAUT, cercle);
	}
	
	@Test
	public void testConstructeurDiametre(){
		int diametre = 30;
		Cercle cercle = new Cercle(diametre);
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, diametre, diametre, cercle);
	}
	
	@Test
	public void testConstructeurVide(){
		Cercle cercle = new Cercle();
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Cercle.DIAMETRE_PAR_DEFAUT, (int)Cercle.DIAMETRE_PAR_DEFAUT, cercle);
	}
	
	@Test
	public void testSetLargeur(){
		Cercle cercle = new Cercle();
		cercle.setLargeur(Cercle.DIAMETRE_PAR_DEFAUT + 1);
		assertArrayEquals(new int []{(int) (Cercle.DIAMETRE_PAR_DEFAUT + 1),(int) (Cercle.DIAMETRE_PAR_DEFAUT + 1)}, new int[]{(int)cercle.getLargeur(), (int)cercle.getHauteur()});
	}
	
	@Test
	public void testSetHauteur(){
		Cercle cercle = new Cercle();
		cercle.setHauteur(Cercle.DIAMETRE_PAR_DEFAUT + 1);
		assertArrayEquals(new int []{(int) (Cercle.DIAMETRE_PAR_DEFAUT + 1),(int) (Cercle.DIAMETRE_PAR_DEFAUT + 1)}, new int[]{(int)cercle.getLargeur(), (int)cercle.getHauteur()});
	}
	
	@Test
	public void testSetRayon(){
		Cercle cercle = new Cercle();
		cercle.setRayon(Cercle.DIAMETRE_PAR_DEFAUT + 1);
		assertArrayEquals(new int []{(int) (Cercle.DIAMETRE_PAR_DEFAUT + 1)*2,(int) (Cercle.DIAMETRE_PAR_DEFAUT + 1)*2}, new int[]{(int)cercle.getLargeur(), (int)cercle.getHauteur()});
	}
	
	private void verifieCoordonnees(int x, int y, int largeur, int hauteur, Cercle cercle){
		assertArrayEquals(new int []{x, y, largeur, hauteur}, new int[]{(int) cercle.getX(), (int) cercle.getY(),(int)cercle.getLargeur(), (int)cercle.getHauteur()});
	}
}
