package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Ellipse;

public class EllipseTest{
	
	@Test
	public void testConstructeurPositionLargeurHauteur(){
		Point position = new Point(10,20);
		int largeur = 30;
		int hauteur = 40;
		Ellipse ellipse = new Ellipse(position, largeur, hauteur);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), largeur, hauteur, ellipse);
	}
	
	@Test
	public void testConstructeurCoordLargeurHauteur(){
		Point position = new Point(10,20);
		int largeur = 30;
		int hauteur = 40;
		Ellipse ellipse = new Ellipse(position.getX(), position.getY(), largeur, hauteur);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), largeur, hauteur, ellipse);
	}
	
	@Test
	public void testConstructeurPosition(){
		Point position = new Point(10,20);
		Ellipse ellipse = new Ellipse(position);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), (int)Ellipse.LARGEUR_PAR_DEFAUT,(int)Ellipse.HAUTEUR_PAR_DEFAUT, ellipse);
	}
	
	@Test
	public void testConstructeurLargeurHauteur(){
		int largeur = 30;
		int hauteur = 40;
		Ellipse ellipse = new Ellipse(largeur, hauteur);
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, largeur, hauteur, ellipse);
	}
	
	@Test
	public void testConstructeurVide(){
		Ellipse ellipse = new Ellipse();
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Ellipse.LARGEUR_PAR_DEFAUT,(int)Ellipse.HAUTEUR_PAR_DEFAUT, ellipse);
	}
	
	@Test
	public void testSetLargeur(){
		Ellipse ellipse = new Ellipse();
		ellipse.setLargeur(Ellipse.LARGEUR_PAR_DEFAUT + 1);
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Ellipse.LARGEUR_PAR_DEFAUT+1,(int)Ellipse.HAUTEUR_PAR_DEFAUT, ellipse);
	}
	
	@Test
	public void testSetHauteur(){
		Ellipse ellipse = new Ellipse();
		ellipse.setHauteur(Ellipse.HAUTEUR_PAR_DEFAUT + 1);
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Ellipse.LARGEUR_PAR_DEFAUT,(int)Ellipse.HAUTEUR_PAR_DEFAUT+1, ellipse);
	}
	
	@Test
	public void testPerimetre(){
		int largeur = 20;
		int hauteur = 20;
		Ellipse ellipse = new Ellipse(largeur, hauteur);
		
		assertEquals(2*Math.PI*10, ellipse.perimetre(), Forme.EPSILON);
	}
	
	@Test
	public void testAire(){
		int largeur = 20;
		int hauteur = 20;
		Ellipse ellipse = new Ellipse(largeur, hauteur);
		
		assertEquals(Math.PI*Math.pow(10, 2), ellipse.aire(), Forme.EPSILON);
	}
	
	@Test
	public void testContientTrue(){
		int largeur = 2;
		int hauteur = 2;
		Ellipse ellipse = new Ellipse(largeur, hauteur);
		
		assertTrue(ellipse.contient(1/Math.sqrt(2), 1/Math.sqrt(2)));
	}
	
	@Test
	public void testContientFalse(){
		int largeur = 2;
		int hauteur = 2;
		Ellipse ellipse = new Ellipse(largeur, hauteur);
		
		assertFalse(ellipse.contient(5, 5));
	}
	
	private void verifieCoordonnees(int x, int y, int largeur, int hauteur, Ellipse ellipse){
		assertArrayEquals(new int []{x, y, largeur, hauteur}, new int[]{(int) ellipse.getX(), (int) ellipse.getY(),(int)ellipse.getLargeur(), (int)ellipse.getHauteur()});
	}
}
