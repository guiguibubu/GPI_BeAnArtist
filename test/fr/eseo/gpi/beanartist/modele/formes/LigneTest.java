package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Ligne;

public class LigneTest{
		
	@Test
	public void testConstructeurPositionLargeurHauteur(){
		Point position = new Point(10,20);
		int largeur = 30;
		int hauteur = 40;
		Ligne ligne = new Ligne(position, largeur, hauteur);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), largeur, hauteur, ligne);
	}
	
	@Test
	public void testConstructeurCoordLargeurHauteur(){
		Point position = new Point(10,20);
		int largeur = 30;
		int hauteur = 40;
		Ligne ligne = new Ligne(position.getX(), position.getY(), largeur, hauteur);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), largeur, hauteur, ligne);
	}
	
	@Test
	public void testConstructeurPosition(){
		Point position = new Point(10,20);
		Ligne ligne = new Ligne(position);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), (int)Ligne.LARGEUR_PAR_DEFAUT,(int)Ligne.HAUTEUR_PAR_DEFAUT, ligne);
	}
	
	@Test
	public void testConstructeurLargeurHauteur(){
		int largeur = 30;
		int hauteur = 40;
		Ligne ligne = new Ligne(largeur, hauteur);
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, largeur, hauteur, ligne);
	}
	
	@Test
	public void testConstructeurVide(){
		Ligne ligne = new Ligne();
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Ligne.LARGEUR_PAR_DEFAUT,(int)Ligne.HAUTEUR_PAR_DEFAUT, ligne);
	}
	
	@Test
	public void testConstructeur2Points(){
		Point position1 = new Point(10,20);
		Point position2 = new Point(30,40);
		Ligne ligne = new Ligne(position1, position2);
		
		verifieCoordonnees((int)position1.getX(), (int)position1.getY(), 20,20, ligne);
	}
	
	@Test
	public void testConstructeurLargeurHauteurNegatives(){
		Point position2 = new Point(10,20);
		Point position1 = new Point(30,40);
		Ligne ligne = new Ligne(position1, position2);
		
		verifieCoordonnees((int)position1.getX(), (int)position1.getY(), -20, -20, ligne);
	}
	
	@Test
	public void testSetLargeur(){
		Ligne ligne = new Ligne();
		ligne.setLargeur(Ligne.LARGEUR_PAR_DEFAUT + 1);
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Ligne.LARGEUR_PAR_DEFAUT+1,(int)Ligne.HAUTEUR_PAR_DEFAUT, ligne);
	}
	
	@Test
	public void testSetHauteur(){
		Ligne ligne = new Ligne();
		ligne.setHauteur(Ligne.HAUTEUR_PAR_DEFAUT + 1);
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Ligne.LARGEUR_PAR_DEFAUT,(int)Ligne.HAUTEUR_PAR_DEFAUT+1, ligne);
	}
	
	@Test
	public void testPerimetre(){
		int largeur = 20;
		int hauteur = 20;
		Ligne ligne = new Ligne(largeur, hauteur);
		
		assertEquals(Math.sqrt(Math.pow(largeur, 2)+Math.pow(hauteur, 2)), ligne.perimetre(), Forme.EPSILON);
	}
	
	@Test
	public void testAire(){
		Ligne ligne = new Ligne();
		
		assertEquals(0, ligne.aire(), Forme.EPSILON);
	}
	
	@Test
	public void testContientTrue(){
		int largeur = 2;
		int hauteur = 2;
		Ligne ligne = new Ligne(largeur, hauteur);
		
		assertTrue(ligne.contient(1, 1));
	}
	
	@Test
	public void testContientFalse(){
		int largeur = 2;
		int hauteur = 2;
		Ligne ligne = new Ligne(largeur, hauteur);
		
		assertFalse(ligne.contient(0, 1));
	}
	
	private void verifieCoordonnees(int x, int y, int largeur, int hauteur, Ligne ligne){
		assertArrayEquals(new int []{x, y, largeur, hauteur}, new int[]{(int) ligne.getX(), (int) ligne.getY(),(int)ligne.getLargeur(), (int)ligne.getHauteur()});
	}
}
