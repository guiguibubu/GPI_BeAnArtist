package fr.eseo.gpi.beanartist.modele.formes;

import fr.eseo.gpi.beanartist.modele.formes.Point;

public class PointEssai {

	public static void main(String[] args) {
		Point p1 = new Point();
		Point p2 = new Point(15.1, 1.254545);
		System.out.println(p1.toString());
		System.out.println(p2.toString()+"\n");
	}
}