package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Rectangle;

public class RectangleTest{

	@Test
	public void testConstructeurPositionLargeurHauteur(){
		Point position = new Point(10,20);
		int largeur = 30;
		int hauteur = 40;
		Rectangle rectangle = new Rectangle(position, largeur, hauteur);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), largeur, hauteur, rectangle);
	}
	
	@Test
	public void testConstructeurCoordLargeurHauteur(){
		Point position = new Point(10,20);
		int largeur = 30;
		int hauteur = 40;
		Rectangle rectangle = new Rectangle(position.getX(), position.getY(), largeur, hauteur);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), largeur, hauteur, rectangle);
	}
	
	@Test
	public void testConstructeurPosition(){
		Point position = new Point(10,20);
		Rectangle rectangle = new Rectangle(position);
		
		verifieCoordonnees((int)position.getX(), (int)position.getY(), (int)Rectangle.LARGEUR_PAR_DEFAUT,(int)Rectangle.HAUTEUR_PAR_DEFAUT, rectangle);
	}
	
	@Test
	public void testConstructeurLargeurHauteur(){
		int largeur = 30;
		int hauteur = 40;
		Rectangle rectangle = new Rectangle(largeur, hauteur);
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, largeur, hauteur, rectangle);
	}
	
	@Test
	public void testConstructeurVide(){
		Rectangle rectangle = new Rectangle();
		
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Rectangle.LARGEUR_PAR_DEFAUT,(int)Rectangle.HAUTEUR_PAR_DEFAUT, rectangle);
	}
	
	@Test
	public void testSetLargeur(){
		Rectangle rectangle = new Rectangle();
		rectangle.setLargeur(Rectangle.LARGEUR_PAR_DEFAUT + 1);
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Rectangle.LARGEUR_PAR_DEFAUT+1,(int)Rectangle.HAUTEUR_PAR_DEFAUT, rectangle);
	}
	
	@Test
	public void testSetHauteur(){
		Rectangle rectangle = new Rectangle();
		rectangle.setHauteur(Rectangle.HAUTEUR_PAR_DEFAUT + 1);
		verifieCoordonnees((int)Point.ABSCISSE_PAR_DEFAUT, (int)Point.ORDONNEE_PAR_DEFAUT, (int)Rectangle.LARGEUR_PAR_DEFAUT,(int)Rectangle.HAUTEUR_PAR_DEFAUT+1, rectangle);
	}
	
	@Test
	public void testPerimetre(){
		int largeur = 20;
		int hauteur = 10;
		Rectangle rectangle = new Rectangle(largeur, hauteur);
		
		assertEquals(60, rectangle.perimetre(), Forme.EPSILON);
	}
	
	@Test
	public void testAire(){
		int largeur = 20;
		int hauteur = 10;
		Rectangle rectangle = new Rectangle(largeur, hauteur);
		
		assertEquals(200, rectangle.aire(), Forme.EPSILON);
	}
	
	@Test
	public void testContientTrue(){
		int largeur = 2;
		int hauteur = 2;
		Rectangle rectangle = new Rectangle(largeur, hauteur);
		
		assertTrue(rectangle.contient(1,1));
	}
	
	@Test
	public void testContientFalse(){
		int largeur = 2;
		int hauteur = 2;
		Rectangle rectangle = new Rectangle(largeur, hauteur);
		
		assertFalse(rectangle.contient(5, 5));
	}
	
	private void verifieCoordonnees(int x, int y, int largeur, int hauteur, Rectangle rectangle){
		assertArrayEquals(new int []{x, y, largeur, hauteur}, new int[]{(int) rectangle.getX(), (int) rectangle.getY(),(int)rectangle.getLargeur(), (int)rectangle.getHauteur()});
	}
}
