package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.modele.formes.Point;

public class TraceTest{
	
	/**
	 * Test du constructeur et du getter.
	 */
	@Test
	public void testConstructeur2Points() {
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		verifieCoordonnees((int)p1.getX(), (int)p1.getY(), 1, 1, trace);
	}
	
	@Test
	public void testConstructeur1Point() {
		Point p1 = new Point(1,1);
		Trace trace = new Trace(p1);
		verifieCoordonnees((int)p1.getX(), (int)p1.getY(), 0, 0, trace);
	}
	
	@Test 
	public void testGetPoints(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		
		assertArrayEquals(new Point[]{p1, p2}, trace.getPoints().toArray());
	}
	
	@Test
	public void testAjouterPoint(){
		Point p2 = new Point(1,1);
		Point p1 = new Point(2,2);
		Trace trace = new Trace(p1);
		trace.ajouterPoint(p2);
		
		verifieCoordonnees((int)p2.getX(), (int)p2.getY(), 1, 1, trace);
	}
	
	@Test 
	public void testSetX(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		trace.setX(0);
		
		assertArrayEquals(new int[]{0,1,1,2}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
	}
	
	@Test 
	public void testSetY(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		trace.setY(0);
		
		assertArrayEquals(new int[]{1,0,2,1}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
	}
	
	@Test
	public void testSetPosition1(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		
		trace.setPosition(0, 0);
		assertArrayEquals(new int[]{0,0,1,1}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
	}
	
	@Test
	public void testSetPosition2(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		
		trace.setPosition(new Point(0,0));
		assertArrayEquals(new int[]{0,0,1,1}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
	}
	
	@Test
	public void testDeplacerDe(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		
		trace.deplacerDe(-1, 2);
		assertArrayEquals(new int[]{0,3,1,4}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
	}
	
	@Test
	public void testDeplacerVers(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);
		
		trace.deplacerVers(0,0);
		assertArrayEquals(new int[]{0,0,1,1}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
	}
	
	@Test
	public void testSetLargeur(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);

		trace.setLargeur(-1);
		assertArrayEquals(new int[]{1,1,0,2}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
		verifieCoordonnees(0, 1, 1, 1, trace);
	}
	
	@Test
	public void testSetHauteur(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Trace trace = new Trace(p1, p2);

		trace.setHauteur(-1);
		assertArrayEquals(new int[]{1,1,2,0}, new int[]{(int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY()});
		verifieCoordonnees(1, 0, 1, 1, trace);
	}
	
	@Test
	public void testPerimetre(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Point p3 = new Point (3,1);
		Trace trace = new Trace(p1, p2);
		trace.ajouterPoint(p3);
		
		assertEquals(2*Math.sqrt(2), trace.perimetre(), Forme.EPSILON);
	}
	
	@Test
	public void testAire(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Point p3 = new Point (3,1);
		Trace trace = new Trace(p1, p2);
		trace.ajouterPoint(p3);
		
		assertEquals(0, trace.aire(), Forme.EPSILON);
	}
	
	@Test
	public void testContientPointTrue(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Point p3 = new Point (3,1);
		Trace trace = new Trace(p1, p2);
		trace.ajouterPoint(p3);
		
		assertTrue(trace.contient(new Point(1.5,1.5)));
	}
	
	@Test
	public void testContientPointFalse(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Point p3 = new Point (3,1);
		Trace trace = new Trace(p1, p2);
		trace.ajouterPoint(p3);
		
		assertFalse(trace.contient(new Point(0.5,0.5)));
	}
	
	@Test
	public void testContientCoordTrue(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Point p3 = new Point (3,1);
		Trace trace = new Trace(p1, p2);
		trace.ajouterPoint(p3);
		
		assertTrue(trace.contient(1.5,1.5));
	}
	
	@Test
	public void testContientCoordFalse(){
		Point p1 = new Point(1,1);
		Point p2 = new Point(2,2);
		Point p3 = new Point (3,1);
		Trace trace = new Trace(p1, p2);
		trace.ajouterPoint(p3);
		
		assertFalse(trace.contient(0.5,0.5));
	}
	
	private void verifieCoordonnees(int x, int y, int largeur, int hauteur, Trace trace){
		assertArrayEquals(new int []{x, y, largeur, hauteur}, new int[]{(int) trace.getX(), (int) trace.getY(),(int)trace.getLargeur(), (int)trace.getHauteur()});
	}
	
}