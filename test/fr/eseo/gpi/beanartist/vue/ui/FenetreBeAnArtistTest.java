package fr.eseo.gpi.beanartist.vue.ui;

import javax.swing.SwingUtilities;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class FenetreBeAnArtistTest{
	
	public FenetreBeAnArtistTest(){
		testDefaultConstructor();
	}
	// Méthode de test
	public void testDefaultConstructor() {
		FenetreBeAnArtist fenetre = FenetreBeAnArtist.getInstance();
		fenetre.setVisible(true);
	}
		
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				new FenetreBeAnArtistTest();
			}
		});
	}
}
