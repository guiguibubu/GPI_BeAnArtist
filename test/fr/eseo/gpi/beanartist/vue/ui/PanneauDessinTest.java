package fr.eseo.gpi.beanartist.vue.ui;

import javax.swing.SwingUtilities;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.vue.formes.VueCercle;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueTrace;

public class PanneauDessinTest{
	
	public PanneauDessinTest(){
		testDefaultConstructor();
	}
	// Méthode de test
	public void testDefaultConstructor() {
		FenetreBeAnArtist fenetre = FenetreBeAnArtist.getInstance();
		fenetre.setVisible(true);
		Rectangle rectangle = new Rectangle (20,20,200,100);
		Ellipse ellipse = new Ellipse(20,20,200,100);
		Carre carre = new Carre(40,40,50);
		Cercle cercle = new Cercle(40,40,50);
		Ligne ligne = new Ligne(220,120,-200,-100);
		Trace trace = new Trace(new Point(20,120), new Point(220,20));
		trace.ajouterPoint(new Point(420,120));
		fenetre.getPanneauDessin().ajouterVueForme(new VueRectangle(rectangle));
		fenetre.getPanneauDessin().ajouterVueForme(new VueCarre(carre));
		fenetre.getPanneauDessin().ajouterVueForme(new VueEllipse(ellipse));
		fenetre.getPanneauDessin().ajouterVueForme(new VueCercle(cercle));
		fenetre.getPanneauDessin().ajouterVueForme(new VueLigne(ligne));
		fenetre.getPanneauDessin().ajouterVueForme(new VueTrace(trace));
		
	}
		 
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				new PanneauDessinTest();
			}
		});
	}
}